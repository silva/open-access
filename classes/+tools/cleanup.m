function [ db ] = cleanup( db )
    %UNTITLED Summary of this function goes here
    %   Detailed explanation goes here
    
    mark_for_delete = zeros(length(db),1);
    for k=1:length(db)
        
        if isempty(db{k,2})
            mark_for_delete(k) = 1;
        end
        
    end
    db(mark_for_delete==1,:) = [];
    
    
end

