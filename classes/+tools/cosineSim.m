function [ costeta, adeg] = cosineSim( R,S )
    %COSINESIM computes the cosine similarity of vectors R and S
    %  
    
    N = min([length(S) length(R)]);
    A = zeros(N,1);
    for i=1:N
       
        A(i) = sum(R(i)*S(i));
        
    end
    
    costeta = sum(A)/( sqrt( sum(R(1:N).^2) .* sum(S(1:N).^2) ) );
    
    assert(costeta > -1 || costeta <1,'Cos(teta) outside validity range [-1,1]');
    
    
    adeg = acosd( costeta );
    
end


