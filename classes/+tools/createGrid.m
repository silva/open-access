function grid_coord = createGrid( x,y,z, xlen, ylen, zlen )
    %CREATE_GRID

    if isempty(xlen)
        xlen=1;
    end
    
    if isempty(ylen)
        ylen=1;
    end
    
    if isempty(zlen)
        zlen=1;
    end
    
    if length(x) < 2
        error('X needs to be a vector with [min_lim max_lim');
    end
    
    if length(y) < 2
        error('Y needs to be a vector with [min_lim max_lim');
    end
    
    if ~isempty(z)
        if length(z) < 2
            error('Z needs to be a vector with [min_lim max_lim');
        end
         [xg,yg,zg]=meshgrid(x(1):xlen:x(2),y(1):ylen:y(2),z(1):zlen:z(2));
         grid_coord = [xg(:),yg(:),zg(:)];
    else
         [xg,yg]=meshgrid(x(1):xlen:x(2),y(1):ylen:y(2));
         zg = NaN;
         grid_coord = [xg(:),yg(:)];
    end
    
    
    
end

