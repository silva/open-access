function status = isWritable( mfile )
    %isWritable checks if you can write the given mfile
    
    status=mfile.Properties.Writable;
    
end

