
function vObj = createVideo(filename,framerate)
%CREATEVIDEO creates a video object
%
%   DESCRIPTION
%   This functions creates a video object with the given name and
%   framerate. The video is open and frames can then be added to it.
%
%   INPUT
%   filename  - Video name n disk (default: videoOutput)
%   framerate - Video framerate (default: 2)
%
%   OUPUT
%   video     - Video object
%
%   Pedro Silva
%   Tampere University of Technology, 2014

    if ~exist('framerate','var') || isempty(framerate)
        framerate = 2;
    end
    
    if ~exist('filename','var') || isempty(filename)
        filename = 'videoOutput';
    end
    
    vObj = VideoWriter(filename);
    set(vObj,'FrameRate',framerate);
    open(vObj); 
    
end


