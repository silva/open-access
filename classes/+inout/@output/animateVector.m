function animateVector(vector,speed,lims)
    %CREATEFIG creates a new figure and returns handles for it and its axes
    %   A simple wraper to quickly obtain axes handles to a figure. The default
    %   behaviour is set to "hold" all drawings in the axes.
    %
    %   USAGE
    %   [ axs, fig ] = createfig({1|0})
    %
    %   INPUT
    %   VISIBILITY - by default is set to 'off'. Set on to see the figure.
    %
    %   OUTPUT
    %   AXS - Figure's axes
    %   FIG - Figure's handle
    %
    %   NOTE
    %   Use get(hAxs(k),'parent') to retrieve corresponding figure handle
    %
    %   Pedro Silva, Tampere University of Technology, 2015
    
    if ~exist('speed','var')
        speed = 0.2;
    end
    
    if ~exist('lims','var')
        lims(:,1) = [floor(min(vector(:,1))) ceil(max(vector(:,1)))];
        lims(:,2) = [floor(min(vector(:,2))) ceil(max(vector(:,2)))];
    end
    
    % Creates handles
    fig     = figure('Visible','on');
    axs     = axes('Parent',fig);
    hold(axs,'on');
    xlim(lims(:,1));
    ylim(lims(:,2));
    
    for k=2:length(vector)
        plot(axs,vector(k-1:k,1),vector(k-1:k,2),'b-','Linewidth',2)
        plot(axs,vector(k,1),vector(k,2),'xk','Linewidth',2,'MarkerSize',10)
        pause(speed)
        plot(axs,vector(k,1),vector(k,2),'xw','Linewidth',2,'MarkerSize',10)
%         plot(axs,vector(k,1),vector(k,2),'.b','Linewidth',2)
k
    end
    
end

