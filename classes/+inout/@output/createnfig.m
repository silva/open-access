        function [hAxs,hFigs] = createnfig(n,visible,holdfig,type)
            %CREATEHANDLES creates M handles and returns them in a col vector
            %	If no input is passed, a single handle (visible) to a normal plot is
            %	created.
            %
            %	USAGE
            %   [hAxs,hFigs] = mhandles(M,visible,holdfig,type)
            %
            %   DESCRIPTION
            %   The output vector is the same size as the output vector, however only
            %   the positions marked as 1 will get an handle created. Type specifies if
            %   the normal plot or the subplot is used.
            %
            %   INPUT
            %   opvector   - Vector of options (mark with one to create an handle)
            %   visibility - Sets the figure visibility ( 0 or 1 )
            %   type       - Default is plot (1), however subplot (0) is also available
            %
            %   OUTPUT
            %   hAxs       - axes handles
            %   hSubfig    - subplot handle
            %
            %   NOTE
            %   Use get(hAxs(k),'parent') to retrieve corresponding figure handle
            %
            %   AUTHOR
            %   Pedro Silva
            %   Tampere University of Technology, 2013
            
            % Default parameters
            switch nargin
                case 1
                    visible = 1;
                    type    = 1;
                    holdfig = 1;
                case 2
                    type    = 1;
                    holdfig = 1;
                case 3
                    type = 1;
            end
            
            % Create return
            hAxs  = zeros(n,1);
            hFigs = zeros(n,1);
            
            if holdfig == 1
                holdstatus = 'on';
            else
                holdstatus = 'off';
            end
            
            if type % normal plot
                for k=1:n
                    [hAxs(k), hFigs(k)] = inout.output.createfig(visible);
                end
            else % subplot
                hAxs = inout.output.createfig(visible);
                subplot(hAxs);
                
                hAxs = kron(hAxs,ones(n,1));
                m = round(n/2);
                n = 2;
                p = 0;
                for k=1:n
                    p       = p+1;
                    hAxs(k) = subplot(m,n,p);
                    hold(hAxs(k),holdstatus);
                end
            end
        end

