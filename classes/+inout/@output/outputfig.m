function ack = outputfig(filename, handles, format, beautify)
    %OUTPUTFIG saves the figure to the disk and exports it in PNG format
    %   By default, this function tries to save and export the figures given in
    %   the HANDLES vector, using FILENAME cell as the target. The format to be
    %   used can also be specified as an optional parameter.
    %   The output ACKs that all figures where succesfully saved.
    %
    %   DEPENDENCIES
    %   For better figure output (might be slower), please download the
    %   export_figure package present in file exchange.
    %   http://www.mathworks.com/matlabcentral/fileexchange/23629-exportfig
    %
    %   INPUT
    %   FILENAME - name to use when saving the figure to the disk
    %   FIG      - figure's handle
    %   [FORMAT] - default format is set to PNG
    %
    %   OUTPUT
    %   ACK      - signals if figure was saved with success
    %
    %   AUTHOR
    %   Pedro Silva
    %   Tampere University of Technology, 2013
    import inout.*;
    
    % Parameter check
    nn = size(filename,1);
    nf = size(handles,1);
    if nn~=nf
        appendnb = 1;
    else
        appendnb = 0;
    end
    
    % Default format (for export_fig)
    if ~exist('format','var')
        format = '-pdf';
    end
    
    if strcmpi(format,'-pdf') || strcmpi(format,'-eps')
        use_export = 1;
    else
        use_export = 0;
    end
    
    if isempty(beautify)
        beautify = 1;
    end
    
    % Assume it succeeds
    ack = 1;
    for k=1:max(size(handles))
        
        % Skip empty handles and sets name
        if handles(k)== 0, continue; end;
        if appendnb
            name = [filename,'_fig_',num2str(k,'%02d')];
        else
            if k==1
                name = filename;
            else
                name = filename{k};
            end
        end
        
        % If binary saving fails, skip all
        %         try
        %             saveas(handles(k),name); % Save's .fig
        %         catch
        %             disp('Could not save .fig');
        %         end
        try % Might not be present
            if beautify
                inout.output.beautifyfig(handles(k));
            end
        end
        if use_export == 1
            try
                export_fig(name,format,'-a4','-transparent','-q101',get(handles(k),'Parent'),'-opengl'); % if not figure, then only axis gets exported
                cleanfigure('handle',get(handles(k),'Parent'));
                matlab2tikz('filename',[name,'.tex'],'figurehandle',get(handles(k),'Parent'),'height', '\figureheight','width','\figurewidth');
            catch % normal MATLAB command
                print(get(handles(k),'Parent'),'-dpng',name);
            end
        else
            print(get(handles(k),'Parent'),'-dpng',name);
        end
    end
    
end
