function beautifyfig(hAxis,Nx,Ny)
    %BEAUTIFYFIGURE tweaks several figure parameters
    %   This script tweaks figure's visual elements in order to provide a more
    %   pleasing end result.
    %
    %   USAGE
    %   beautifyfigure(hfig)
    %
    %   INPUT
    %   hfig  - figure handle (Parent will be searched for figure handle)
    %
    %   CREDITS
    %   http://blogs.mathworks.com/loren/2007/12/11/making-pretty-graphs/
    %
    %   AUTHOR
    %   Pedro Silva
    %   Tampere University of Technology
    
    % Test figure handle
    if ~exist('Nx','var')
        Nx = 10;
    end
    if ~exist('Ny','var')
        Ny = 5;
    end
    
    % Set all text to LaTeX intepreter
    set(findall(hAxis,'type','text'),...
        'Interpreter','Latex',...
        'FontName','Times New Roman',...
        'fontSize',11)
    
    
    hFig    = get(hAxis,'Parent');
    h       = get(hFig,'children');
    hLegend = [];
    for k = 1:length(h)
        if strcmpi(get(h(k),'Tag'),'legend')
            hLegend = h(k);
            break;
        end
    end
%     set(get(hAxis,'children'),'Linewidth',1);
%     set(hAxis,'Linewidth',1);
    grid(hAxis,'on');
    box(hAxis,'off');
    
    hTitle  = get(hAxis,'title');
    hXLabel = get(hAxis,'xlabel');
    hYLabel = get(hAxis,'ylabel');
    [x,y]   = inout.output.getData(hAxis);
%     set(hTitle,'backgroundcolor','none','color','k');
%     set(hAxis, 'Color', 'none'); % Sets axes background
% %     set(hFig,'Parent', 'Color', 'none'); % Sets axes background
%     set(hLegend,'color','none');
    
    set( hAxis                      , ...
        'FontName'   , 'Times New Roman' );
    set([hTitle, hXLabel, hYLabel], ...
        'FontName'   , 'Times New Roman');
    
    
    set([hLegend] , ...
        'FontName','Times New Roman',...
        'FontSize'   , 10           );
    set([hXLabel, hYLabel]  , ...
        'FontName','Times New Roman',...
        'FontSize'   , 10          );
    set( hTitle                    , ...
        'FontName','Times New Roman',...
        'FontSize'   , 12          , ...
        'FontWeight' , 'bold'      );
    
    set(hAxis, ...
        'FontName','Times New Roman',...
        'Box'         , 'off'     , ...
        'TickDir'     , 'out'     , ...
        'TickLength'  , [.02 .02] , ...
        'XMinorTick'  , 'on'      , ...
        'YMinorTick'  , 'on'      , ...
        'YGrid'       , 'on'      , ...
        'Xgrid'       , 'on'      , ...
        'LineWidth'   , 1         );
            %'XColor'      , [.3 .3 .3], ...
        %'YColor'      , [.3 .3 .3], ...
    if iscell(x)
        x = x{1};
    end
    if iscell(y)
        y = y{1};
    end
    try
%     if ~isempty(x)
%         set(hAxis, ...
%             'Xtick'       , round(linspace(floor(min(x)),ceil(max(x)),Nx)), ...
%             'YTick'       , linspace(floor(min(y)),ceil(max(y)),Ny));
%     end
    end

end