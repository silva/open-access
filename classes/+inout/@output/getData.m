function [ x, y, z ] = getData( hFigAxis )
    %GETDATA retrieves X, Y and Z data from figure
    %   Receives a Axis handle
    %   Pedro Silva
    
    ch = get(hFigAxis,'children');
    x  = get(ch,'xData');
    y  = get(ch,'yData');
    z  = get(ch,'zData');
end

