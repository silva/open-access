function matobj = createMatfile( dir, filename, writable, overwrite)
    % createMatfile creates object for matfile
    
    if ~exist('writable','var')
        writable = true;
    end
    
    if ~strcmp(filename(end-3:end),'.mat')
        filename = [filename,'.mat'];
    end
    
    file = fullfile(dir,filename);
    if overwrite && exist(file,'file')
        delete(file);
    end
    matobj = matfile(file,'Writable',writable);
    
end

