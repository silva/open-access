classdef output < handle
    %OUTPUT handles content generatio
    %   This class provides methods to deal with figure managment, such as
    %   proper creation of handles, fetching markers, creating colorbars,
    %   generating video files, among others
    %
    %   Pedro Silva
    
    properties
        hAxis
        vObj
        dirStorage
        dirResults
        dirTemp
    end
    properties (Dependent = true)
        xlabel
        ylabel
        title
    end
    
    methods
        function obj = output(dirStorage,dirResults)
            % Constructor
            if nargin == 1
                dirResults = './results/';
            elseif nargin == 0
                dirStorage = './storage/';
                dirResults = './results/';
            end
            
            % checks if they exist
            if ~exist(dirStorage,'dir')
                mkdir(dirStorage);
            end
            if ~exist(dirResults,'dir')
                mkdir(dirResults);
            end
            
            obj.dirStorage = dirStorage;
            obj.dirResults = dirResults;
        end
    end
    
    %%% STATIC
    methods(Static)
        
        beautifyfig(hAxisFig); % beautify figures
        [X,Y,Z]      = getData(hAxisFig);
        [hAxis,hFig] = createfig(visible);
        [hAxis,hFig] = createnfig(n,visible,holdfig,type);
        ack          = outputfig(filename, handles, format, beautify);
       
         
        video        = createVideo(filename,framerate);
        captureframe(vObj, hScreen);
        
        animateVector(vector,speed);

        mObj = createMatfile(dir,filename,writable,overwrite);

        function writeIn(obj, pos, txt)
            if isempty(obj)
                h=gca;
                set(h,'Parent',gcf);
            end
            for k = 1:size(pos,1)
                text(pos(k,1)-0.5,pos(k,2),txt(k,:))
            end
            
        end
        function showGrid(obj, grid, marker, color, Msize,unit)
            if isempty(obj)
                h=gca;
                set(h,'Parent',gcf);
            end
            
            if ~exist('Msize','var')
                Msize = 8;
            end
                
            
            % Prints scenario
            wall  = [];
            [j,i] = size(grid);
            for x=1:j
                for y=1:i
                    point = grid(x,y);
                    if point == 0
                        continue;
                    
                    elseif isinf(point)
                        wall = [wall;x,y];
                        continue;
                    else
                    plot(x,y,'marker',marker{point},'color',color{point},'MarkerfaceColor',color{point},'MarkerSize',Msize);
                    end
                end
            end
            try
            x1=min(wall(:,1));
            x2=max(wall(:,1));
            y1=min(wall(:,2));
            y2=max(wall(:,2));
            
            vl = y1:unit:y2;
            plot(ones(size(vl)).*x1,(y1:unit:y2),'-k','lineWidth',2);
            plot(ones(size(vl)).*x2,(y1:unit:y2),'-k','lineWidth',2);
            hl = x1:unit:x2;
            plot(hl,y1.*ones(size(hl)),'-k','lineWidth',2);
            plot(hl,y2.*ones(size(hl)),'-k','lineWidth',2);
            
            title(['Grid ',num2str(j-1),'x',num2str(i-1),' Room ',num2str(x2-x1),'x',num2str(y2-y1)])
            end
            xlabel('x (m)')
            ylabel('y (m)')
        end
        
        function draw_path(hAxis,walk,color,overlay,xlen,ylen)
            M = length(walk);
            for s=2:M
                if nargin == 6 && ~isempty(overlay)
                    contourf(reshape(overlay(:,s),xlen,ylen)')
                end
                plot(hAxis,walk(s-1,1),walk(s-1,2),'wx');
                plot(hAxis,walk(1:s,1),walk(1:s,2),'-','color',color);
                plot(hAxis,walk(s,1),walk(s,2),'x','color',color);
                
                
                drawnow
                pause(0.1);
            end
            plot(hAxis,walk(s,1),walk(s,2),'wx');
            plot(hAxis,walk(s,1),walk(s,2),'o','MarkerFaceColor',color,'color',color);
            plot(hAxis,walk(1,1),walk(1,2),'bo','MarkerFaceColor','w');            
        end
        
        function draw_two_path(hAxis,aWalk,bWalk,aColor,bColor,savetovideo,hVid)
            M = size(aWalk,1);
            for s=2:M
%                 if nargin == 6 && ~isempty(overlay_img)
%                     contourf(reshape(overlay_img(:,s),xlen,ylen)')
%                 end
                plot(hAxis,aWalk(s-1,1),aWalk(s-1,2),'wx');
                plot(hAxis,aWalk(1:s,1),aWalk(1:s,2),'-','color',aColor);
                plot(hAxis,aWalk(s,1),aWalk(s,2),'x','color',aColor);
                
                plot(hAxis,aWalk(s-1,1),aWalk(s-1,2),'wx');
                plot(hAxis,aWalk(1:s,1),aWalk(1:s,2),'-','color',aColor);
                plot(hAxis,aWalk(s,1),aWalk(s,2),'x','color',aColor);
                
                plot(hAxis,bWalk(s-1,1),bWalk(s-1,2),'wp');
                plot(hAxis,bWalk(1:s,1),bWalk(1:s,2),'-','color',bColor);
                plot(hAxis,bWalk(s,1),bWalk(s,2),'p','color',bColor);
                
                plot(hAxis,bWalk(s-1,1),bWalk(s-1,2),'wp');
                plot(hAxis,bWalk(1:s,1),bWalk(1:s,2),'-','color',bColor);
                plot(hAxis,bWalk(s,1),bWalk(s,2),'p','color',bColor);
                drawnow
                if savetovideo
                    hVid.addToVideo(gcf);
                end
                pause(0.1);
            end
            plot(hAxis,aWalk(s,1),aWalk(s,2),'wx');
            plot(hAxis,aWalk(s,1),aWalk(s,2),'o','MarkerFaceColor',aColor,'color',aColor);
            plot(hAxis,bWalk(s,1),bWalk(s,2),'o','MarkerFaceColor',bColor,'color',bColor);
            plot(hAxis,aWalk(1,1),aWalk(1,2),'bo','MarkerFaceColor','w');            
        end
        
        
    end
    
    methods
        
        
        
        function hAxis = figure(obj,visible)
            if nargin == 1
                visible = 1;
            end
            hAxis = inout.output.createfig(visible);
            inout.output.beautifyfig(hAxis);
            obj.hAxis = hAxis;
        end
        
        function hAxis = nfigure(obj,N,visible)
            if nargin == 2
                visible = 1;
            end
            
            hAxis = inout.output.createnfig(N,visible,1,1);
            for k=1:N
                inout.output.beautifyfig(hAxis(k));
            end
            obj.hAxis = hAxis;
        end
        
        function video(obj,filename,framerate)
            import inout.*;      % Adds package
            import inout.output; % adds class in package
            obj.vObj = obj.createVideo(filename,framerate);
        end
        
        function addToVideo(obj,frame)
            %addToVido adds frame to video object
            obj.captureframe(obj.vObj, frame);
        end
        
        function animateEstimation(obj,idx,walk,anchors,epos,V,xlim,ylim,delay)

            if ~exist('delay','var')
                delay = 0.3;
            end
            
            if ~exist('V','var')
                V=[];
            end
            
            if ~exist('xlim','var')
                xlim=Inf;
            end
            
            if ~exist('ylim','var')
                ylim=Inf;
            end
            
            if any(any(isnan(V))) || any(any(isinf(V)))
                V = inv(eye(size(V)));
            end
            
            plot(anchors(:,1),anchors(:,2),'ks','MarkerfaceColor','k'); % color code this!
            plot(epos(1),epos(2),'og','MarkerSize',8,'MarkerfaceColor','g');
            plot(walk(1:idx,1),walk(1:idx,2),'b-.','MarkerSize',8);
            plot(walk(idx,1),walk(idx,2),'bx','MarkerSize',8);
            
            if ~isempty(V)
                r_ellipse = math.createCovEllipse(V);            
                plot(r_ellipse(:,1) + epos(1),r_ellipse(:,2) + epos(2),'k-')
            end
            
            if any(epos>xlim) || any(epos>ylim)
                aux=get(gcf,'color');
                set(gcf,'color',[ 1 0 0]);
                pause(0.05);
                set(gcf,'color',aux);
            end
                
            drawnow;
            pause(delay);
            if ~isempty(obj.vObj)
                obj.captureframe(obj.vObj);
            end
            plot(walk(idx,1),walk(idx,2),'wx','MarkerSize',8);
            plot(epos(1),epos(2),'ow','MarkerSize',8,'MarkerfaceColor','w');
            if ~isempty(V)
                plot(r_ellipse(:,1) + epos(1),r_ellipse(:,2) + epos(2),'w-')
            end
        end
        
        function candle(obj,varargin)
            mpval=-Inf;
            mmval=+Inf;
            
            for k=3:2:nargin-1
                mpval = max([max(max(varargin{k})),mpval]);
                mmval = min([min(min(varargin{k})),mmval]);
            end
            
            for k=3:2:nargin-1
                figure;
                boxplot(varargin{k},'plotstyle','compact','symbol','k.','medianstyle','line');
                title(varargin{k+1});
                xlabel(varargin{1});
                ylabel(varargin{2});
                ylim([1+fix(mmval) 1+fix(mpval)]);
            end
            
            
        end
        
        function print(obj,filename,format,beautify)
            if ~exist('format','var')
                format = 'png';
            end
            if ~exist('beautify','var')
            beautify = true;
            end
           inout.output.outputfig(filename,obj.hAxis, format, beautify);
        end
        
        
        function close(obj)
            if ~isempty(obj.hAxis)
                close(obj.hAxis);
            end
            if ~isempty(obj.vObj)
                close(obj.vObj);
            end
        end
        
        function set.xlabel(obj,txt)
            xlabel(obj.hAxis,txt,'interpreter','Latex')
        end
        
        function set.ylabel(obj,txt)
            ylabel(obj.hAxis,txt,'interpreter','Latex')
        end
        
        function set.title(obj,txt)
            title(obj.hAxis,txt,'interpreter','Latex')
        end
        
        function beautify(obj)
            inout.output.beautifyfig(obj.hAxis);
        end
    end
    
end

