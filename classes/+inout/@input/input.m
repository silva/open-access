classdef input < handle
    %INPUT handles import of content 
    %   This class provides methods to deal with files, streams and other
    %   sources of content
    %
    %   Pedro Silva
    
    properties(Access = private)
        in_dir
        mfile
        nb_mfiles
        fid
        tline
    end
    
    properties
       listing 
       nfiles
    end


    methods
        function obj = input(dirInput)
            if ~isempty(dirInput) || ~isnan(dirInput)
                obj.in_dir = dirInput;
            end
            if obj.in_dir(end)=='\' || obj.in_dir(end)=='/'
               %do nothing
            else
               error('Invalid pathname'); 
            end
        end
    end
    
    %%% STATIC
    methods(Static)
    
    end
    
    methods
        function [list,nfile] = getMFileList(obj,type,disp_on)
            if nargin == 1
                type = '*.m';
            end
            list          = dir(fullfile(obj.getDir(), type));
            nfile         = length(list);
            obj.mfile     = list;
            obj.nb_mfiles = nfile;
            if exist('disp_on','var') && disp_on
            obj.list();
            end
            obj.nfiles    = nfile;
        end
    
        function dir_str=getDir(obj)
            dir_str = obj.in_dir;
        end
        
        function openfile(obj,filename)
            obj.fid = fopen([obj.in_dir,filename]);
            assert(obj.fid > 0);
        end
       
        function openfidx(obj,idx)
            obj.fid = fopen([obj.in_dir,obj.mfile(idx).name]);
            assert(obj.fid > 0);
        end
        
        function file=getfname(obj,idx)
           file=[obj.in_dir,obj.mfile(idx).name];
        end
        
        function tline = getline(obj)
            tline      = fgetl(obj.fid);
            obj.tline  = tline;
        end
        
        function fields=getStrFields(obj,string)
            tmp    = regexp(string,'([^ ,:]*)','tokens');
            fields = cat(2,tmp{:});
        end
        
        function files=fullpathlist(obj,type,disp_on)
            if nargin == 1
                type = '*.m';
            end
            
            if ~exist('disp_on','var')
                disp_on = 0;
            end
            
            if isempty(obj.nb_mfiles)
                obj.getMFileList(type,disp_on);
            end
            obj.listing = cell(obj.nb_mfiles,1);
            for k=1:obj.nb_mfiles
                if disp_on
                    disp([num2str(k),': ',obj.mfile(k).name]); 
                end
               obj.listing{k} = [obj.in_dir,obj.mfile(k).name];
            end
            files = obj.listing;
        end
        
        
        function files=list(obj,type)
            if nargin == 1
                type = '*.m';
            end
            if isempty(obj.nb_mfiles)
                obj.getMFileList(type),
            end
            obj.listing = cell(obj.nb_mfiles,1);
            for k=1:obj.nb_mfiles
               disp([num2str(k),': ',obj.mfile(k).name]); 
               obj.listing{k} = obj.mfile(k).name;
            end
            files = obj.listing;
        end
                
        function close(obj)
            fclose(obj.fid);
        end
        
    end
    
end

