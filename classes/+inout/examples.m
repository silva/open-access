%%%%%
%
%   example.m
%
%   Script with several examples on how to use the inout package
%
%   Author
%   Pedro Silva
%
%   NOTE;
%   run it outside the package if you get errors
%   Last revision: January 2015
close all
clear all

% call import to add the package to your path
import inout.*;      % Adds package
import inout.output; % adds class in package


% Use the object based it you want to print
hfig = output.createfig(1); % Static members still need the class name
plot(1:100,randn(1,100))
xlabel('some time');
ylabel('some data');
title('some title');
output.beautifyfig(hfig);


%%% As an object 
hOut = output(); % or explicitly inout.output()
hOut.figure(1); % 0 / 1 sets figure invisible / invisible
hOut.xlabel = 'some time';
hOut.ylabel = 'some data';
hOut.title  = 'some title';
hOut.beautify();
plot(1:100,randn(1,100))

hOut.print('name','-png')

% And if you need several ones
hMult = output(); % or explicitly inout.output()
hMult.nfigure(10); % 0 / 1 sets figure invisible / invisible

for k = 1:10
   
    hFig = hMult.hAxis(k); % handle for fig k
    plot(hFig,1:100,randn(1,100))
    
end

%once you're ready just print them
mkdir('lovely')
hMult.print('./lovely/name','-png') % it appends a number to each figure
