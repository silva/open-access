classdef (Abstract) fingerprint < handle
    
    properties(Constant)
        a_hybrid = 2;
    end
    
    
    methods(Static)
        
        function llh = likelihood(sigma,Pdiff)
            llh = 1/sqrt(2*pi*sigma^2)*exp(-(Pdiff).^2/(2*sigma.^2));
        end
        
        
        function estimate(hStorage,fingerprints,em_sel,nbFps,nbEm,db,db_pos,sigma,nbNeighbours)
            % now I want to get the likelihood for each grid point
            head  = 1;
            len   = nbEm;
            fplen = length(em_sel);
            hStorage.pest = NaN(nbFps,2);
            for k = 1:nbFps
                % For each FP chunk,
                fps  = fingerprints.meas(head:head+len-1,1); %contains fps for all
                
                % break this into smaller loops
                rxd  = bsxfun(@minus,db,repmat(fps(em_sel),size(db,1)/fplen,1));
                llh  = radio.fingerprint.likelihood(sigma,rxd); % likelihood vector for the n-th fingerprint
                
                % computes the cost and rerieves the index
                if ~all(isnan(llh))
                    score = nansum(reshape(llh,fplen,length(llh)/fplen),1); % reshape into N_AP x board and sums rows
                    [~,pos] = sort(score,'descend'); % kudos Simona, sort keeps the index information from the vector
                    hStorage.pest(k,:) = mean(db_pos(pos(1:nbNeighbours),1:2),1);
                else
                    %does nothing, since it has been prealocated with NaN
                end
                % compute the likehood for each db entry.
                head  = head+len;
            end
        end
        
        function estimateHybrid(hStorage,fingerprints,db_pos,nbFps,db_wifi,em_sel_wifi,sigma_wifi,db_rfid,em_sel_rfid,sigma_rfid,nbNeighbours)
            
            % Compute llh for Wifi
            head  = 1;
            hStorage.pest = NaN(nbFps,2);
            nbEm_wifi     = length(em_sel_wifi);
            nbEm_rfid     = length(em_sel_rfid);
            
            
            % For all the fingerprints
            for k = 1:nbFps
                
                % Compute the difference for each app
                % WIFI part
                fps_wifi = fingerprints.meas(head:head+nbEm_wifi-1,1); %contains fps for all
                rxd      = bsxfun(@minus,db_wifi,repmat(fps_wifi,size(db_wifi,1)/nbEm_wifi,1));
                llh_wifi = radio.fingerprint.likelihood(sigma_wifi,rxd); % likelihood vector for the n-th fingerprint
                
                head  = head+nbEm_wifi;
                
                % RFID part
                fps_rfid = fingerprints.meas(head:head+nbEm_rfid-1,1); %contains fps for all
                rxd      = bsxfun(@minus,db_rfid,repmat(fps_rfid,size(db_rfid,1)/nbEm_rfid,1));
                llh_rfid = radio.fingerprint.likelihood(sigma_rfid,rxd); % likelihood vector for the n-th fingerprint
                
                head  = head+nbEm_rfid;
                
                % computes the cost and rerieves the index
               
                % Compute Gama
                score_wifi = nansum(reshape(llh_wifi,nbEm_wifi,length(llh_wifi)/nbEm_wifi),1); % reshape into N_AP x board and sums rows
                score_rfid = nansum(reshape(llh_rfid,nbEm_rfid,length(llh_rfid)/nbEm_rfid),1); % reshape into N_AP x board and sums rows
                
                fps_rfid(isnan(fps_rfid),1)=-Inf;
                
                % w is either 1 or 0
                if max(fps_wifi)-radio.fingerprint.a_hybrid > max(fps_rfid)
                    score = score_wifi; %w = 1;
                else
                    score = score_rfid; %w = 1;
                end
                
                [~,pos] = sort(score,'descend'); % kudos Simona, sort keeps the index information from the vector
                hStorage.pest(k,:) = mean(db_pos(pos(1:nbNeighbours),:),1);
                
                
            end
            
        end
        
        function estimateJoint(hStorage,fingerprints,db_pos,nbFps,db_wifi,em_sel_wifi,sigma_wifi,db_rfid,em_sel_rfid,sigma_rfid,nbNeighbours)
            
            % Compute llh for Wifi
            head  = 1;
            hStorage.pest = NaN(nbFps,2);
            nbEm_wifi     = length(em_sel_wifi);
            nbEm_rfid     = length(em_sel_rfid);
            
            for k = 1:nbFps
                
                % WIFI part
                fps      = fingerprints.meas(head:head+nbEm_wifi-1,1); %contains fps for all
                rxd      = bsxfun(@minus,db_wifi,repmat(fps,size(db_wifi,1)/nbEm_wifi,1));
                llh_wifi = radio.fingerprint.likelihood(sigma_wifi,rxd); % likelihood vector for the n-th fingerprint
                head  = head+nbEm_wifi;
                
                % RFID part
                
                fps      = fingerprints.meas(head:head+nbEm_rfid-1,1); %contains fps for all
                rxd      = bsxfun(@minus,db_rfid,repmat(fps,size(db_rfid,1)/nbEm_rfid,1));
                llh_rfid = radio.fingerprint.likelihood(sigma_rfid,rxd); % likelihood vector for the n-th fingerprint
                
                % compute the likehood for each db entry.
                head  = head+nbEm_rfid;
                
                
                % computes the cost and rerieves the index
                
                
                % Compute Gama
                score_wifi = nansum(reshape(llh_wifi,nbEm_wifi,length(llh_wifi)/nbEm_wifi),1); % reshape into N_AP x board and sums rows
                score_rfid = nansum(reshape(llh_rfid,nbEm_rfid,length(llh_rfid)/nbEm_rfid),1); % reshape into N_AP x board and sums rows
                    
                score  = score_wifi+score_rfid;
                
                [~,pos] = sort(score,'descend'); % kudos Simona, sort keeps the index information from the vector
                hStorage.pest(k,:) = mean(db_pos(pos(1:nbNeighbours),:),1);
                
                
            end
            
        end
        
        
        
    end
    
    methods
        
        function obj = fingerprint(varargin)
            %nothing is done
        end
        
        
    end
    
end