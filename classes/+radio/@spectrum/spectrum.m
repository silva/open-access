classdef spectrum < handle
    %SPECTRUM<HANDLE aggregates spectrum information
    %
    %   CONSTRUCTOR
    %    Spectrum()
    %
    %
    %   Tampere University of Technology
    %   Pedro Silva, 2014
    %
    %   TODO
    %   Currently RSS can have different size! make sure it is the
    %   reference size
    
    %%% VARIABLES
    %%%%% PRIVATE
    %%%%% PUBLIC
    %%%%% PROTECTED
    properties (Constant)
        wifi  = 1;
        rfid  = 2;
        ble   = 3;
        uwb   = 4;
        light = 5;
        lte   = 6;
        debug = 0;
    end
    
    properties (Constant, Access = private)
        idx_train = 1;  % (#) training index for shadowing matrix
        idx_acq   = 2;  % (#) acquisition index for shadowing matrix
    end
    
    properties
        id_counter      % ([]) next free id
        radiomaps       % ([]) matrix containing all the RADIO     maps for each emitter/technology
        distmaps        % ([]) matrix containing all the RADIO     maps for each emitter/technology
        shadowing       % ([]) matrix containing all the N-SHADOWING maps for each emitter/technology
        limits          % ([]) contains the limits of the area
        unit            % (#)  how many meters each position should represent
        emitter         % ([]) contains list of ID, techtype and positon?
        rm_size         % (#)  radio maps size
        rm_pos          % ([]) points acquired in the database
        tx_model        % ([]) transmission model for radio maps
        storage         % (p)  object for mfile
    end
    
    properties(Dependent)
        nbEmitters      % (#) Total number of emitter being considered
    end
    
    properties
        x               % x coordinates
        y               % y coordinates
        z               % z coordinates
        gdop            % Dillution of precision for object location
    end
    
    
    %%%%% ABSTRACT
    methods(Abstract)
        %         survey(obj,varargin);
    end
    
    %%% CONSTRUCTOR
    methods
        function obj = spectrum(limits,unit,varargin)
            obj.limits     = limits;
            obj.unit       = unit;
            obj.id_counter = 1;
            
            % Compute starting and ending points
            scale       = obj.unit;
            limit       = obj.limits;
            x1          = limit(1,1); %x1
            y1          = limit(1,2); %x2
            z1          = limit(1,3); %x2
            x2          = limit(2,1); %y1
            y2          = limit(2,2); %y2
            z2          = limit(2,3); %x2
            
            % Generates a matrix table with distance
            xlen = length(x1:scale:x2);
            ylen = length(y1:scale:y2);
            zlen = length(z1:scale:z2);
            
            obj.x = repmat(x1:scale:x2,xlen,1)';
            obj.y = repmat(y1:scale:y2,ylen,1);
            if zlen ~= xlen
                obj.z = obj.x.*0;
                obj.rm_size = [xlen ylen];
            else
                obj.z = repmat(z1:scale:z2,zlen,1);
                obj.rm_size = [xlen ylen zlen];
            end
            
            if nargin>=3
                obj.storage = varargin{1};
            end
            
            if nargin>=4
                obj.tx_model = varargin{2};
            end
            
        end
        function reset(obj)
            obj.radiomaps =[];
            obj.shadowing =[];
            obj.distmaps  =[];
        end
    end
    
    %%% METHODS
    %%%%% PRIVATE
    methods(Access = private, Static)
        
    end
    
    methods(Access=private)
        function wipeRadioMap(obj,id,orientation)
            Em  = obj.emitter;
            pos = Em{2}(id,1:3);
            x = fix(pos(1));
            y = fix(pos(2));
            
            switch(orientation)
                case 'up'
                    obj.radiomaps(obj.y < pos(2),id) = NaN;
                case 'down'
                    obj.radiomaps(obj.y > pos(2),id) = NaN;
                case 'left'
                    obj.radiomaps(obj.x > pos(1),id) = NaN;
                case 'right'
                    obj.radiomaps(obj.x < pos(1),id) = NaN;
                case '90-right-down'
                    obj.wipeRadioMap('right');
                    obj.wipeRadioMap('down');
                case '90-left-down'
                    obj.wipeRadioMap('left');
                    obj.wipeRadioMap('down');
                case '90-right-up'
                    obj.wipeRadioMap('right');
                    obj.wipeRadioMap('up');
                case '90-left-up'
                    obj.wipeRadioMap('left');
                    obj.wipeRadioMap('up');
            end
        end
    end
    
    %%%%% PUBLIC
    methods(Static)
        
        
        function str = getInterfaceName(intId)
            switch(intId)
                case radio.spectrum.wifi
                    str = 'WIFI';
                case radio.spectrum.rfid
                    str = 'RFID';
                otherwise
            end
        end
        
        function [IntId,hFunc] = getInterfaceIdentity(strList)
            %GETINTERFACEID converts text to integer represenation
            %
            %   How to access the handles: hFunc(k) <= hFunc{3}{1}(k)
            %
            lenList          = size(strList,1);
            IntId(lenList,1) = -1;
            hFunc            = cell(size(strList,1),1);
            for k=1:lenList
                switch(upper(strList(k,1:3)))
                    case 'WIF', %WIFI
                        IntId(k) = radio.spectrum.wifi;
                        hFunc{k} = [{@radio.pathloss.wifi_model_params};
                            {@radio.pathloss.wifi_rss};...
                            {@radio.pathloss.wifi_shadowing}];
                    case 'RFI', %RFID
                        IntId(k)   = radio.spectrum.rfid;
                        hFunc{k}   = [{@radio.pathloss.rfid_model_params};
                            {@radio.pathloss.rfid_rss};...
                            {@radio.pathloss.rfid_shadowing}];
                    case 'BLE', %BLUETOOTH
                        IntId(k) = radio.spectrum.ble;
                        hFunc{k} = NaN;
                    case 'UWB', %UWB
                        IntId(k) = radio.spectrum.uwb;
                        hFunc{k} = NaN;
                    case 'LTE', %LTE
                        IntId(k) = radio.spectrum.lte;
                        hFunc{k} = NaN;
                    otherwise
                        error('spectrum:setInterfaceIdentity','radio type unknown');
                end
            end
            hFunc = {IntId, hFunc}; %tag function with interface id
        end
        
        function IntId = inter2id(strList)
            %GETINTERFACEID converts text to integer represenation
            lenList = size(strList,1);
            IntId(lenList,1) = -1;
            for k=1:lenList
                switch(upper(strList(k,1:3)))
                    case 'WIF', %WIFI
                        IntId(k) = 1;
                    case 'RFI', %RFID
                        IntId(k) = 2;
                    case 'BLE', %BLUETOOTH
                        IntId(k) = 3;
                    case 'UWB', %UWB
                        IntId(k) = 4;
                    case 'LTE', %LTE
                        IntId(k) = 5;
                    otherwise
                        error('radio type unknown');
                end
            end
        end
    end
    
    
    
    
    %%%%% PUBLIC
    methods
        
        function fixDirection(obj,id,location)
            
            nbEms = length(id);
            
            if strcmpi(location,'CORNER')
                %do nothing
                return
            end
            
            switch (nbEms)
                
                case 1
                    obj.direction(id,'up');
                    
                case 2
                    obj.direction(id(1),'up');
                    obj.direction(id(2),'down');
                    % correct
                    
                case 4
                    % i do not think it is necessary to switch according to middle
                    obj.direction(id(1),'up');
                    obj.direction(id(2),'down');
                    obj.direction(id(3),'right');
                    obj.direction(id(4),'left');
            end
            
        end
        
        
        function direction(obj,id,direction)
            obj.wipeRadioMap(id,direction);
        end
        
        function create(obj,type,emitter)
            %CREATE creaes N radio maps for each emmitter given
            % [(x,y,(z)),tech,id]
            bEm = NaN(size(emitter,1),size(emitter,2)+2);
            
            % Update the counter
            for id = obj.id_counter:length(emitter)
                
                bEm(id,:) = [emitter(id,:),id];
                
                % create radio map
                obj.survey(emitter(1:3,type(id,:)));
                
            end
            
            % Store information
            obj.id_counter = id+1; % creates next ID
            if isempty(obj.emitter)
                obj.emitter = bEm;
            else
                obj.emitter = [obj.emitter,bEm];
            end
            
        end
        
        
        function insert(obj,id,radio_map,shadowing,distance_map)
            %INSERT stores in the database the radio maps
            %   MAPS are vectorised!
            obj.radiomaps(:,id) = radio_map;
            obj.shadowing(:,id) = shadowing;
            obj.distmaps(:,id)  = distance_map;
        end
        
        
        function [map,dm] = retrieve(obj,id,ideal)
            %RETRIEVE returns the maps with corrsponding shadowing
            %  Each radiomap is a collumn
            %  There is 2 collumns for the shadowing
            
            if isempty(obj.radiomaps)
                obj = obj.storage;
            end
            
            map = obj.radiomaps(:,id);
            if ~ideal % if true, add no shadowing
                map = map + obj.shadowing(:,id);
            end
            dm = obj.distmaps(:,id);
            
        end
        
        
        % Make a copy of a handle object.
        function new = copy(this)
            % Instantiate new object of the same class.
            new = feval(class(this));
            
            % Copy all non-hidden properties.
            p = fieldnames(struct(this));;
            for i = 1:length(p)
                new.(p{i}) = this.(p{i});
            end
        end
        
        function heard = getHeardAps(obj,pos)
            idx   = sub2ind(obj.rm_size,pos(1),pos(2));
            if isempty(obj.radiomaps)
                obj = obj.storage;
            end
            heard = obj.radiomaps(idx,:)+obj.shadowing(idx,:);
        end
        
        function [heard,nbSeen] = dumpSelection(obj,sel_ids,sensitivity)
            nbSeen = length(sel_ids);
            if isempty(obj.radiomaps)
                obj = obj.storage;
            end
            heard  = obj.radiomaps(:,sel_ids)'+obj.shadowing(:,sel_ids)';
            heard  = heard(:);
            if nargin == 3 && ~isempty(sensitivity)
                sensitivity = repmat(sensitivity,length(obj.rm_pos),1);
                heard(heard < sensitivity) = NaN;
            end
        end
        
        function heard = dumpHeardAps(obj,sensitivity)
            if isempty(obj.radiomaps)
                obj = obj.storage;
            end
            heard = obj.radiomaps'+obj.shadowing';
            % Saturates to sensitivity
            if nargin == 2 && ~isempty(sensitivity)
                for k=1:size(heard,2) % for each collumn
                    heard(heard(:,k) < sensitivity,k) = NaN;
                end
            end
            heard = heard(:); % vector with all APs measurement for each point in the data base
        end
        
       
        
        
        function survey(obj,storage)
            
            % SURVEY implements the radio map generation
            Em    = obj.getDB();
            N     = obj.nbEmitters;
            Ga    = [obj.x(:),obj.y(:),obj.z(:)];
            MxN   = [size(Ga,1) 1]; % build one shawdowing map for each user + training
            M     = MxN(1);
            
            if ~isempty(storage)
                storage.pos       = zeros(N,3);
                storage.L0        = zeros(N,1);
                storage.n         = zeros(N,1);
                storage.s         = zeros(N,1);
                
                storage.distmaps  = zeros(M,N);
                storage.radiomaps = zeros(M,N);
                storage.shadowing = zeros(M,N);
                
            end
            
            % for each emitter
            for k=1:N
                id   = Em{1}(k);
                pos  = Em{2}(k,1:3);
                pFct = Em{3}(k,:);
                hFct = Em{end}{k}{1};
                
                
                % Creates the rss maps
                L0            = pFct(1);
                n             = pFct(2);
                s             = pFct(3);
                
                shadMaps      = radio.pathloss.shadowing(s,MxN); % calls shadowing function
                distMaps      = zeros(M,1);
                radioMaps     = zeros(M,1);
                
                parfor m=1:M
                    distMaps(m,:)  = sqrt(sum((Ga(m,:)-pos).^2));
                    radioMaps(m,:) = hFct(distMaps(m,:),L0,n);      
                end
                
                if ~isempty(storage)
                    
                    storage.pos(id,1:3)       = pos;
                    storage.L0(id,1)          = pFct(1);
                    storage.n(id,1)           = pFct(2);
                    storage.s(id,1)           = pFct(3);
                    
                    storage.distmaps(1:M,id)  = distMaps;
                    storage.radiomaps(1:M,id) = radioMaps; % no shawdowing!
                    storage.shadowing(1:M,id) = shadMaps;  % calls shadowing function
                    
                    storage.rm_pos = Ga;
                    
                end
                
                if M < 1e3
                    obj.insert(id,radioMaps,shadMaps,distMaps); % saves to the database
                    obj.rm_pos = Ga;
                end
                
                
                if obj.debug
                    disp(' ');
                    disp('Radio map generated...');
                    disp(['ID: ',num2str(Em{1}(k))]);
                    disp(['Pos: ',num2str(pos)]);
                    disp('Function handles')
                    disp(hFct);
                    disp(['Model input: ',num2str(pFct)]);
                end
                
                clear distmaps
                clear radiomaps
                clear shadowing
            end
            
            
        end
        
        
        function surveyGPU(obj,storage)
            
            % SURVEY implements the radio map generation
            Em    = obj.getDB();
            N     = obj.nbEmitters;
            Ga    = [obj.x(:),obj.y(:),obj.z(:)];
            MxN   = [size(Ga,1) 1]; % build one shawdowing map for each user + training
            M     = MxN(1);
            
            if ~isempty(storage)
                storage.pos       = zeros(N,3);
                storage.L0        = zeros(N,1);
                storage.n         = zeros(N,1);
                storage.s         = zeros(N,1);
                
                storage.distmaps  = zeros(M,N);
                storage.radiomaps = zeros(M,N);
                storage.shadowing = zeros(M,N);
                
            end
            
            % for each emitter
            for k=1:N
                id   = Em{1}(k);
                pos  = Em{2}(k,1:3);
                pFct = Em{3}(k,:);
                hFct = Em{end}{k}{1};
                
                
                % Creates the rss maps
                L0            = repmat(pFct(1),length(Ga),1);
                n             = repmat(pFct(2),length(Ga),1);
                s             = pFct(3);
                distMaps      = sqrt(sum(bsxfun(@minus,Ga,pos).^2,2));
                radioMaps     = arrayfun(hFct,distMaps,L0,n);    % no shawdowing!
                shadMaps      = radio.pathloss.shadowing(s,MxN); % calls shadowing function
                
                if ~isempty(storage)
                    
                    storage.pos(id,1:3)       = pos;
                    storage.L0(id,1)          = pFct(1);
                    storage.n(id,1)           = pFct(2);
                    storage.s(id,1)           = pFct(3);
                    
                    storage.distmaps(1:M,id)  = distMaps;
                    storage.radiomaps(1:M,id) = radioMaps; % no shawdowing!
                    storage.shadowing(1:M,id) = shadMaps;  % calls shadowing function
                    
                    storage.rm_pos = Ga;
                    
                else
                    obj.insert(id,radioMaps,shadMaps,distMaps); % saves to the database
                    obj.rm_pos = Ga;
                    obj.DOP(Em{2}(:,1:2));
                end
                
                
                if obj.debug
                    disp(' ');
                    disp('Radio map generated...');
                    disp(['ID: ',num2str(Em{1}(k))]);
                    disp(['Pos: ',num2str(pos)]);
                    disp('Function handles')
                    disp(hFct);
                    disp(['Model input: ',num2str(pFct)]);
                end
                
                
                clear L0 n s
                clear distmaps
                clear radiomaps
                clear shadowing
            end
            
            
        end
        
        
        
        function gdop = DOP(obj,emitters)
            
            % Prepares for DOP calculation
            em_pos   = emitters(:,1:2);
            coord    = obj.getCoordinates;
            
            N        = size(em_pos,1);
            xs       = em_pos(:,1);
            ys       = em_pos(:,2);
            x        = coord(:,1);
            y        = coord(:,2);
            % dm is a distance map from every point in the grid, left -> right -> upwards
            for k=1:N
                [rm(:,k),dm(:,k)] = obj.retrieve(k,false);
            end
            
            for k=1:N
                
                dm(:,k) = dm(:,k);
                
                cdiff        = bsxfun(@minus,coord(:,1:2),em_pos(k,1:2));
                cos_phi(:,k) = (cdiff(:,1))./(dm(:,k)+ eps);
                sin_phi(:,k) = (cdiff(:,2))./(dm(:,k)+ eps);
                
                tau(:,k)     = cos_phi(:,k)./(dm(:,k)+ eps);
                rho(:,k)     = sin_phi(:,k)./(dm(:,k)+ eps);
            end
            
            
            % For the G construction
            for k=1:length(cdiff)
                taul(k,1) = sum(tau(k,:))/N;
                rhol(k,1) = sum(rho(k,:))/N;
                tsqr(k,1) = sum(tau(k,:).^2)/N;
                rsqr(k,1) = sum(rho(k,:).^2)/N;
                rtl(k,1)  = sum(tau(k,:).*rho(k,:))/N;
                
                G_down = [ ...
                    1       taul(k) rhol(k); ...
                    taul(k) tsqr(k) rtl(k); ...
                    rhol(k) rtl(k)  rsqr(k)...
                    ];
                G_up   = tsqr(k) + rsqr(k) - taul(k)^2 - rhol(k)^2;
                
                dG(k) = det(G_down);
                G(k) = sqrt(  G_up / (det(G_down) + eps) );
                
            end
            
            obj.gdop = G;
            gdop     = G;
        end
        
        
        function meas = probe(obj,posx,posy,id,shw,sense)
            %PROBE rerieves the value at the given position
            
            [~,idx] = min(sqrt(sum(bsxfun(@minus,obj.rm_pos(:,1:2),[posx posy]).^2,2)));
            if isempty(obj.radiomaps)
                obj = obj.storage;
            end
            meas = obj.radiomaps(idx,id)+shw;
            if meas < sense
                meas = NaN;
                if obj.debug
                    disp(['Not heard for ID:',num2str(id),' at <',num2str(posx),',',num2str(posy),'>'])
                end
            end
            
        end
        
         function meas = probeIdeal(obj,posx,posy,id,sense)
            %PROBE rerieves the value at the given position
            [~,idx] = min(sqrt(sum(bsxfun(@minus,obj.rm_pos(:,1:2),[posx posy]).^2,2)));
            if isempty(obj.radiomaps)
                obj = obj.storage;
            end
            meas = obj.radiomaps(idx,id);
            if meas < sense
                meas = NaN;
                if obj.debug
                    disp(['Not heard for ID:',num2str(id),' at <',num2str(posx),',',num2str(posy),'>'])
                end
            end
            
        end
        
        
        
        
    end
    %%%%% PROTECTED
    
    
    %%% GETTERS and SETTERS
    methods
        function em = getDB(obj)
            em = obj.emitter;
        end
        
        function nb = get.nbEmitters(obj)
            nb = length(obj.emitter{1});
        end
        
        function pos = getCoordinates(obj)
            pos = obj.rm_pos;
        end
        
    end
    
    methods(Access = protected)
        function setDB(obj,em,storage)
            obj.emitter = em;
            if exist('storage','var') && ~isempty(storage)
                if tools.isWritable(storage)
                    storage.emitter = em;
                end
            end
        end
    end
    
    
end

