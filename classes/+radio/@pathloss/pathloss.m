classdef(Abstract) pathloss
    
    
    properties(Constant)
        %Table obtained from [1] table 2
        wifi_freq  = 2.4e9;
        ble_freq   = 2.4e9;
        rfid_freq  = 866e6;
        umts_freq  = 2.1e9;
        
        wifi_table = [%Coef    %Pt-A  %sigma
            % Building
            1.93     -13.03  6.00 % All building floors
            2.43     4.80    6.85 % Same-floor
            % Room based
            2.10     -10.84  3.10 % All building APs (inside room M)
            1.91     -14.32  4.07 % Same-floor Aps (inside room M)
            ];
        
        %Table obtained from [1] table 3
        rfid_default_angle = 180;
        rfid_table = [%angle     %Coef    %Pt-A  %shadowing av
            36       2.46     -0.41         1.54;
            72       2.43     -1.32         1.62;
            108      2.24     -5.91         2.62;
            144      2.00     -11.49        3.69;
            180      1.78     -16.05        4.28;
            ];
        
        % Interpolation method
        interpMethod = 'spline';
    end
    
    
    
    properties
        
    end
    
    methods(Static)
        
        
        
        function [n,L,s,f] = fetchModelParameters(type,varargin)
            switch(upper(type))
                
                case 'WIFI'
                    nrow = varargin{1};
                    n = radio.pathloss.wifi_table(nrow,1);
                    L = radio.pathloss.wifi_table(nrow,2);
                    s = radio.pathloss.wifi_table(nrow,3);
                    f = radio.pathloss.wifi_freq;
                case 'RFID'
                    a = varargin{1};
                    L = interp1(radio.pathloss.rfid_table(:,1),radio.pathloss.rfid_table(:,2),a,radio.pathloss.interpMethod,'extrap');
                    n = interp1(radio.pathloss.rfid_table(:,1),radio.pathloss.rfid_table(:,3),a,radio.pathloss.interpMethod,'extrap');
                    s = interp1(radio.pathloss.rfid_table(:,1),radio.pathloss.rfid_table(:,4),a,radio.pathloss.interpMethod,'extrap');
                    f = radio.pathloss.rfid_freq;
            end
        end
        % -30 dB = 0 dBm
        
        function [cno,svar] = dist2cno(d,f,BW,G,Nfig,F2,hFunc,To)
            rss        = hFunc{1}{1}(d,f); % f in GHz
            [~,noise]  = radio.pathloss.noisePower(Nfig,BW,G); %0 dB gain
            cno        = rss - noise; %N0
            svar       = math.CRBT(10^(cno/10),F2,To);
            %svar       = math.CRBD(d,10^(cno/10),F2,[],[],To);
        end
        
        function [npdBm,npdB] = noisePower(NF,B,G)
            % Computes the noise power for bandwith B
            npdBm = -174+10*log10(B)+NF+G;
            npdB  = npdBm - 30;  % to
            %            disp(npdBm)
        end
        
        
        function [n,L,s,f] = wifi_model_params(tab_row)
            n = radio.pathloss.wifi_table(tab_row,1);
            L = radio.pathloss.wifi_table(tab_row,2);
            s = radio.pathloss.wifi_table(tab_row,3);
            f = radio.pathloss.wifi_freq;
        end
        
        function [n,L,s,f] = rfid_model_params(op_ang)
            n = interp1(radio.pathloss.rfid_table(:,1),radio.pathloss.rfid_table(:,2),op_ang,radio.pathloss.interpMethod,'extrap');
            L = interp1(radio.pathloss.rfid_table(:,1),radio.pathloss.rfid_table(:,3),op_ang,radio.pathloss.interpMethod,'extrap');
            s = interp1(radio.pathloss.rfid_table(:,1),radio.pathloss.rfid_table(:,4),op_ang,radio.pathloss.interpMethod,'extrap');
            f = radio.pathloss.rfid_freq;
        end
        
        function [n,L,s,f] = umts_indoor_model_params(varargin)
            n = [];
            L = [];
            s = [];
            f = radio.pathloss.umts_freq;
        end
        
        %%%%% Generic
        %%% Generic shadowing function
        function sigma = shadowing(s,MxNxK)
            sigma  = normrnd(0,s,MxNxK);
        end
        
        function rss = ITU_indoor_rss(d,f)
            s   = math.drawnumber(4,10,1);
            Pt  = 20-30; %dBm
            L   = 20*log10(f/1e6)+30*log10(d)-28+normrnd(0,s); %in dB
            rss = Pt-L; % in dB
        end
        
        function rss = LogDistance(d,L0,n,s)
            if ~exist('s','var')
                sv = 0;
            else
                sv = normrnd(0,s);
            end
            rss = L0 - n*10*log10(d)+sv;
        end
        
        function [L0,n,s] = commonParams(interId,env_type)
           
            N = length(interId);
            L0 = zeros(N,1);
            n  = zeros(N,1);
            s  = zeros(N,1);
            
            for k=1:N
                
                % Shadowing typical values
                s(k)  = math.drawnumber(4,10,1); % random number 4 to 10
                
                % Pathloss coefficient
                switch(upper(env_type))
                    case 'OFFICE'
                        n(k) = math.drawnumber(0.8,1,1);
                    case 'IDEAL'
                        n(k) = 1;
                end
                
                % Apparent power at 1 metre
                switch(interId(k))
                    case 1 % WIFI
                        L0(k) = math.drawnumber(-50,-40,1); % random number 4 to 10
                        
                    case 3 % BLE
                        L0(k) = math.drawnumber(-80,-70,1); % random number 4 to 10
                        
                    case 2 % RFID 
                        L0(k) = math.drawnumber(-90,-80,1); % random number 4 to 10
                        
                    otherwise
                        error('unknown type');
                        
                end
            end
        end
        
        
        
        %%%%% RFID
        function sigma = rfid_shadowing(s,MxNxK) % Name holder
            sigma    = radio.pathloss.shadowing(s,MxNxK);
        end
        
        function rss = rfid_rss(d,n,L,f,s)
            % Computes RSS for RFID device
            %
            %   USAGE
            %   rss = calcRSSRFID(d,a,f)
            %
            %   ARGUMENTS
            %   d (m)   Distance
            %   f (Hz)  Frequency
            %   s (dB)  Standard deviation
            if ~exist('s','var')
                sv = 0;
            else
                sv = normrnd(0,s);
            end
            rss = L - n*(radio.pathloss.B(f)+10*log10(2*d))+sv;
        end
        
        %%%%% UMTS
        function rss = umts_indoor_rss(d,f)
            %From http://www.mathworks.com/matlabcentral/fileexchange/42638-path-loss-calculator-for-jtg-5-6-propagation-model/content/JTG5-6/JTG5_6.m
            % Section 2.1 of Appendix 1 to Annex 2 of Rep.  ITU-R  SM.2028-1
            % Case 1: distance < 0.04 use free space path loss model for the modified
            % hata model.
            % For indoor scenarios the standard diviation of the path loss value is
            % determined based on Case 1 of Section 2.2 of Appendix 1 to Annex 2 of Rep. ITU-R
            % SM.2028-1 and the standard diviation of the building entry loss from
            % Section 4.9 of Annex 1 of ITU-R P.1812-2. The calculations are based on
            % Section 4 of Appendix 1 to Annex 2 of Rep. ITU-R SM.2028-1.
            
            h_a  = 15; % tx antenna
            lPct = 0.5; %50%
            h_2  = 1.5; % user
            
            L = 32.5 + 20*log10(f/1e6)+20*log10(d/1e6 + (h_a - h_2)^2/10^6);
            Qi_x = (-norminv(lPct/100,0,1));
            L = L + 11 + Qi_x * sqrt(3.5^2+36);
            
            Pt  = 24-30; %dB %dBm https://books.google.fi/books?id=7m-MnwW_o7AC&pg=PA175&lpg=PA175&dq=link+budget+uplink+umts&source=bl&ots=2pqmLesYRm&sig=sr50ZP_CT_PAe3XEXy4p3tTX5w4&hl=en&sa=X&ei=FszQVOWPFMLkyAPJzYJI&ved=0CD8Q6AEwBQ#v=onepage&q=link%20budget%20uplink%20umts&f=false
            rss = Pt-L;
        end
        
        %%%%% WIFI
        function sigma = wifi_shadowing(s,MxNxK) % Name holder
            sigma    = radio.pathloss.shadowing(s,MxNxK);
        end
        
        
        function rss = wifi_rss(d,n,L,f,s)
            % Computes RSS for WIFI device
            %
            %   USAGE
            %   calcRSSWIFI(d,n,L,f,s)
            %
            %   ARGUMENTS
            %   d (m)  Distance
            %   n (#)  Pathloss coefficient
            %   L (dB) Budget(L=Pt-A)
            %   f (Hz) Frequency
            %   s (dB) Standard deviation
            if ~exist('s','var')
                sv = 0;
            else
                sv = normrnd(0,s);
            end
            rss = L - n*(radio.pathloss.B(f)+10*log10(d))+sv;
        end
        
        function loss = B(f)
            % Computes the loss due to the frequency
            %
            %   USAGE
            %   loss = B(f)
            %
            %   ARGUMENTS
            %   loss (dB) Attenuation due to freq. travel
            loss = 10*log10(4*pi*f/299792458);
        end
    end
    
end

