classdef walk < handle
    %WALK contains methods for modelling human behaviour
    %
    %   % example with plot3
    %   nbSteps = 100;
    %   pvar    = 2;
    %   vvar    = [3 3 0.01];
    %   limits  = [0 0 0; 10 10 10];
    %   x=build.walk.random3DAngleWalk(nbSteps,pvar,vvar,limits)
    %   for k=1:nbSteps; plot3(x(1:k,1),x(1:k,2),x(1:k,3),'-');hold on;plot3(x(k,1),x(k,2),x(k,3),'x');hold off;drawnow;pause(0.1);end;
    properties
    end
    
    methods(Static)
        
        function walk = random3DWalk(nbSteps,pvar,vvar)
            %random3DWalk creates a random 3D walk by propagating the user
            %position and velocity with a displacement matrix (constant
            %veloctity).
            T  = 1;
            AT = [1 0 T 0 0 0;               
                0 1 0 T 0 0;
                0 0 1 0 T 0;
                0 0 0 1 0 0;
                0 0 0 0 1 0;
                0 0 0 0 0 1];
            
            % initial position and velocity
            x            = zeros(nbSteps,3*2);
            x(1,1:3)     = sqrt(pvar)*randn([1 3]);
            x(1,3+1:end) = sqrt(vvar)*randn([1 3]);
            
            % Create the walk
            for s = 2:nbSteps
                x(s,:) = AT*x(s-1,:)'; % you can add noise here
            end
            walk = x(1:3);
        end
        
        function walk = random3DAngleWalk(nbSteps,pvar,vvar,limits)
            T  = 1;
            AT =    [1 0 T 0 0 0;
                0 1 0 T 0 0;
                0 0 1 0 T 0;
                0 0 0 1 0 0;
                0 0 0 0 1 0;
                0 0 0 0 0 1];
            
            x          = zeros(nbSteps,6);
            
            % initial position and velocity
            mx         = mean(limits(:,1));
            my         = mean(limits(:,2));
            mz         = mean(limits(:,3));
            
            if length(pvar)==1
                pvar = repmat(pvar,3,1);
            end
            if length(vvar)==1
                vvar = repmat(vvar,3,1);
            end
            x(1,1)     = math.drawNumberVarMean(pvar(1),mx,1);
            x(1,2)     = math.drawNumberVarMean(pvar(2),my,1);
            x(1,3)     = math.drawNumberVarMean(pvar(3),mz,1);
            x(1,4:end) = sqrt(vvar).*randn([1 3]);
            
            % Choses an angle for later
            ang             = math.drawnumber(0.1,2*pi,1);
            rnd_sleep       = fix(math.drawnumber(40,60,1));
            s               = 2;
            redraw          = false;
            
            % Create the walk
            while s <= nbSteps
                if mod(s,rnd_sleep)==0 || redraw
                    % picks a new direction
                    switch(math.drawDiscrete(4,1,1))
                        case 1
                            x(s,1:3)     = x(s-1,1:3)+[cos(ang)*vvar(1) sin(ang)*vvar(2) sin(ang)*vvar(3)];
                        case 2
                            x(s,1:3)     = x(s-1,1:3)+[1 sin(ang)*vvar(2) sin(ang)*vvar(3)];
                        case 3
                            x(s,1:3)     = x(s-1,1:3)+[cos(ang)*vvar(1) 1 sin(ang)*vvar(3)];
                        case 4
                            x(s,1:3)     = x(s-1,1:3)+[cos(ang)*vvar(1) sin(ang)*vvar(2) 1];
                    end
                    
                    x(s,3+1:end) = sqrt(vvar).*randn([1 3]);
                    ang          = math.drawnumber(0.1,2*pi,1);
                    rnd_sleep    = fix(math.drawnumber(20,50,1));
                    redraw       = false;
                else
                    x(s,:) = AT*x(s-1,:)'; % you can add noise here
                end
                
                %redraw if not valid
                if ~geometry.isinside(x(s,1:3),limits(:,1:3))
                    ang       = math.drawnumber(0.1,2*pi,1);
                    redraw    = true;
                    continue;
                end
                s=s+1;
            end
            
            walk = x(1:3);
        end
        
        
        function walk = ramdomWalkAngle(nbSteps,limits,lang,uang,vel)
            %ramdomWalkAngle creates a walk with NBSTEPS in the direction
            %given by a random angle or the one specified by
            %angle [LANG, UANG]. It is assumed the user moves for 1 m/s
            %otherwise specified by USEL_VEL
            
            % initial position and velocity
            x      = zeros(nbSteps,2);
            
            if ~isempty(limits)
                x(1,1) = math.drawnumber((limits(2,1)-limits(1,1))/2,(limits(2,1)-limits(1,1))/2,1);
                x(1,2) = math.drawnumber((limits(2,2)-limits(1,2))/2,(limits(2,2)-limits(1,2))/2,1);
            else
                x(1,1:2) = nbSteps*randn([1 2]);
            end
            if nargin >= 5
                ang = math.drawnumber(lang,uang,1);
            else
                vel = 1;
                ang = math.drawnumber(0,2*pi,1);
            end
            
            % Create walk
            s = 2;
            while s <= nbSteps
                
                x(s,:) = x(s-1,:)+[sin(ang) -cos(ang)]*vel;
                if ~isempty(limits)
                    if ~geometry.isinside(x(s,:),limits(:,1:2)) %redraw
                        ang     = math.drawnumber(0.1,2*pi,1);
                        continue;
                    end
                end
                s = s+1;
            end
            walk = x(:,1:2);
        end
        
        
        function walk = ramdomWalkRndAngle(obj,limits,nbSteps)
            %RANDOMWALKRNDANGLE picks a random angle and makes the user
            % travel along it at a constant velocity until a random number
            % of steps. At that moment a new direction and velocity is
            % picked.
            
            % initial position and velocity
            x         = zeros(nbSteps,2);
            x(1,1)    = math.drawnumber((limits(2,2)-limits(1,2))/2,(limits(2,1)-limits(1,1))/2,1);
            x(1,2)    = math.drawnumber((limits(2,2)-limits(1,2))/2,(limits(2,2)-limits(1,2))/2,1);
            
            ang       = math.drawnumber(0.1,2*pi,1);
            rnd_sleep = fix(math.drawnumber(20,50,1));
            
            % init
            vel = 1;
            s   = 2;
            while s <= nbSteps
                
                if mod(s,rnd_sleep)==0
                    vel    = math.drawnumber(0,1,1);
                    ang = math.drawnumber(0.1,2*pi,1);
                    disp([vel,ang]);
                    rnd_sleep = fix(math.drawnumber(20,50,1));
                end
                
                new_step = x(s-1,:)+[sin(ang) -cos(ang)]*vel;
                
                if ~geometry.isinside(new_step,limits(:,1:2)) %redraw
                    ang     = math.drawnumber(0.1,2*pi,1);
                    continue;
                end
                
                x(s,:) = new_step;
                s      = s+1;
            end
            walk = x(:,1:2);
        end
        function nextStep = movementsAvailable(ndim)
            %MovementsAvailable returns the possible movements by the user,
            %considering a random walk
            choices = [ -1, 0 , 0;
                1 , 0 , 0;
                0 , 1 , 0;
                0 , -1, 0;
                0 , 0 , 1;
                0 , 0 , -1];
            
            n = ndim*2;
            if n < size(choices,1)
                nextStep = choices(1:n,1:ndim);
            else
                error('Dimension not supported');
            end
        end
        
        
        function walk = gridRamdomWalk(nsteps,limits,steplength)
            %randomWalk creates a random walk assuming N,S,E,W steps of
            %STEPLENGTH
            
            % creates starting point
            walk = nan(nsteps,2);
            dim  = size(limits,2);
            
            % check if 3D that it is not 0
            if limits(1,end)-limits(2,end) == 0 && dim == 3
                dim = 2;
            end
            
            % initial point
            walk(1,:) = math.drawRandomNdimNumber( limits );
            
            % Retrieves available motion and scales it for step length
            moves = build.walk.movementsAvailable(dim);
            moves = moves.*steplength;
            
            % generate choices vector
            % basically a vector to index choices
            choices = moves(math.drawDiscrete(dim*2,nsteps-1,1),:);
            for k=1:nsteps-1 % creates path
                walk(k+1,:)=walk(k,:)+choices(k,:);
                cnt = 0;
                %validate step
                while ~geometry.isinside(walk(k+1,:),limits) %redraw
                    walk(k+1,:)=walk(k,:)+moves(math.drawDiscrete(dim*2,1,1),:)*steplength;
                    cnt = cnt + 1;
                    if cnt > 100
                        error('build:scenario','stuck inside random walk');
                    end
                end
                assert(walk(k+1,1) == walk(k,1) || walk(k+1,2) == walk(k,2));
            end
            
        end
        
        
    end
    
end

