classdef scenario < handle
    % Scenario
    %   This class defines a generic scenario limited in a (up to) 3
    %   dimensional space by WID LEN DEP.
    %   Several OBJS can be deployed inside the LIMITS of the scenario
    %
    %   The coordinate system starts in 0 and for WID = 25 => 0:WID-1 =>
    %   0:24. There is an internal representation of the space defined in a
    %   matlab matrix, where the coordinate system is mapped into 1:inf
    
    properties (Constant)
        debug = 0;
    end
    
    properties
        area    % (#)  dimension
        len     % (#)  length of scenario
        wid     % (#)  width of scenario
        dep     % (#)  depth
        scene   % ([]) virtual representation
        unit    % (#)  quantaty to identify measure in Matlab
        ndim    % (#)  dimension indicator (2 or 3)
        objs    % ([]) objects location and type
        Mx      % ([]) x coordinates matrice
        My      % ([]) y coordinates matrice
        Mz      % ([]) z coordinates matrice
        coord   % ([]) x coordinates matrice
        limits  % ([]) limits of the area
        exists  % (b) flag that mentions if it exists or not
        
        dir     % storage directory
        storage % matfile to store data
    end
    
    
    methods(Static)
        
        function pos = coordinateMapping(pos,region_limits)
            %CoordinateMapping transforms the coordinates for storing in vector
            
            pos = fix(pos+1);
            pos(pos(:,1) < region_limits(1,1)+1) = region_limits(1,1)+1;% zero does not count
            pos(pos(:,1) > region_limits(2,1)+1) = region_limits(2,1)+1;% zero does not count
            
            pos(pos(:,2) < region_limits(1,2)+1) = region_limits(1,2)+1;% zero does not count
            pos(pos(:,2) > region_limits(2,2)+1) = region_limits(2,2)+1;% zero does not count
            
        end
        
        
        function [Mx,My,Mz,limits] = buildCoordinates(len,wid,dep,unit)
            % BUILDCOORDINATES provides a coordinate system for the given
            % area in the matlab unit system provided (converted from
            % meter)
            %
            %   LEN is defined as left to right (# cols)
            %   WID is defined as bottom to top (# rows)
            %
            %   ORIG defines the LOCATION of the origin, either top left or
            %   bottom left (No negative numbering here!)
            %
            %   Example,
            %       If the grid size is 0.5m then matlab_unit should be 2
            %
            
            My = repmat((0:unit:len)',1,len*1/unit+1)';
            Mx = repmat((0:unit:wid)',1,wid*1/unit+1);
            Mz = repmat((0:unit:dep)',1,dep*1/unit+1);
            
            if isempty(Mz) || length(Mz) == 1
                Mz = zeros(size(Mx));
            else
                dep = dep;
            end
            
            limits = [0,0,0;wid,len,dep];
        end
        
        
        function [wid,len,dep] = computeWidLen(limits)
            %COMPUTEWIDLEN returns the with and length
            
            wid = limits(2,1)-limits(1,1);%x
            len = limits(2,2)-limits(1,2);%y
            dep = limits(2,3)-limits(1,3);%z
        end
        
    end
    
    
    methods(Access = private)
        function pos = place_object(obj,limits,restricted_area)
            %PLACE_OBJECT places objects inside the given LIMITS and avoids
            %the RESTRICTED_AREA if provided.
            
            % Creates aps in the grid
            pos = math.drawRandomNdimNumber( limits(:,1:obj.ndim) );
            
            % check if valid for each room!
            if ~isempty(restricted_area)
                redraw = 1;
                while redraw
                    if(geometry.isinside(pos,restricted_area))
                        pos = math.drawRandomNdimNumber( limits(:,1:obj.ndim) );
                    else
                        redraw = 0;
                    end
                end
            end
        end
        
    end
    
    methods
        function obj = scenario(len,wid,dep,ndim,unit,dir,matfilename,forceOW)
            % SCENARIO is the object constructor
            %   LEN, WID, DEP and UNIT are inputs in meter
            
            obj.len   = len;
            obj.wid   = wid;
            obj.dep   = dep; if isempty(dep), obj.dep = 0; end;
            obj.ndim  = ndim;
            obj.area  = len*wid;
            obj.unit  = unit; % convert meter to vector size
            obj.scene = zeros((wid)*1/unit+1,(len)*1/unit+1); % floor map
            obj.dir   = dir;
            
            if ~exist(dir,'dir')
                mkdir(dir);
            end
            
            if nargin > 5
                % initialise storage
                if exist([dir,matfilename,'.mat'],'file') && ~forceOW
                    obj.exists  = true;
                    obj.storage = inout.output.createMatfile(dir,matfilename,false,false); % Writable and Overwrite
                else
                    obj.exists  = false;
                    obj.storage = inout.output.createMatfile(dir,matfilename,true,true); % Writable and Overwrite
                end
            end
            % Creates a coordinate system
            obj.createCoordinates();
            if tools.isWritable(obj.storage)
                obj.storage.coordinates    = {obj.Mx,obj.My,obj.Mz,obj.limits};
                grid                       = [obj.Mx(:),obj.My(:),obj.Mz(:)];
                grid                       = unique(grid, 'rows');
                obj.storage.GridCoordinates = grid;
            end
        end
        
        
        function setReference(obj,main_limits)
            n_wid = main_limits(2,1)-main_limits(1,1);%x
            n_len = main_limits(2,2)-main_limits(1,2);%y
            obj.scene = zeros(n_wid*1/obj.unit+1,n_len*1/obj.unit+1); % floor map
            obj.coord = main_limits;
        end
        
        function mapToScene(obj,pos_vec,region_limits,type)
            %MAPTOSCENE maps the coordinates into an internal
            %representation
            
            pos_vec(:,1:2) = pos_vec(:,1:2)*1/obj.unit;
            pos_vec        = build.scenario.coordinateMapping(pos_vec,region_limits);
            for k=1:size(pos_vec,1)
                obj.scene(pos_vec(k,1),pos_vec(k,2))=type(k);
            end
            
        end
        
        
        function fixObjects(obj,type,location)
            %FIXOBJECTS fixes objects to given coordinates
            %  TYPE and LOCAITON shall have the same length
            
            region = obj.limits; %assumes objects limits
            posN   = location;
            obj.mapToScene(posN,region,type);
            obj.objs = [posN,type];
            if tools.isWritable(obj.storage)
                obj.storage.objs   = obj.objs;
                obj.storage.nbObjs = size(obj.objs,1);
            end
        end
        
        function posN = placeObjects(obj,N,type,invalid_limits)
            %PLACEOBJECTS randomly places objects in the given region
            
            region = obj.limits; %assumes objects limits
            posN = NaN(N,obj.ndim);
            for k=1:N
                posN(k,:) = obj.place_object(region,invalid_limits);
            end
            obj.mapToScene(posN,region,type);
            obj.objs           = [posN,type];
            
            if tools.isWritable(obj.storage)
                obj.storage.objs   = obj.objs;
                obj.storage.nbObjs = size(obj.objs,1);
                obj.storage.scene  = obj.scene;
            end
        end
        
        function buildScene(obj,area)
            %BUILDSCENE constructs the internal scene representation
            obj.dim = area;
        end
        
        
        function createCoordinates(obj)
            %CREATECOORDINATES initialises coordinates for the object
            [obj.Mx,obj.My,obj.Mz,obj.limits] = build.scenario.buildCoordinates(obj.len,obj.wid,obj.dep,obj.unit);
        end
        
        
        function create2DWalk(obj,nbSteps,lenSteps,nbWalks,maxEm)
            %CREATE2DWALK generates N walks of size nbSteps with a step
            %size defined by lenSteps.
            % The walks are stored in a matfile.
            if tools.isWritable(obj.storage)
                obj.storage.walk(nbSteps,2,nbWalks)  = NaN;
                obj.storage.number_of_walks          = nbWalks;
                obj.storage.number_of_steps_per_walk = nbSteps;
                for k=1:nbWalks
                    obj.storage.walk(:,:,k) = build.walk.gridRamdomWalk(nbSteps,obj.limits(:,1:2),lenSteps);
                end
            else
                disp('not writable');
            end
        end
        
        
        
        
    end
    
end