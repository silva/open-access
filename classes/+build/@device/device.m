classdef device < handle
    %DEVICE serves as an interface for fingerprinting devices
    
    properties
        spectrum
        sensitivity
        nbSeen
        storage
        filename 
        dirOut
    end
    
    methods
        
        function obj = device(hSpectrum,sensitivity,varargin)
            %tbd
            assert(numel(sensitivity) == hSpectrum.nbEmitters);
            obj.spectrum    = hSpectrum;   % handle for spectrum access
            obj.sensitivity = sensitivity'; % one per tech
            
            if nargin == 5
                dir     = varargin{1};
                fname   = varargin{2};
                forceOW = varargin{3};
                
                if exist(fname,'file') && ~forceOW
                    obj.storage = inout.output.createMatfile( dir, fname, false, false);
                else
                    obj.storage = inout.output.createMatfile( dir, fname, true, true);
                end
                
                obj.dirOut   = dir;
                obj.filename = fname;
            end
            
        end
        
        function acquire(obj)
            %FOR ALL TRACKS
            
            % baisc step is to go to the database and colect data for each
            % point the user walks by
            
            % do it for all
            nbPoints   = obj.storage.nbSteps*obj.storage.nbWalks;
            Em         = obj.spectrum.getDB();
            N          = obj.spectrum.nbEmitters;
            
            % makes sure there are no old values
            obj.nbSeen                          = N;
            obj.storage.nbSeen                  = N;
            obj.storage.shadowing               = [];
            obj.storage.shadowing(N*nbPoints,1) = 0;
            obj.storage.nbFingerprints          = nbPoints*N;
            
            
            % computes all the shawdowing values
            s         = zeros(N,1);
            shadowing = zeros([N nbPoints]);
            id_list   = zeros(N,1);
            for k=1:N
                id_list(k)     = Em{1}(k);
                pFct           = Em{3}(k,:);
                hFct           = Em{end}{k}{2};
                s(k)           = pFct(3);
                shadowing(k,:) = hFct(pFct(3),[1 nbPoints]);    % calls shadowing function
            end
            obj.storage.shadowing        = shadowing(:);
            obj.storage.shadowing_per_AP = s;
            
            
            
            % computes the rss at each point
            walk = obj.storage.walk;
            M    = length(walk);
            meas = zeros(1,M);
            
            for k=1:N % for all emitters
                plFct = Em{end}{k}{1};
                L0    = pFct(1);
                n     = pFct(2);
                pos   = Em{2}(k,1:2);
                slim  = obj.sensitivity(k,1);
                for m = 1:M
                    d                  = sqrt(sum((walk(m,:)-pos).^2));
                    meas(k,m) = plFct(d,L0,n);
                end
                meas(k,meas(k,:)<slim) = NaN;
            end
            obj.storage.meas       = meas(:) + shadowing(:); % store vectorised
            obj.storage.meas_ideal = meas(:); % store vectorised
            
            clear meas
            clear shadowing;
        end
        
        
        
        
        function estimate(obj,type,sigma_shadowing_global,nbNeighbours,hStore)
            
            Em     = obj.spectrum.getDB;
            Em_id  = Em{1};
            Em_dtl = Em{2};
            switch upper(type(1:3))
                case 'ALL'
                    sel_ids = Em_id;
                    sel_sen = obj.sensitivity;
                    
                case 'WIF'
                    sel     = Em_dtl(:,end)==radio.spectrum.inter2id('wifi');
                    sel_ids = Em_id(sel);
                    sel_sen = obj.sensitivity(sel);
                    
                case 'RFI'
                    sel     = Em_dtl(:,end)==radio.spectrum.inter2id('rfid');
                    sel_ids = Em_id(sel);
                    sel_sen = obj.sensitivity(sel);
                    
                case 'BLE'
                    sel     = Em_dtl(:,end)==radio.spectrum.inter2id('ble');
                    sel_ids = Em_id(sel);
                    sel_sen = obj.sensitivity(sel);
                    
            end
            
            
            nbEm   = obj.spectrum.nbEmitters;
            nbPnt  = obj.storage.nbFingerprints/nbEm; % always true
            db_pos = obj.spectrum.getCoordinates();
            obj.spectrum.dumpSelection(sel_ids,sel_sen,obj.storage); % all the database in a vector with
            db = obj.storage.heard;
            db = db(:);
            
            % I need to send the fingerprints only
            radio.fingerprint.estimate(hStore,obj.storage,sel_ids.*0+1,nbPnt,nbEm,db,db_pos,sigma_shadowing_global,nbNeighbours);
                        
            
            % with the track positions estimated, compute the error
            [hStore.rmse,hStore.dist] = math.rmse(obj.storage.walk, hStore.pest(:,1:2));
            
            if length(hStore.dist)<1e3
                obj.pest       = hStore.pest(:,1:2);
                obj.rmse       = hStore.rmse;
                obj.disttoreal = hStore.dist;
            end
            
        end
        
    end
end
