function [wid,len,dep,area2d,area3d] = limits2area(limits)
    %LIMITS2AREA uses the limits to retrieve the area
    
    
    wid    = limits(2,1)-limits(1,1);%x
    len    = limits(2,2)-limits(1,2);%y
    dep    = limits(2,3)-limits(1,3);%y
    area3d = wid*len*dep;
    area2d = wid*len;
    
end