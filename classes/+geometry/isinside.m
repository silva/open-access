function rt = isinside( P, B)
    %ISINSIDE is P inside B?
    %   tests if P is inside B
    %
    %   It assumes 
    %       P as [x y]
    %       B as [x1 y1
    %             x2 y2]
    
    %Sanity test : B neds to have 2 l but same col
    assert(isrow(P));
    
    % Switch according to dimensions
    dims           = numel(P);
    inside(dims,1) = 0;
    switch(dims)
        
        case 1,
            if(P==B),
                inside = 1;
            else
                inside = 0;
            end;
            
        otherwise,
            % For each dimension (x,y,z,...) test if O is inside the region
            % defined by starting and ending points of the region B
            for k=1:dims
                if P(k) > B(1,k) && P(k) < B(2,k) % x intersection
                    inside(k) = 1;
                else
                    inside(k) = 0;
                end
            end
    end
    
    % true if B contains P
    rt = all(inside);
    
end

