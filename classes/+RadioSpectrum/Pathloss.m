classdef Pathloss
    %PATHLOSS offers several pathloss methods for different technologies
    %   This class offers a static interface to obtain theorectical rss
    %   values following the implemented models
    %
    %   USAGE
    %       No instance is allowed (Abstract class)
    %
    %NOTES
    %http://www.connect802.com/tech_notes.htm
    % What are the transmit power limitations for 802.11 devices?
    %
    % In the United States, the Federal Communications Commission defines power limitations for wireless LANS in FCC Part 15.247. The wording in the FCC rules is somewhat complex and has led to some misunderstandings. There are two values to be considered in assessing transmit power: TPO (Transmitter Power Output) and EIRP (Equivalent Isotropically Radiated Power, sometimes defined as Effective Isotropically Radiated Power). TPO is a measure of the power being delivered to the transmitting antenna and EIRP is the result of adding any antenna gain. Although the details of the rules for the 2.4 GHz and 5.8 GHz band are slightly different, the essence is as follows:
    %
    % Maximum TPO:    1 watt (1000 mW, 30 dBm)
    % Maximum EIRP for non-point-to-point links:
    % 802.11b
    % 100 mW (20 dBm)
    % 802.11g
    % 50 mW (17 dBm) @24 Mbps and less, 40 mW (16 dBm) @ 36 Mbps,
    % 31.6 mW (15 dBm) @48 Mbps, 20 mW (13 dBm) @54 Mbps
    % 802.11a
    % 40 mW (16 dBm) @24 Mbps and less, 25.1 mW (14 dBm) @36 Mbps, 20 mW (13 dBm) @48&54 Mbps
    % Maximum EIRP for fixed, point-to-point links using an antenna with a minimum 6 dB gain:
    % EIRP Maximum = 4 watts (4000 mW, 36 dBm) where TPO Max = 30 dBm
    % For antennas with greater than 6 dB of gain:
    % Reduce TPO from 1 watt by 1 dB for each 3 dB of additional antenna gain beyond 6 dB
    % This allows E IRP to be greater than 36 dBm for antennas with greater than 6 dB gain).
    %
    %   [1] WLAN and RFID Propagation Channels for Hybrid Indoor
    %   Positioning, E.S.Lohan, Koski K., Talvitie J., Ukkonen L.
    
    properties(Constant)
        
        
        %Table obtained from [1] table 2
        table_WIFI = [%Coef    %Pt-A  %sigma
            % Building
            1.93     -13.03  6.00 % All building floors
            2.43     4.80    6.85 % Same-floor
            % Room based
            2.10     -10.84  3.10 % All building floors
            1.91     -14.32  4.07 % Same-floor
            ];
        
        %Table obtained from [1] table 3
        RFID_default_angle = 180;
        table_RFID = [%angle     %Coef    %Pt-A  %shadowing av
            36       2.46     -0.41         1.54;
            72       2.43     -1.32         1.62;
            108      2.24     -5.91         2.62;
            144      2.00     -11.49        3.69;
            180      1.78     -16.05        4.28;
            ];
        
        % Interpolation method
        interpMethod = 'spline';
        
    end
    
    % Static methods implementing
    methods(Static)
        %noise figure UMTS http://www.diva-portal.org/smash/get/diva2:277389/FULLTEXT01.pdf
        function [rss,sigma] = dist2rss(varargin)
            % Converts a distance to rss for wifi and rfid
            %
            %   dist2rss(varargin)
            %
            % ARGUMENTS
            %   dist    (m)
            %   freq    (Hz)
            %   sigma   (dB)
            %   [alpha] (degrees)
            
            import RadioSpectrum.*;
            
            % Some defaults are loaded
            nrow  = 4;
            f     = 1;
            alpha = Pathloss.RFID_default_angle;
            sigma = Pathloss.table_WIFI(nrow,3);
            
            % Switch according to input
            if nargin == 2
                d    = varargin{1};
                type = varargin{2};
                
            elseif nargin == 3
                d    = varargin{1};
                f    = varargin{2};
                type = varargin{3};
                
            elseif nargin == 4
                d     = varargin{1};
                f     = varargin{2};
                sigma = varargin{3};
                type  = varargin{4};
                
            elseif nargin == 5
                d     = varargin{1};
                f     = varargin{2};
                sigma = varargin{3};
                
                %Sanity checks
                check(alpha,0,'>');
                type = varargin{5};
            else
                %                 error('Too few input arguments');
            end
            
            %Sanity checks
            %             check(d,0,'>');
            assert(f>0);
            assert(sigma>0);
            
            % Call rss calculation method method
            switch(type)
                case 'wifi'
                    if f == 1,f = 2.400e9; end
                    nrow = 4;
                    rss = Pathloss.calculateRSSWIFI(d,Pathloss.table_WIFI(nrow,1),Pathloss.table_WIFI(nrow,2),f,sigma);
                case 'rfid'
                    if f == 1,f = 866e6; end % value from paper
                    rss = Pathloss.calculateRSSRFID(d,alpha,f);
                case 'u-ul'%umts uplink (ITU-R model)
                    h_a  = 15; % tx antenna
                    lPct = 0.5; %50%
                    h_2  = 1.5; % user
                    f    = f; %MHZ
                    d    = d; %km
                    
                    L = 32.5 + 20*log10(f/1e6)+20*log10(d/1e6 + (h_a - h_2)^2/10^6);
                    Qi_x = (-norminv(lPct/100,0,1));
                    L = L + 11 + Qi_x * sqrt(3.5^2+36);
                    
                    %From http://www.mathworks.com/matlabcentral/fileexchange/42638-path-loss-calculator-for-jtg-5-6-propagation-model/content/JTG5-6/JTG5_6.m
                    
                    % Section 2.1 of Appendix 1 to Annex 2 of Rep.  ITU-R  SM.2028-1
                    % Case 1: distance < 0.04 use free space path loss model for the modified
                    % hata model. 
                    % For indoor scenarios the standard diviation of the path loss value is
                    % determined based on Case 1 of Section 2.2 of Appendix 1 to Annex 2 of Rep. ITU-R
                    % SM.2028-1 and the standard diviation of the building entry loss from
                    % Section 4.9 of Annex 1 of ITU-R P.1812-2. The calculations are based on
                    % Section 4 of Appendix 1 to Annex 2 of Rep. ITU-R SM.2028-1.
                    
                    Pt  = 24-30; %dB %dBm https://books.google.fi/books?id=7m-MnwW_o7AC&pg=PA175&lpg=PA175&dq=link+budget+uplink+umts&source=bl&ots=2pqmLesYRm&sig=sr50ZP_CT_PAe3XEXy4p3tTX5w4&hl=en&sa=X&ei=FszQVOWPFMLkyAPJzYJI&ved=0CD8Q6AEwBQ#v=onepage&q=link%20budget%20uplink%20umts&f=false
                    rss = Pt-L;
                    
                case 'u-dl' %umts downlink
                    % from http://www.etsi.org/deliver/etsi_tr/101100_
                    % 101199/101112/03.02.00_60/tr_101112v030200p.pdf
                    % page 40
                    %
                    %https://sites.google.com/site/...
                    % lteencyclopedia/lte-radio-link-budgeting...
                    % -and-rf-planning/lte-link-budget-comparison
                    % f in MHz !!!
                    
                    %http://www.comlab.hut.fi/opetus/333/2004_2005_slides/Path_loss_models.pdf
                    f  = f*1e-03;
                    d  = d*1e-03; %km
                    
                    if d < 1 || d > 10
                        error('Pathloss:dist2rss','Distance outside validity range');
                    end
                    
                    % Frequency f between 150 MHz and 1500 Mhz
                    % TX height hb between 30 and 200 m
                    % RX height hm between 1 and 10 m
                    % TX - RX distance r between 1 and 10 km
                    
                    % for UE
                    hb = 10;  % base station height
                    hm = 1.5; % mobile station height
                    K  = 3; %for metropolitan centers
                    if f < 1.5e6
                        L = 69.55 + 26.16*log10(f) -13.82*log10(hb)...
                            - (1.1*log10(f)-0.7)*hm -1.56*log(f)+0.8 ...
                            + (44.9 -6.55*log10(hb))*log10(d)-K;
                    else
                        L = 46.3 + 33.9*log10(f) -13.82*log10(hb)...
                            - (1.1*log10(f)-0.7)*hm -1.56*log(f)+0.8 ...
                            + (44.9 -6.55*log10(hb))*log10(d)-K;
                    end
                    rss = Pt-L;
                otherwise
                    error('Pathloss:dist2rss','Unknown pathloss for prvided technology');
            end
        end
        
        function [rss,sigma] = dist2rssIdeal(varargin)
            % Converts a distance to rss for wifi and rfid
            %
            %   dist2rss(varargin)
            %
            % ARGUMENTS
            %   dist    (m)
            %   freq    (Hz)
            %   nrow    (#)
            %   [alpha] (degrees)
            
            import RadioSpectrum.*;
            
            % Some defaults are loaded
            nrow  = 4;
            f     = 1;
            alpha = Pathloss.RFID_default_angle;
            
            
            % Switch according to input
            if nargin == 2
                d    = varargin{1};
                type = varargin{2};
                
            elseif nargin == 3
                d    = varargin{1};
                f    = varargin{2};
                type = varargin{3};
                
            elseif nargin == 4
                d     = varargin{1};
                f     = varargin{2};
                nrow  = varargin{3};
                type  = varargin{4};
                
            elseif nargin == 5
                d     = varargin{1};
                f     = varargin{2};
                nrow  = varargin{3};
                alpha = varargin{4};
                %Sanity checks
                check(alpha,0,'>');
                type = varargin{5};
            else
                error('Too few input arguments');
            end
            if isempty(nrow) || nrow == 0
                nrow  = 4;
            end
            sigma = Pathloss.table_WIFI(nrow,3);
            
            %Sanity checks
            %             check(d,0,'>');
            assert(f>0);
            assert(sigma>0);
            
            % Call rss calculation method method
            switch(type)
                case 'wifi'
                    if f == 1,f = 2.400e9; end
                    nrow = 4;
                    rss = Pathloss.calculateIdealRSSWIFI(d,Pathloss.table_WIFI(nrow,1),Pathloss.table_WIFI(nrow,2),f);
                case 'rfid'
                    if f == 1,f = 866e6; end % value from paper
                    rss = Pathloss.calculateIdealRSSRFID(d,alpha,f);
            end
        end
        
        
        % Computes RSS for WIFI
        function rss = calculateIdealRSSWIFI(d,n,L,f)
            % Computes RSS for WIFI device
            %
            %   USAGE
            %   calcRSSWIFI(d,n,L,f,s)
            %
            %   ARGUMENTS
            %   d (m)  Distance
            %   n (#)  Pathloss coefficient
            %   L (dB) Budget(L=Pt-A)
            %   f (Hz) Frequency
            %   s (dB) Standard deviation
            import RadioSpectrum.*;
            rss = L - n*(Pathloss.B(f)+10*log10(d));
        end
        
        
        % Computes RSS for RFID
        function rss = calculateIdealRSSRFID(d,a,f)
            % Computes RSS for RFID device
            %
            %   USAGE
            %   rss = calcRSSRFID(d,a,f)
            %
            %   ARGUMENTS
            %   d (m)   Distance
            %   a (deg) Oppening angle
            %   f (Hz)  Frequency
            %   s (dB)  Standard deviation
            import RadioSpectrum.*;
            [n,L, ~ ] = Pathloss.getRFIDChannel(a);
            rss = L - n*(Pathloss.B(f)+10*log10(2*d));
        end
        
        
        % Computes RSS for RFID
        function rss = calculateRSSRFID(d,a,f)
            % Computes RSS for RFID device
            %
            %   USAGE
            %   rss = calcRSSRFID(d,a,f)
            %
            %   ARGUMENTS
            %   d (m)   Distance
            %   a (deg) Oppening angle
            %   f (Hz)  Frequency
            %   s (dB)  Standard deviation
            import RadioSpectrum.*;
            [n,L,sigma] = Pathloss.getRFIDChannel(a);
            rss = L - n*(Pathloss.B(f)+10*log10(2*d))+normrnd(0,sigma);
        end
        
        
        function [npdBm,npdB] = noisePower(NF,B,G)
            % Computes the noise power for bandwith B
            npdBm = -174+10*log10(B)+NF+G;
            npdB  = npdBm - 30; 
            %            disp(npdBm)
        end
        
        % Computes RSS for WIFI
        function rss = calculateRSSWIFI(d,n,L,f,s)
            % Computes RSS for WIFI device
            %
            %   USAGE
            %   calcRSSWIFI(d,n,L,f,s)
            %
            %   ARGUMENTS
            %   d (m)  Distance
            %   n (#)  Pathloss coefficient
            %   L (dB) Budget(L=Pt-A)
            %   f (Hz) Frequency
            %   s (dB) Standard deviation
            import RadioSpectrum.*;
            rss = L - n*(Pathloss.B(f)+10*log10(d))+normrnd(0,s);
        end
        
        
        function s = getShadowing(type, MxNxK, opening_angle)
            import RadioSpectrum.*;
            switch(upper(type))
                case 'WIFI'
                    sigma = Pathloss.table_WIFI(1,3);
                case 'RFID'
                    sigma  = Pathloss.getShawdowingCoefRFID(opening_angle);
            end
            s     = normrnd(0,sigma,MxNxK);
        end
        
        
        function rss = addShawdowing(rss,sigma)
            rss = rss + normrnd(0,sigma);
        end
        
        
        function [cno,svar] = dist2cno(d,freq,BW,G,Nfig,F2,type,To)
            rss    = RadioSpectrum.Pathloss.dist2rss(d,freq,type); % f in MHz
            [~,noise]  = RadioSpectrum.Pathloss.noisePower(Nfig,BW,G); %0 dB gain
            cno    = rss - noise;
            svar   = math.CRBD(d,10^(cno/10),F2,3,1,To);
        end
        
        
        
        % Provides parameters for RFID channel pathloss computation
        function [L,n,s] = getRFIDChannel(a)
            % Retrieves and interpolates (if necessary) RFID parameters
            %
            %   USAGE
            %   [L,n,s] = getRFIDChannel(a)
            %
            %   ARGUMENTS
            %   L (dB) Budget(L=Pt-A)
            %   n (#)  Pathloss coefficient
            %   s (dB) Standard deviation
            import RadioSpectrum.*;
            % interpolate table for values
            L = interp1(Pathloss.table_RFID(:,1),Pathloss.table_RFID(:,2),a,Pathloss.interpMethod,'extrap');
            n = interp1(Pathloss.table_RFID(:,1),Pathloss.table_RFID(:,3),a,Pathloss.interpMethod,'extrap');
            s = interp1(Pathloss.table_RFID(:,1),Pathloss.table_RFID(:,4),a,Pathloss.interpMethod,'extrap');
        end
        
        function s = getShawdowingCoefRFID(opening_angle)
            import RadioSpectrum.*;
            s = interp1(Pathloss.table_RFID(:,1),Pathloss.table_RFID(:,4),opening_angle,Pathloss.interpMethod,'extrap');
        end
        
        
        function loss = B(f)
            % Computes the loss due to the frequency
            %
            %   USAGE
            %   loss = B(f)
            %
            %   ARGUMENTS
            %   loss (dB) Attenuation due to freq. travel
            loss = 10*log10(4*pi*f/299792458);
        end
    end
    
    methods(Abstract)
        createmap(obj,type,varargin);
        % Creates pathloss map
    end
    
end




