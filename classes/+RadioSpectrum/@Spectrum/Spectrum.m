classdef Spectrum < handle
    %SPECTRUM<HANDLE aggregates spectrum information
    %
    %   CONSTRUCTOR
    %    Spectrum()
    %
    %
    %   Tampere University of Technology
    %   Pedro Silva, 2014
    %
    %   TODO
    %   Currently RSS can have different size! make sure it is the
    %   reference size
    
    %%% VARIABLES
    %%%%% PRIVATE
    %%%%% PUBLIC
    %%%%% PROTECTED
    properties (Constant)
        wifi  = 1;
        rfid  = 2;
        blue  = 3;
        uwb   = 4;
        light = 5;
    end
    
    properties
        nb_neighbours
        sigma_wifi 
        sigma_rfid   
        sigma_global
    end
    
    properties(Access = private)
        map_wifi
        map_rfid
        map_blue
        map_uwb
        map_light
        
        shw_wifi
        shw_rfid
        shw_blue
        shw_uwb
        shw_light
        
        % ID - Handler
        keys_wifi
        keys_rfid
        keys_blue
        keys_uwb
        keys_light
        
        rfid_alpha
    end
    
    properties
        rfid_markers
    end
    
    %%% CONSTRUCTOR
    methods
        function obj = Spectrum(alpha,n_neighbours,n_sigma_global,n_sigma_wifi,n_sigma_rfid)
            %%% constructor
            if ~exist('alpha','var')
                alpha = 180;
            end
            obj.rfid_alpha    = alpha;
            obj.nb_neighbours = n_neighbours;
            obj.sigma_wifi    = n_sigma_wifi;
            obj.sigma_rfid    = n_sigma_rfid;
            obj.sigma_global  = n_sigma_global;
        end
        
        function reset(obj)
            clear Spectrum;
        end
    end
    
    %%% METHODS
    %%%%% PRIVATE
    methods(Access = private, Static)
        function keys_handler = generateKey(identifier, keys_handler)
            % map RSS values for a device
            if isempty(keys_handler)
                keys_handler(1,:) = [identifier, 1];
            else
                % Check if Identifier exists
                assert(isempty(keys_handler(keys_handler(:,1) == identifier)));
                
                % Creates new pair of keys and indexes
                keys_handler        = [keys_handler; zeros(1,2)];
                keys_handler(end,:) = [identifier, keys_handler(end-1,end)+1];
            end
        end
        %         function keys_handler = removeKey(identifier,keys_handler)
        % For cleanup purposes
        %         end
        function key = getKey(identifier,keychain)
            % map RSS values for a device
            key = keychain(keychain(:,1) == identifier); %empty if none
        end
        
        
        function rss = collectRSS(pos,hMap,sMap)
            % Acquire for all in the hMap
            if size(hMap,2) == size(sMap,2) && size(hMap,1) == size(sMap,1)
                rss = hMap(pos(1),pos(2),:)+sMap(pos(1),pos(2),:);
            else
                rss = hMap(pos(1),pos(2),:)+sMap;
            end
            rss = rss(:);
        end
        
        function aps = collectAPS(hKeys)
            
            % Acquire for all in the hMap
            aps = hKeys(:,1);
            aps = aps(:);
            
        end
        
        
    end
    %%%%% PUBLIC
    methods(Static)
        function id = getInterfaceId(str)
            %GETINTERFACEID converts text to integer represenation
            switch(upper(str))
                case 'WIFI',
                    id = 1;
                case 'RFID',
                    id = 2;
                case 'BLUETOOTH',
                    id = 3;
                case 'UWB',
                    id = 4;
                case 'LTE',
                    id = 5;
                otherwise
                    id = NaN;
                    error('radio type unknown');
            end
        end
        
        function [radioMap,x,y,shawdowingMap,sigma,shawdowingMapRecv] = compRadioMap( pos, coordinates, unit, interface, alpha)
            %COMPRADIOMAP Summary of this function goes here
            %   Detailed explanation goes here
            %
            %TODO
            %   l and c need to start in the lower extremes of the map!
            %   Or, trim what is not needed elsewhere...But make radioMap size an
            %   input!
            %
            %   change name to computeRadioMap
            
            if ~isrow(pos)
                pos = pos';
            end
            
            % Compute starting and ending points
            ls    = coordinates(1,1);
            cs    = coordinates(1,2);
            L     = coordinates(2,1);
            C     = coordinates(2,2);
            uarea = abs(coordinates(1,1)-coordinates(2,1));
            
            % init variables
            radioMap = NaN(uarea);
            x        = zeros(uarea);
            y        = zeros(uarea);
            nl       = 1;
            nc       = 1;
            for l=ls:unit:L
                for c=cs:unit:C
                    distance = norm([l,c]-pos);
                    [radioMap(nl,nc),sigma]  = RadioSpectrum.Pathloss.dist2rssIdeal(distance,1,4,alpha,interface); % f=1 default for each technology
                    x(nl,nc) = l;
                    y(nl,nc) = c;
                    nc = nc + 1;
                end
                % Reset cols and inc rows
                nc = 1;
                nl = nl + 1;
            end
            
            % sets max to 0 dB.
            radioMap(radioMap      == Inf) = 0;
            shawdowingMap          = RadioSpectrum.Pathloss.getShadowing(interface,[L,C,nRcv], alpha);
            shawdowingMap(radioMap == Inf) = 0;
            
            % store it
            
        end
    end
    
    %%%%% PUBLIC
    methods
        
        function add(obj,identifier, RSS, SHW, type)
            % map RSS values for a device
            switch upper(type)
                case 'WIFI'
                    obj.keys_wifi  = Spectrum.generateKey(identifier,obj.keys_wifi);
                    obj.map_wifi(:,:,obj.keys_wifi(end,end)) = RSS;
                    obj.shw_wifi(:,:,obj.keys_wifi(end,end)) = SHW;
                    
                case 'RFID'
                    obj.keys_rfid  = Spectrum.generateKey(identifier,obj.keys_rfid);
                    obj.map_rfid(:,:,obj.keys_rfid(end,end)) = RSS;
                    obj.shw_rfid(:,:,obj.keys_rfid(end,end)) = SHW;
                    
                otherwise
                    assert(1==0)
            end
        end
        function radioMap = get(obj,identifier,type)
            switch upper(type)
                case 'WIFI'
                    idx           = obj.keys_wifi(obj.keys_wifi(:,1) == identifier);
                    radioMap(:,:) = obj.map_wifi(:,:,idx);
                otherwise
                    assert(1==0)
            end
        end
        
        function radioMap = getIdealMapsFrom(obj,type)
            switch upper(type)
                case 'WIFI'
                    radioMap(:,:) = obj.map_wifi;
                case 'RFID'
                    radioMap(:,:) = obj.map_rfid;
                otherwise
                    assert(1==0)
            end
        end
        
        function radioMap = getMapsFrom(obj,type)
            switch upper(type)
                case 'WIFI'
                    radioMap = obj.map_wifi+obj.shw_wifi;
                case 'RFID'
                    radioMap = obj.map_rfid+obj.shw_rfid;
                case 'ALL'
                    wf=[];rf=[];
                    if ~isempty(obj.map_wifi)
                        wf = obj.map_wifi+obj.shw_wifi;
                    end
                    if ~isempty(obj.map_rfid)
                        rf = obj.map_rfid;
                    end
                    radioMap = cat(3,wf,rf);
                    
                otherwise
                    assert(1==0)
            end
        end
        
        function aps = getApsFrom(obj,type)
            switch upper(type)
                case 'WIFI'
                    aps(:,:) = obj.keys_wifi;
                case 'RFID'
                    aps(:,:) = obj.keys_rfid;
                case 'ALL'
                    wf=[];rf=[];
                    if ~isempty(obj.keys_wifi)
                        wf = obj.keys_wifi;
                    end
                    if ~isempty(obj.keys_rfid)
                        rf = obj.keys_rfid;
                    end
                    aps = [wf;rf];
                otherwise
                    assert(1==0)
            end
        end
        
        function [rss,aps] = getRSS(obj,pos,type,redraw,rss_lim)
            %getRSS handles database requests
            %   This method retrieves a fingerprint for the given position
            %   It assumes the maps do not have shawdowing, so it is added
            %   at this point
            
            switch upper(type)
                case 'WIFI'
                    
                    if redraw == 1, shw = obj.shw_wifi(:,:,2);
                    else shw = obj.shw_wifi; end;
                    aps = Spectrum.collectAPS(obj.keys_wifi);
                    rss = Spectrum.collectRSS(pos,obj.map_wifi,shw);
                    rss(rss < rss_lim(1)) = -Inf;;
                    %                     interface = ones(length(aps),1);
                case 'ALL'
                    aps = Spectrum.collectAPS(obj.keys_wifi);
                    if redraw == 1, shw = obj.shw_wifi(:,:,2);
                    else shw = obj.shw_wifi; end;
                    rss = Spectrum.collectRSS(pos,obj.map_wifi,shw);
                    rss(rss < rss_lim(1)) = -Inf;
                    ;%
                    interface = ones(length(aps),1);
                    % Put this to function (easier to deal with)
                    if ~isempty(obj.map_rfid) == 1
                        if redraw == 1, shw = obj.shw_rfid(:,:,2);
                        else shw = obj.shw_rfid; end;
                        aps_rfid = Spectrum.collectAPS(obj.keys_rfid);
                        rss_rfid = Spectrum.collectRSS(pos,obj.map_rfid,shw);
                        rss_rfid(rss_rfid < rss_lim(2)) = -Inf;
                        
                        rss      = [rss; rss_rfid];
                        aps      = [aps; aps_rfid];
                        %                         interface = [interface; ones(length(aps_rfid),1).*2];
                    end
                    
                case 'RFID'
                    aps = Spectrum.collectAPS(obj.keys_rfid);
                    if redraw == 1, shw = obj.shw_rfid(:,:,2);
                    else shw = obj.shw_rfid; end;
                    rss = Spectrum.collectRSS(pos,obj.map_rfid,shw)
                    rss(rss < rss_lim(2)) = -Inf;;
                    %                     interface = ones(length(aps),1).*2;
                    
                otherwise
                    assert(1==0)
            end
        end
        
        function s = drawShadowRFID(obj,MxN)
            import RadioSpectrum.*;
            s = Pathloss.getShadowing('rfid',MxN, obj.rfid_alpha);
        end
        
        
        function s = drawShadowWIFI(obj,MxN)
            import RadioSpectrum.*;
            s = Pathloss.getShadowing('wifi',MxN, []);
        end
        
        
        function [l,c] = getSize(obj)
            [l,c] = size(obj.map_wifi(:,:,1));
        end
        
        function cnt = total(obj,type)
            switch upper(type)
                case 'WIFI'
                    cnt = size(obj.keys_wifi,1);
                case 'ALL'
                    cnt = size(obj.keys_wifi,1);
                    if ~isempty(obj.keys_rfid)
                        cnt = cnt + size(obj.keys_rfid,1);
                    end
                    if ~isempty(obj.keys_uwb)
                        cnt = cnt + size(obj.keys_uwb,1);
                    end
                    if ~isempty(obj.keys_blue)
                        cnt = cnt + size(obj.keys_blue,1);
                    end
                case 'RFID'
                    cnt = size(obj.keys_rfid,1);
                otherwise
                    assert(1==0)
            end
        end
        
        function list(obj)
            if ~isempty(obj.keys_wifi)
                disp('wifi')
                disp(obj.keys_wifi(:,1))
            end
            if ~isempty(obj.keys_rfid)
                disp('rfid')
                disp(obj.keys_rfid(:,1))
            end
            if ~isempty(obj.keys_blue)
                disp('bluetooth')
                disp(obj.keys_blue(:,1))
            end
            if ~isempty(obj.keys_uwb)
                disp('uwb')
                disp(obj.keys_uwb(:,1))
            end
            if ~isempty(obj.keys_light)
                disp('light')
                disp(obj.keys_light)
            end
        end
        
        
        function marker = getRFIDmarkers(obj)
            marker=[zeros(obj.total('wifi'),1);ones(obj.total('rfid'),1)];
        end
        
    end
    %%%%% PROTECTED
    
    
    %%% GETTERS and SETTERS
    methods
    end
    
    
    
end

