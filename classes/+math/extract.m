function [ extracted ] = extract( vector, nbBlocks, len )
    %EXTRACT acts as an equivalent for MATLABS's RESHAPE
    %   Allows a bigger flexibility
    %   but consider reshape + squeeze
    
    extracted = zeros(nbBlocks/3,3);
    
    % now I need to unfold it
    state = 0;
    ch    = 1;
    ct    = len;
    head  = 1;
    for k=1:nbBlocks % number of blocks
        % h is the start, so 
        % extract chunk of nbSteps
        chunk = vector(head:head+len-1); % takes chunk of M size
        switch(state) % puts it to the right place
            case 0, %x
            extracted(ch:ch+ct-1,1) = chunk;
            state = 1;
            case 1, %y
            extracted(ch:ch+ct-1,2) = chunk;
            state = 2;    
            case 2, %err
            extracted(ch:ch+ct-1,3) = chunk;
            ch = ch+ct; % updates indexes
            state = 0;
        end
        head = head+len;    
    end
    
end

