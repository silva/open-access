function val = CRBD(d,icno,f2,go,ko,To)
    %UNTITLED2 Summary of this function goes here
    %   Detailed explanation goes here
    
    c         = 3e8;            % speed of light (m/s)
    if ~exist('go','var') || isempty(go)
        go        = 2;              % delta coefficient, 2 in free space
    end
    if ~exist('ko','var') || isempty(ko)
        ko        = 1;              % constant parameter (?)
    end
    val = c^2.*d.^(go+2) ./ (( 2.*ko.^2.*icno.*To.*( (go.^2.*c.^2)./4 + d.^2.*f2 )));
    
end

