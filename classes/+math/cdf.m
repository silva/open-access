function [ecdf,acdf] = cdf(data,nan_val,step,max_val)
    if nargin == 3
        max_val=[];
    end
    
    %find the CDF of the distance error
    data = abs(data);    % absolute value
    data = sort(data);   % sorts data
    len  = size(data,1); % length of each vector
    nvc  = size(data,2); % total number of err vectors
    
    % replaces nan value if value provided
    if exist('nan_val','var') && ~isempty(nan_val);
        for n=1:nvc
            data(isnan(data(:,n)),n)=nan_val;
        end
    end
    
    % axis creation
    if exist('max_val','var')
        if isempty(max_val)
            M = max(data(:)); % max over all the err vectors
        else
            M = round(max_val+5);
        end
    else
        M = max(data(:)); % max over all the err vectors
    end
    N      = min(data(:));
    if ~exist('step','var')
        step   = 1000;
    end
    acdf = N:(M-N)/step:M; % common axis creation
    
    % computes the cdf
    ecdf=zeros(length(acdf),nvc);
    for n=1:nvc
        if any(isnan(data(:,n)))
            warning('CDF: NAN values encountered. Expected?');
        end
        for k=1:length(acdf),
            ecdf(k,n)=sum(data(:,n)<=acdf(k))./len;
        end;
    end
        
end