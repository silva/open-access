function y = drawNumberVarMean(v,m,N)
    %DRAWNUMBERVARMEAN draws a random number with variance V and mean M
    
    y = m + sqrt(v)*randn(N,1);
    
end

