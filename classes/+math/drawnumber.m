function nb = drawNumber( a,b, N )
    %DRAWNUMBER draws a random number from the range a,b
    %   eg, drawnumber(a,b,N)
    %
    
    if ~exist('N','var')
        N = 1;
    end
    assert(a<=b);
    
    nb = (b-a).*rand(N,1) + a;
    
end