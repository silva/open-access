function [ nvec ] = geodistance( a, b )
    %GEODIST computes the geometric distance from a to b
    
    
    vec  = a-b;
    nvec = sqrt(sum(vec.^2,2));
    
    
end

