function [ r_ellipse ] = createCovEllipse( V )
    %CREATECOVRADIUS receives a covariance as input and computes the
    %ellipse
    theta_grid = linspace(0,2*pi);
     % build covariance output
    [eigenvec, eigenval ]       = eig(V);
    [largest_eigenvec_ind_c, r] = find(eigenval == max(max(eigenval)));
    largest_eigenvec            = eigenvec(:, largest_eigenvec_ind_c);
    largest_eigenval            = max(max(eigenval));

    % Get the smallest eigenvector and eigenvalue
    if(largest_eigenvec_ind_c == 1)
        smallest_eigenval = max(eigenval(:,2));
        smallest_eigenvec = eigenvec(:,2);
    else
        smallest_eigenval = max(eigenval(:,1));
        smallest_eigenvec = eigenvec(1,:);
    end
    % This angle is between -pi and pi.
    % Let's shift it such that the angle is between 0 and 2pi
    angle                 = atan2(largest_eigenvec(2), largest_eigenvec(1));
    if(angle < 0)
        angle = angle + 2*pi;
    end
    a=2.4477*sqrt(largest_eigenval);
    b=2.4477*sqrt(smallest_eigenval);

    % the ellipse in x and y coordinates
    ellipse_x_r  = a*cos( theta_grid );
    ellipse_y_r  = b*sin( theta_grid );
    %Define a rotation matrix
    phi = angle;
    R   = [ cos(phi) sin(phi); -sin(phi) cos(phi) ];

    %let's rotate the ellipse to some angle phi
    r_ellipse = [ellipse_x_r;ellipse_y_r]' * R;
    
    
end

