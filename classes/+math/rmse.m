function [err,dist] = rmse( xr, xe )
    %UNTITLED Summary of this function goes here
    %   Detailed explanation goes here
    dist = sqrt(sum((xe-xr).^2,2));
    err = sqrt(mean(dist.^2));
    
end

