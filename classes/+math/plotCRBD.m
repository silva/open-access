%%%%
%
%   CRBD 3D plots
%
%%%%
close all;
clear all;
% Load F2 data
%load('\\intra.tut.fi\home\figueire\My Documents\MATLAB\classes\+math\plotCRBDdata\f2_data.mat')
% NEW datafile _ F2wifib;F2wifig;F2wifiac;F2wcdma;
%for k=size(F_2(:),1)
%    aF2(k,1) = F_2(k);
%end

%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Signal   Parameters %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%
CN0       = 5:5:50;         % carrier to noise ratio, (dB)
fs        = 100e6;          % sampling frequency, (Hz)
simTime   = 1e-03;          % simulation time, (s)
Ts        = 1/fs;           % sampling period, (s)
c         = 3e8;            % speed of light (m/s)

To        = simTime;%*fs;
B         = fs;

go        = 2;              % delta coefficient, 2 in free space
ko        = 1;              % constant parameter (?)
D         = 1:1:10;         % distance (m)
plotCN0   = 40;
alpha     = ko./(sqrt(D.^(go)));

cno       = 10.^(CN0./10);  % linear conversion of CN0


F2      = @(f,s) sum( (2.*pi.*f).^2 .* abs(s).^2)./ ( sum(abs(s).^2));

% CDMA based
flag_cdma = 1;
flag_ofdm = 0;
generateWCDMA;
createbounds;
wcdma     = cdma;
F2wcdma   = F2cdma;

generateWIFIb;
createbounds;
wifib     = cdma;
F2wifib   = F2cdma;

% OFDM based
flag_cdma = 0;
flag_ofdm = 1;
generateWIFIac;
createbounds;
wifiac     = ofdm;
F2wifiac   = F2ofdm;
CRBwifiac  = CRBofdm;
CRBDwifiac = dpCRBDofdm;
generateWIFIg;
createbounds;
wifig     = ofdm;
F2wifig   = F2ofdm;



aF2(1) = F2wifib;
aF2(2) = F2wifig;
aF2(3) = F2wifiac;
aF2(4) = F2wcdma;





% CN0 and Distance parameters
CN0 = 25:10:40;  % define CN0
D   = 1:25; % distance range
cno = 10.^(CN0./10);
go  = [];
ko  = [];



% Create the maps for each technology
lenD   = length(D);
lenCN0 = length(CN0);
lenF2  = length(aF2);
m(lenD,lenCN0) = 0;
figure(1);
for n = 1:lenF2;
    for k=1:lenD
        for l=1:lenCN0
            CRBDval(k,l,n) = sqrt(math.CRBD(D(k),cno(l),aF2(n),go,ko,simTime));
        end
    end
    mesh(D,CN0,CRBDval(:,:,n)');
    hold on;
    xlabel('Distance (m)');
    ylabel('CN0 (dB)');
    zlabel('\sigma (m) [CRBD]');
    xlim([min(D) max(D)]);
    ylim([min(CN0) max(CN0)]);
end

mval = max([CRBDval(k,l,1) CRBDval(k,l,2) CRBDval(k,l,3) CRBDval(k,l,4)]');

text(max(D),min(CN0),max(CRBDval(:,l,1)) ,'802.11b')
text(max(D),min(CN0),max(CRBDval(:,l,2)) ,'802.11g')
text(max(D),min(CN0),max(CRBDval(:,l,3)) ,'802.11ac')
text(max(D),min(CN0),max(CRBDval(:,l,4)),'WCDMA')


figure(2);
for n = 1:lenF2;
    for k=1:lenD
        for l=1:lenCN0
            CRBTval(k,l,n) = sqrt(math.CRBT(cno(l),aF2(n),simTime));
        end
    end
    mesh(D,CN0,CRBTval(:,:,n)');
    hold on;
    xlabel('Distance (m)');
    ylabel('CN0 (dB)');
    zlabel('\sigma (m) [CRBT]');
    xlim([min(D) max(D)]);
    ylim([min(CN0) max(CN0)]);
end
text(max(D),min(CN0),max(CRBTval(:,l,1)) ,'802.11b')
text(max(D),min(CN0),max(CRBTval(:,l,2)) ,'802.11g')
text(max(D),min(CN0),max(CRBTval(:,l,3)) ,'802.11ac')
text(max(D),min(CN0),max(CRBTval(:,l,4)),'WCDMA')


figure(3)
hold on;
plot(CN0,squeeze(CRBTval(1,:,:)),'b','linewidth',2);
plot(CN0,squeeze(CRBDval(1,:,:)),'r','linewidth',2);
legend('CRBT (b)','CRBD (r)');
ylabel('var');
xlabel('CN0');


figure(4)
hold on;
plot(D,squeeze(CRBTval(:,1,:))','b','linewidth',2);
plot(D,squeeze(CRBDval(:,1,:))','r','linewidth',2);
legend('CRBT (b)','CRBD (r)');
ylabel('var (s)');
xlabel('distance (m)');
