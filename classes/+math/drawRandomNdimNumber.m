function [nb] = drawRandomNdimNumber( limits )
    %DRAWNUMBER draws several random number from the limits
    %   eg, nb = (b-a).*rand(size(a)) + a; with 1st colum = a 
    
    
    a = limits(1,:);
    b = limits(2,:);
    assert(all(a<=b));
    
    nb = (b-a).*rand(size(a)) + a;
end

