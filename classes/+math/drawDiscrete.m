function nb = drawDiscrete( N, m, n )
    %DRAW draws a mxn random numbers from the range 1:N
    if ~exist('m','var')
        m=1;
    end
    if ~exist('n','var')
        n=1;
    end    
    nb = unidrnd(N,m,n);
    
end

