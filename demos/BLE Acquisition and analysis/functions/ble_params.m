%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%   TAMPERE UNIVERSITY OF TECHNOLOGY
%
%   Fitting BLE RSS measurements
%
%   This file contains the coordinates
%   used for the BLE acquisitions
%   at TUT and UNOTT.
%
%
%   Pedro Silva
%   pedro.silva@tut.fi
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Constants
fcarr           = 2.4e9;     % (#)   Operating frequency [Hz]
lspeed          = 299792458; % (#)   Ligth speec in vaccum [m/s]

% Manufacter constants
Ptx             = -12;       % (#)   Tx power (dBm)
manufacter_PLd0 = -77;       % (#)   Apparent power (dBm)

% Model parameters
model_freq_loss = -20*log10(fcarr*4*pi/lspeed); % (#) Loss at operating frequency
ube             = [0 +Inf];                     % (#) upper bound for estimation
xo              = [0 1];                        % (#) LSQ initial point
d0              = 1;                            % (#) Reference distance

if use_model_itu
    lbe         = [-Inf 1]; % (#) for ITU model it must be at least 1 (assuming 20*n*log10(-))
else
    lbe         = [-Inf 0]; % (#) lower bound for estimation (log_distance model)
end

if pl_manufacturer
    PLd0            = manufacter_PLd0; % (#) PLd0 reported by the manufacter
else
    PLd0            = tag_PLd0(ble_master_tag); % (#) PLd0 measured (1h)
end

% Coordinates
corr_width            = 1.22;
tag_coordinates_UNOTT = [...
                        1.5*3 corr_width 1.45;  % #1
                        1.5*3 0          1.5;   % #2
                        1.5*2   corr_width 1.5; % #3
                        1.5*2   0          1.5; % #4
                        1.5 corr_width 1.5;     % #5
                        1.5 0          1.5;     % #6
                        0   corr_width 1.5;     % #7
                        0   0          1.5;     % #8
                          ];

reader_pos           = [0           corr_width/2 0.5;
                        1.5+1.5/2 corr_width/2 0.5;
                        1.5*3     corr_width/2 0.5];

tag_coordinates_TUT = [ 0.05   0; %1
                        0.10   0; %2
                        0.50   0; %3
                        1      0; %4
                        1.5    0; %5
                        2      0; %6
                        2.5    0; %7
                        3      0];%8

theo_axis               = 0:0.01:5;

% Plots
mmk            = {'^','s','o'};
reader_pos_str = [{'A'},{'B'},{'C'}];

%EOF