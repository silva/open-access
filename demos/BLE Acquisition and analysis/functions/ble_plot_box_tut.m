%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%   TAMPERE UNIVERSITY OF TECHNOLOGY
%
%   Fitting BLE RSS measurements
%
%   This script draws box plots for the
%   measured data at TUT
%
%   Pedro Silva
%   pedro.silva@tut.fi
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


% inputs
minx        = +Inf(n_ble_files,1);
maxx        = -Inf(n_ble_files,1);

miny        = +Inf(n_ble_files,1);
maxy        = -Inf(n_ble_files,1);

x_margin    = 0;
y_margin    = 10;
med_marg    = 0.1;

n_ble       = size(ble_rss,1);

% Plot control

nvec  = 1:n_ble;
for k=1:n_ble_files
    
    csum      = cellfun('length',ble_rss(:,k));
    rss_cell  = NaN(sum(csum),sum(csum~=0));
    rss_label = [];
    cnt=0;
    for tid = 1:n_ble
        if isempty(ble_rss{tid,k})
            continue;
        end;
        cnt=cnt+1;
        
        % Puts data to a cell, not that it has to be NaN otherwise the
        % boxplot will take into account the 0s in the vectors!
        rss_cell(1:length(ble_rss{tid,k}),cnt) = ble_rss{tid,k}(:);
        rss_cell(rss_cell(:,cnt)==0,cnt)       = NaN;
        rss_label{cnt}                         = ['BLE ',num2str(tid)];
    end
    
    % Sotre in a matrix cell for later use
    id_vec      = nvec(csum~=0);
    box_db{k,1} = rss_cell;
    box_db{k,2} = rss_label;
    
    if isempty(id_vec), continue; end;
    
    % Plots box plots and applies color patch
    xx = tag_coordinates_TUT(k,1);
    boxplot(hPlot,rss_cell,'Labels',rss_label,'symbol','k.','medianstyle','line','position',xx);%,'labelorientation','horizontal');
    h = findobj(hPlot,'Tag','Box');
    cnt=0;
    plot(hPlot,xx,nanmean(rss_cell),'xk','Markersize',8,'Linewidth',2); % mean
    for tid=id_vec
        cnt=cnt+1;
        plot(hPlot,[(xx-med_marg):0.1:(xx+med_marg)],([(xx-med_marg):0.1:(xx+med_marg)].*0+1)*nanmedian(rss_cell(:,cnt)),'-k','Markersize',8,'Linewidth',3); % mean
        patch(get(h(cnt),'XData'),get(h(cnt),'YData'),ble_device_identity{tid,1},'FaceAlpha',.5,'Parent', hPlot);
    end
    
end

% Adjust axis limits
xlim(hPlot,[-0.1 3.5]);
ylim(hPlot,[-100 -40]);
set(hPlot,'xtickmode','auto')
set(hPlot,'xticklabelmode','auto')
ylabel(hPlot,'RSS (dBm)','Interpreter','Latex');
xlabel(hPlot,'Distance (m)','Interpreter','Latex');


%EOF