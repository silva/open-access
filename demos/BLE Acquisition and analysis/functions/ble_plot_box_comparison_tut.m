%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%   TAMPERE UNIVERSITY OF TECHNOLOGY
%
%   Fitting BLE RSS measurements
%
%   This script draws box plots for the
%   measured data at TUT
%
%   Pedro Silva
%   pedro.silva@tut.fi
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% index 1 contains data from interface on
% index 2 contains data from interface off


%% for Wi-Fi interface on
subplot(hDist.hAxis);
hPlot    = subplot(2,1,1);
hold(hPlot,'on');
box(hPlot,'off');
grid(hPlot,'on');


% switch data set
ble_rss       = store_rss{1}; 
sel_xx        = 1:25:length(theo_axis);
sel_xx2       = 10:25:length(theo_axis);
model_xx      = theo_axis;
model_yy(1,:) = model_itur(store_PLO(1),theo_axis,store_eta(1));
model_yy(2,:) = model_itur(store_PLO(2),theo_axis,store_eta(2));
plot(hPlot,model_xx(sel_xx),model_yy(1,sel_xx),'g^-','linewidth',1,'markerfacecolor','g');
plot(hPlot,model_xx(sel_xx2),model_yy(2,sel_xx2),'bs-','linewidth',1,'markerfacecolor','b');

ble_plot_box_tut;

plot(hPlot,model_xx,model_yy(1,:),'g-','linewidth',1);
plot(hPlot,model_xx,model_yy(2,:),'b-','linewidth',1);

% Title and legend
if use_model_itu 
    title(hPlot,['Wi-Fi interface on, $C=',num2str(store_L(1),'%2.2f'),'$, $\eta =',num2str(store_eta(1),'%2.2f'),'$'],'Interpreter','Latex');
else
    title(hPlot,['Wi-Fi interface on $P_r(d_0)=',num2str(PLd0,'%2.2f'),'$, $\eta =',num2str(store_eta(1),'%2.2f'),'$'],'Interpreter','Latex');
end
    
hl=legend(hPlot,'Model fit Wi-Fi ON','Model fit Wi-Fi OFF','Mean RSS','Median','location','northeast');
set(hl,'interpreter','latex','units','normalized');   
hlp=get(hl,'position');
set(hl,'position',hlp+[0.02 0.03 0 0]);



%% for Wi-Fi interface off
hPlot    = subplot(2,1,2);
hold(hPlot,'on');
box(hPlot,'off');
grid(hPlot,'on');


% switch data set
ble_rss       = store_rss{2};
sel_xx        = 1:25:length(theo_axis);
sel_xx2       = 10:25:length(theo_axis);
model_xx      = theo_axis;
model_yy(1,:) = model_itur(store_PLO(1),theo_axis,store_eta(1));
model_yy(2,:) = model_itur(store_PLO(2),theo_axis,store_eta(2));
plot(hPlot,model_xx(sel_xx),model_yy(1,sel_xx),'g^-','linewidth',1,'markerfacecolor','g');
plot(hPlot,model_xx(sel_xx2),model_yy(2,sel_xx2),'bs-','linewidth',1,'markerfacecolor','b');

ble_plot_box_tut;

plot(hPlot,model_xx,model_yy(1,:),'g-','linewidth',1);
plot(hPlot,model_xx,model_yy(2,:),'b-','linewidth',1);

if use_model_itu 
    title(hPlot,['Wi-Fi interface on, $C=',num2str(store_L(2),'%2.2f'),'$, $\eta =',num2str(store_eta(2),'%2.2f'),'$'],'Interpreter','Latex');
else
    title(hPlot,['Wi-Fi interface on $P_r(d_0)=',num2str(PLd0,'%2.2f'),'$, $\eta =',num2str(store_eta(2),'%2.2f'),'$'],'Interpreter','Latex');
end

hl=legend(hPlot,'Model fit Wi-Fi ON','Model fit Wi-Fi OFF','Mean RSS','Median','location','northeast');
set(hl,'interpreter','latex','units','normalized');   
hlp=get(hl,'position');
set(hl,'position',hlp+[0.02 0.03 0 0]);

% For printing purposes
hDist.hAxis=hPlot;
