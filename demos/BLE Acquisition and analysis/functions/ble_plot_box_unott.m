%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%   TAMPERE UNIVERSITY OF TECHNOLOGY
%
%   Fitting BLE RSS measurements 
%
%   This script plots several boxplots
%   for each measurement point of UNOTT
%
%   Pedro Silva
%   pedro.silva@tut.fi
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% output
hBox  = inout.output;
hBox.nfigure(n_ble_files);
hPlot = hBox.hAxis;

% inputs
minx = +Inf(n_ble_files,1);
maxx = -Inf(n_ble_files,1);

miny = +Inf(n_ble_files,1);
maxy = -Inf(n_ble_files,1);

x_margin = 0;
y_margin = 10;

y_label_str = 'RSS (dBm)';
x_label_str = '$T-T_0$ (s)';

plot_legend = ble_leg;
n_ble       = size(ble_rss,1);
nvec        = 1:n_ble;

for k=1:n_ble_files
    hPlot = hBox.hAxis(k);
    
    csum     = cellfun('length',ble_rss(:,k));
    rss_cell = NaN(sum(csum),sum(csum~=0));
    
    rss_lb   =[];
    cnt=0;
    for tid = 1:n_ble
        if isempty(ble_rss{tid,k})
            continue;
        end;
        cnt=cnt+1;
        rss_cell(1:length(ble_rss{tid,k}),cnt) = ble_rss{tid,k}(:);
        rss_cell(rss_cell(:,cnt)==0,cnt)=NaN;
        rss_lb{cnt} = ['BLE ',num2str(tid)];
        
    end
    
    id_vec = nvec(csum~=0);
    box_db{k,1}=rss_cell;
    box_db{k,2}=rss_lb;
    
    if isempty(id_vec), continue; end;
    
    boxplot(hPlot,rss_cell,'Labels',rss_lb,'symbol','k.','medianstyle','line','position',id_vec);%,'labelorientation','horizontal');
    h = findobj(hPlot,'Tag','Box');
    cnt=0;
    plot(hPlot,id_vec,nanmean(rss_cell),'xk','Markersize',8,'Linewidth',2); % mean
    for tid=id_vec
        cnt=cnt+1;
        patch(get(h(cnt),'XData'),get(h(cnt),'YData'),ble_device_identity{tid,1},'FaceAlpha',.5,'Parent', hPlot);
        plot(hPlot,[(tid-0.3):0.1:(tid+0.3)],([(tid-0.3):0.1:(tid+0.3)].*0+1)*nanmedian(rss_cell(:,cnt)),'-k','Markersize',8,'Linewidth',2); % mean
    end
    
    
    % Adjust axis limits
    box(hPlot,'off');
%     xlim(hPlot,[-0.1 3.5]);
%     ylim(hPlot,[-100 -40]);
    set(hPlot,'xtickmode','auto')
    set(hPlot,'xticklabelmode','auto')
    ylabel(hPlot,'RSS (dBm)','Interpreter','Latex');
    xlabel(hPlot,'Distance (m)','Interpreter','Latex');
    set(hPlot,'xtickmode','auto')
    set(hPlot,'xticklabelmode','auto')
    
end


