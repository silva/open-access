% Model for all on, cases 1 through 3
col_idx = 1;
scn     = [1 2 3];
for tid=minId:maxId
    tpos         = tag_coordinates_UNOTT(tid,:);
    tag_distance = math.distance(tpos,reader_pos);
    
    csum  = cellfun('length',ble_rss(tid,scn(1):scn(end)));
    mdist = [ones(csum(1),1).*tag_distance(1);...
        ones(csum(2),1).*tag_distance(2);...
        ones(csum(3),1).*tag_distance(3)];
    
    Nmeas = sum(csum);
    d     =  [ble_rss{tid,scn(1)}(:);ble_rss{tid,scn(2)}(:);ble_rss{tid,scn(3)}(:)];
    ble_run_lsq;
        
    clear d
    clear tag_distance
    clear mdist
    clear C
end

%% change col idx
col_idx      = 2;


%% MODEL for TAG 1
scn          = [7 8 9];
tid          = 1;
tpos         = tag_coordinates_UNOTT(tid,:);
tag_distance = math.distance(tpos,reader_pos);

% preprares data
csum  = cellfun('length',ble_rss(tid,scn(1):scn(end)));
mdist = [ones(csum(1),1).*tag_distance(1);...
         ones(csum(2),1).*tag_distance(2);...
         ones(csum(3),1).*tag_distance(3)];
Nmeas = sum(csum);
d     =  [ble_rss{tid,scn(1)}(:);ble_rss{tid,scn(2)}(:);ble_rss{tid,scn(3)}(:)];
ble_run_lsq;


%% MODEL for TAG 2
scn  =[7 8 9];
tid  = 2;
tpos = tag_coordinates_UNOTT(tid,:);
tag_distance = math.distance(tpos,reader_pos);

% prepares data
csum  = cellfun('length',ble_rss(tid,scn(1):scn(end)));
mdist = [ones(csum(1),1).*tag_distance(1);...
    ones(csum(2),1).*tag_distance(2);...
    ones(csum(3),1).*tag_distance(3)];
Nmeas = sum(csum);
d     =  [ble_rss{tid,scn(1)}(:);ble_rss{tid,scn(2)}(:);ble_rss{tid,scn(3)}(:)];
ble_run_lsq;

%% MODEL for TAG 3
scn=[10 11 12];
tid = 3;
tpos = tag_coordinates_UNOTT(tid,:);
tag_distance = math.distance(tpos,reader_pos);

% prepares data
csum  = cellfun('length',ble_rss(tid,scn(1):scn(end)));
mdist = [ones(csum(1),1).*tag_distance(1);...
    ones(csum(2),1).*tag_distance(2);...
    ones(csum(3),1).*tag_distance(3)];
Nmeas = sum(csum);
d     =  [ble_rss{tid,scn(1)}(:);ble_rss{tid,scn(2)}(:);ble_rss{tid,scn(3)}(:)];
ble_run_lsq;


%% MODEL for TAG 7
scn=[5 6];
tid = 7;
tpos = tag_coordinates_UNOTT(tid,:);
tag_distance = math.distance(tpos,reader_pos);

% prepares data
csum  = cellfun('length',ble_rss(tid,scn(1):scn(end)));
mdist = [ones(csum(1),1).*tag_distance(1);...
    ones(csum(2),1).*tag_distance(2)];
Nmeas = sum(csum);
d     =  [ble_rss{tid,scn(1)}(:);ble_rss{tid,scn(2)}(:)];
ble_run_lsq;



%% MODEL for TAG 8
scn=[5 6];
tid = 8;
tpos = tag_coordinates_UNOTT(tid,:);
tag_distance = math.distance(tpos,reader_pos);

csum  = cellfun('length',ble_rss(tid,scn(1):scn(end)));
mdist = [ones(csum(1),1).*tag_distance(1);...
    ones(csum(2),1).*tag_distance(2)];
Nmeas = sum(csum);
d     =  [ble_rss{tid,scn(1)}(:);ble_rss{tid,scn(2)}(:)];
ble_run_lsq;


%% Pathloss
assert(Ptx <0 & all(all(L<=0)) & model_freq_loss <0)
PL0 = Ptx+L+model_freq_loss;

%% Plot Pt and Eta
hPtEta = inout.output;
hPtEta.figure(1);
% subplot(get(hPtEta.hAxis,'parent'));
subplot(hPtEta.hAxis);
hPlot=subplot(2,1,1);
hold(hPlot,'on');
plot(hPlot,1:8,L(:,1)','b^');
plot(hPlot,[1:3 7:8],[L(1:3,2);L(7:8,2)]','go');
ylabel(hPlot,'Apparent tx power, $P_t$ (dBm)','interpreter','latex');
legend(hPlot,'Low Inter.','High Inter.','Location','best')
xlabel(hPlot,'BLE tag','interpreter','latex');
% ylabel(hPlot,'RSS (dBm)','interpreter','latex');

hPlot=subplot(2,1,2);
hold(hPlot,'on');
plot(hPlot,1:8,eta(:,1),'bs');
plot(hPlot,[1:3 7:8],[eta(1:3,2);eta(7:8,2)],'g*');
ylabel(hPlot,'Pathloss coefficient, $\eta$','interpreter','latex');
legend(hPlot,'Low Inter.','High Inter.','Location','best')
xlabel(hPlot,'BLE tag','interpreter','latex','interpreter','latex');


