%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%   TAMPERE UNIVERSITY OF TECHNOLOGY
%
%   This script evaluates the model fit for the UNOTT observations
%
%
%   Pedro Silva
%   pedro.silva@tut.fi
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

try
    clear d_emo
end

coord = tag_distance;


% compute difference
col_idx = 1;
N     = size(coord(:,1),1);
for n=1:length(model_py)
    for k=1:N
        d_emo(k,1)   = coord(k);
        y_pred       = model_py{n}(k);
        y_obs        = model_my{col_idx}(k); 
        d_emo(k,n+1) = sqrt(mean((y_obs-y_pred).^2));
    end
    if n == 4
    col_idx = 2;
    end
end

d_emo = [d_emo; mean(d_emo,1);];


if use_model_itu
    disp(['    xx | itu_r | itu_r (previous) '])
    disp(d_emo)
else
    disp(['    xx |  itu_r | itu_r (office) | log dist | itur (opposite)'])
    disp(d_emo)
end

clear model_yy
clear model_xx