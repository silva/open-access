function [ rmse ] = rmse( xr, xe )
    %RMSE Summary of this function goes here
    %   Detailed explanation goes here
%     xe   = est(q,:);
%     xr   = userxy;
    dist = sqrt(sum((xe(:)-xr(:)).^2,2));
    rmse = sqrt(mean(dist.^2));
    
end

