%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%   TAMPERE UNIVERSITY OF TECHNOLOGY
%
%   Fitting BLE RSS measurements 
%
%   This script plots several boxplots
%   for each measurement point of UNOTT
%
%   Pedro Silva
%   pedro.silva@tut.fi
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% output
hold(hPlot,'on');
n_ble = size(ble_rss,1);
nvec  = 1:n_ble;
for k=1:size(ble_rss,2)
    
    csum     = cellfun('length',ble_rss(:,k));
    rss_cell = NaN(sum(csum),sum(csum~=0));
    
    rss_label   =[];
    cnt=0;
    for tid = 1:n_ble
        if isempty(ble_rss{tid,k})
            continue;
        end;
        cnt=cnt+1;
        rss_cell(1:length(ble_rss{tid,k}),cnt) = ble_rss{tid,k}(:);
        rss_cell(rss_cell(:,cnt)==0,cnt)=NaN;
        rss_label{cnt} = ['BLE ',num2str(tid)];
        
    end
    
    id_vec = nvec(csum~=0);
    box_db{k,1}=rss_cell;
    box_db{k,2}=rss_label;
    
    if isempty(id_vec), continue; end;
    
    boxplot(hPlot,rss_cell,'Labels',rss_label,'symbol','k.','medianstyle','line','position',id_vec);%,'labelorientation','horizontal');
    h = findobj(hPlot,'Tag','Box');
    cnt=0;
    plot(hPlot,id_vec,nanmean(rss_cell),'xk','Markersize',8,'Linewidth',2); % mean
    for tid=id_vec
        cnt=cnt+1;
        patch(get(h(cnt),'XData'),get(h(cnt),'YData'),ble_device_identity{tid,1},'FaceAlpha',.5,'Parent', hPlot);
        plot(hPlot,[(tid-0.3):0.1:(tid+0.3)],([(tid-0.3):0.1:(tid+0.3)].*0+1)*nanmedian(rss_cell(:,cnt)),'-k','Markersize',8,'Linewidth',2); % mean
    end
    
    
    % Adjust axis limits
    box(hPlot,'off');
    set(hPlot,'xtickmode','auto')
    set(hPlot,'xticklabelmode','auto')
    ylabel(hPlot,'RSS (dBm)','Interpreter','Latex');
    xlabel(hPlot,'BLE tag','Interpreter','Latex');
    set(hPlot,'xtickmode','auto')
    set(hPlot,'xticklabelmode','auto')
    
end

xlim(hPlot,[1 12]);
ylim(hPlot,[-100 -60]);
box(hPlot,'off');
grid(hPlot,'on');


