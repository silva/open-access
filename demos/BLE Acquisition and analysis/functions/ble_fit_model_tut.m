%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%   TAMPERE UNIVERSITY OF TECHNOLOGY
%
%   Fitting BLE RSS measurements 
%
%   This script fits a constrained LSQ
%   solution to the measurement RSS 
%   data acquired at TUT
%
%   Pedro Silva
%   pedro.silva@tut.fi
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Fit RSS model
tid          = ble_master_tag;
tpos         = tag_coordinates_TUT;
tag_distance = math.distance(tpos,[0,0]); %sanity

%%% LSQ solution
% Prepares observation vector (d)
d     = [];
Nmeas = 0;
mdist = [];
for k=1:length(tag_distance)
    nelems = length(ble_rss{tid,k});
    mdist  = [mdist;ones(nelems,1).*tag_distance(k)];
    d      = [d;ble_rss{tid,k}(:)];
    Nmeas  = nelems+Nmeas;
end

ble_run_lsq;

disp('Estimated model parameters')
disp(['Pathloss coef. : n = ',num2str(eta)]);
disp(['System losses  : C = ',num2str(L)]);
disp(['Model equation']);
disp(['Pr(d) = ',num2str(PL0,'%2.2f'),' - ',num2str(20*eta,'%2.2f'),'*log(d)']);

% free space
clear d
clear tag_distance
clear mdist
clear C

%EOF