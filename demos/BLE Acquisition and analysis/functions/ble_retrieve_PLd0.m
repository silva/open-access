%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%   TAMPERE UNIVERSITY OF TECHNOLOGY
%
%   Retrieves measurement data (1h) at 1 m from tag (empirical PLd0)
%
%   Pedro Silva
%   pedro.silva@tut.fi
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clear variables
close all

global cmap;
h=figure;
cmap = jet;
close(h);

%% INPUTS
print_figures = 1;
fig_out_type  = '-png'; % ([s]) figure format (supported by export_fig/matlab)
datadir       = 'resultsTUT_POWER/';

hPLd0 = inout.output;
hPLd0.figure(1);
hPlot = hPLd0.hAxis;

%% Retrieving data
ble_load_data;
ble_plot_box_multiple;
title(hPlot,'Path-loss at 1 m');

ble_mean_mat(isnan(ble_mean_mat))=0;
tag_PLd0 = sum(ble_mean_mat,2);

%% PRINTING
if print_figures == 1
    hPLd0.print(['pub/TUT_box_compare'],fig_out_type);
end

% Cleanup [IMPORTANT!]
clearvars -except tag_PLd0 

%EOF