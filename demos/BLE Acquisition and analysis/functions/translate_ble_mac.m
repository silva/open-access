function [nbl,nbc,mrk,idcolor]=translate_ble_mac(mac)
    
    len  = length(mac);
    nbl  = nan(len,1);
    nbc  = cell(len,1);
    mrk  = cell(len,1);
    
    global cmap;
    cmap_idx = [1:5:length(cmap)];
    idcolor  = nan(len,3);
    
    for k=1:len
        
        for d=1:8
              
            if strfind(mac{k}, '54321-45350')
                nbl(k)       = 1;
                nbc{k}       = ['BLE ',num2str(nbl(k))];
                mrk{k}       = '*';
                idcolor(k,:) = cmap(cmap_idx(nbl(k)),:);
                continue;
            end
            
            if strfind(mac{k}, '31603-41982')
                nbl(k)  = 2;
                nbc{k}  = ['BLE ',num2str(nbl(k))];
                mrk{k} = '^';
                idcolor(k,:) = cmap(cmap_idx(nbl(k)),:);
                continue;
            end
            
            if strfind(mac{k}, '32025-26801')
                nbl(k) = 3;
                nbc{k} = ['BLE ',num2str(nbl(k))];
                mrk{k} = 'p';
                idcolor(k,:) = cmap(cmap_idx(nbl(k)),:);
                continue;
            end
            
            if strfind(mac{k}, '41350-61607')
                nbl(k) = 4;
                nbc{k} = ['BLE ',num2str(nbl(k))];
                mrk{k} = 's';
                idcolor(k,:) = cmap(cmap_idx(nbl(k)),:);
                continue;
            end
            
            if strfind(mac{k}, '27459-32550')
                nbl(k) = 5;
                nbc{k} = ['BLE ',num2str(nbl(k))];
                mrk{k} = '>';
                idcolor(k,:) = cmap(cmap_idx(nbl(k)),:);
                continue;
            end
            
            if strfind(mac{k}, '64628-57921')
                nbl(k) = 6;
                nbc{k} = ['BLE ',num2str(nbl(k))];
                mrk{k} = 'o';
                idcolor(k,:) = cmap(cmap_idx(nbl(k)),:);
                continue;
            end
            
            if strfind(mac{k}, '21723-46717')
                nbl(k) = 7;
                nbc{k} = ['BLE ',num2str(nbl(k))];
                mrk{k} = '<';
                idcolor(k,:) = cmap(cmap_idx(nbl(k)),:);
                continue;
            end
            
            if strfind(mac{k}, '22245-7762')
                nbl(k) = 8;
                nbc{k} = ['BLE ',num2str(nbl(k))];
                mrk{k} = 'd';
                idcolor(k,:) = cmap(cmap_idx(nbl(k)),:);
                continue;
            end
            
            
            %%% TAGS IN FINLAND
            if strfind(mac{k}, '10764-20350')
                nbl(k) = 10;
                nbc{k} = ['BLE ',num2str(nbl(k))];
                mrk{k} = 'd';
                idcolor(k,:) = cmap(cmap_idx(nbl(k)),:);
                continue;
            end
			
			if strfind(mac{k}, '38925-52352')
                nbl(k) = 11;
                nbc{k} = ['BLE ',num2str(nbl(k))];
                mrk{k} = 'd';
                idcolor(k,:) = cmap(cmap_idx(nbl(k)),:);
                continue;
            end
			
			
			if strfind(mac{k}, '21296-2309')
                nbl(k) = 12;
                nbc{k} = ['BLE ',num2str(nbl(k))];
                mrk{k} = 'd';
                idcolor(k,:) = cmap(cmap_idx(nbl(k)),:);
                continue;
            end
			
			
        end
    end
    
end