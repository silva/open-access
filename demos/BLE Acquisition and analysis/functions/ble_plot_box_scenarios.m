
%% BOX PLOTS

% 1 - strong 
% 2 - weak interferance
m_margin    = 0.2;
xmin        = 0;
xmax        = 5;

ymin        = -100;
ymax        = -40;
x_label_str = 'Distance (m)';

% Plot control
hBoxModel = inout.output;
hBoxModel.nfigure(2);
hPlot = hBoxModel.hAxis(1);

%% WEAK
%  For tag BLE_MASTER_TAG
tid          = ble_master_tag;
scn          = [10 11 12];
tpos         = tag_coordinates_UNOTT(tid,:);
tag_distance = math.distance(tpos,reader_pos);
csum         = cellfun('length',box_db(scn,1));
rss_cell     = NaN(sum(csum),sum(csum~=0));
for m = 1:length(scn)
    rss_cell(1:csum(m),m) = box_db{scn(m),1};
    rss_lb{m} = [num2str(tag_distance(m))];
end
id_vec = nvec(csum~=0);
if isempty(id_vec), assert(0==1); end;

% Fitted models
model_xx    = theo_axis;
sxx         = [1:3:10,25:25:length(model_xx)];
model_yy{5} = model_itur(PL_weak,theo_axis,eta_weak);
model_yy{6} = model_logdist(-77,theo_axis,0.9357);
model_yy{7} = model_logdist(PL_log_dist_weak,theo_axis,eta_log_dist_weak);
model_yy{8} = model_itur(corridor_PL0_s,theo_axis,corridor_eta_s);

model_py{5} = model_itur(PL_weak,tag_distance,eta_weak);
model_py{6} = model_logdist(-77,tag_distance,0.9357);
model_py{7} = model_logdist(PL_log_dist_weak,tag_distance,eta_log_dist_weak);
model_py{8} = model_itur(corridor_PL0_s,tag_distance,corridor_eta_s);

model_my{2} = nanmean(rss_cell);

if use_model_itu
    plot(hPlot,model_xx,model_yy{5},'linewidth',2);
    plot(hPlot,model_xx,model_yy{8},'--','linewidth',2,'color','c');
else
    plot(hPlot,model_xx(sxx),model_yy{7}(sxx),'o-','linewidth',2,'color','g','markerfacecolor','g'); 
    plot(hPlot,model_xx(sxx),model_yy{6}(sxx),'^-','linewidth',2,'color','c','markerfacecolor','c'); 
    plot(hPlot,model_xx(sxx),model_yy{8}(sxx),'s-','linewidth',2,'color','b','markerfacecolor','b');
end

% Box plot
boxplot(hPlot,rss_cell,'symbol','k.','Labels',rss_lb,'medianstyle','line','position',tag_distance);%,'labelorientation','horizontal');
h = findobj(hPlot,'Tag','Box');
cnt=0;
plot(hPlot,tag_distance,nanmean(rss_cell),'xk','Markersize',8,'Linewidth',2); % mean

for k=1:length(tag_distance)
    cnt=cnt+1;
    plot(hPlot,[(tag_distance(k)-m_margin):0.1:(tag_distance(k)+m_margin)],([(tag_distance(k)-m_margin):0.1:(tag_distance(k)+m_margin)].*0+1)*nanmedian(rss_cell(:,cnt)),'-k','Markersize',8,'Linewidth',2); % mean
    patch(get(h(cnt),'XData'),get(h(cnt),'YData'),ble_device_identity{tid,1},'FaceAlpha',.5,'Parent', hPlot);
end

% title and legends
ylim(hPlot,[ymin ymax]);
xlim(hPlot,[xmin xmax]);
box(hPlot,'off');
set(hPlot,'xtickmode','auto')
set(hPlot,'xticklabelmode','auto')


if use_model_itu 
    title(hPlot,['BLE ',num2str(tid),' in weak interference, ','$C=',num2str(PL_weak,'%2.2f'),'$, $\eta =',num2str(eta_weak,'%2.2f'),'$'],'Interpreter','Latex');
else
%     title(hPlot,['BLE ',num2str(tid),' in weak interference, ','$P_r(d_0)=',num2str(PLd0,'%2.2f'),'$, $\eta =',num2str(eta_weak,'%2.2f'),'$'],'Interpreter','Latex');
        title(hPlot,['1 tag broadcasting'],'Interpreter','Latex');
end
ylabel(hPlot,y_label_str,'Interpreter','Latex');
xlabel(hPlot,x_label_str,'Interpreter','Latex');
if use_model_itu 
    hl=legend(hPlot,'weak model fit','strong model fit','Mean RSS','Median','25$^{th}$ - 75$^{th}$ Percentile');
else
    hl=legend(hPlot,'Log-distance model','ITU-R model','Mean RSS','Median','25$^{th}$ - 75$^{th}$ Percentile');
end
set(hl,'interpreter','latex');


hPlot = hBoxModel.hAxis(2);


%% STRONG
%  For tag BLE_MASTER_TAG
scn          = [1 2 3];
tpos         = tag_coordinates_UNOTT(tid,:);
tag_distance = math.distance(tpos,reader_pos);

csum     = cellfun('length',box_db(scn,1));
rss_cell = NaN(sum(csum),sum(csum~=0));
for m = 1:length(scn)
    rss_cell(1:csum(m),m) = box_db{scn(m),1}(:,tid);
    rss_lb{m} = [num2str(tag_distance(m))];
end
id_vec = nvec(csum~=0);
if isempty(id_vec), assert(0==1); end;


% Fitted models
model_xx    = theo_axis;
model_yy{1} = model_itur(PL_strong,theo_axis,eta_strong); % current estimation
model_yy{2} = model_logdist(-77,theo_axis,0.9583); % obtained from TUT data
model_yy{3} = model_logdist(PL_log_dist_strong,theo_axis,eta_log_dist_strong);
model_yy{4} = model_itur(corridor_PL0_w,theo_axis,corridor_eta_w);

model_py{1} = model_itur(PL_strong,tag_distance,eta_strong);
model_py{2} = model_logdist(-77,tag_distance,0.9583);
model_py{3} = model_logdist(PL_log_dist_strong,tag_distance,eta_log_dist_strong);
model_py{4} = model_itur(corridor_PL0_w,tag_distance,corridor_eta_w);

model_my{1} = nanmean(rss_cell);

if use_model_itu
    plot(hPlot,model_xx,model_yy{1},'-','linewidth',2,'color','g');  % log_distance 
    plot(hPlot,model_xx,model_yy{3},'--','linewidth',2,'color','c'); % from TUT
else
    plot(hPlot,model_xx(sxx),model_yy{3}(sxx),'o-','linewidth',2,'color','g','markerfacecolor','g');  % log_distance 
    plot(hPlot,model_xx(sxx),model_yy{2}(sxx),'^-','linewidth',2,'color','c','markerfacecolor','c');  % log_distance 
    plot(hPlot,model_xx(sxx),model_yy{4}(sxx),'s-','linewidth',2,'color','b','markerfacecolor','b'); % ITU fitted to corridor data
end


% boxplot
boxplot(hPlot,rss_cell,'symbol','k.','Labels',rss_lb,'medianstyle','line','position',tag_distance);%,'labelorientation','horizontal');
h = findobj(hPlot,'Tag','Box');
cnt=0;
plot(hPlot,tag_distance,nanmean(rss_cell),'xk','Markersize',8,'Linewidth',2); % mean
for k=1:length(tag_distance)
    cnt=cnt+1;
    plot(hPlot,[(tag_distance(k)-m_margin):0.1:(tag_distance(k)+m_margin)],([(tag_distance(k)-m_margin):0.1:(tag_distance(k)+m_margin)].*0+1)*nanmedian(rss_cell(:,cnt)),'-k','Markersize',8,'Linewidth',2); % mean
    patch(get(h(cnt),'XData'),get(h(cnt),'YData'),ble_device_identity{tid,1},'FaceAlpha',.5,'Parent', hPlot);
end

% Legend and title
ylim(hPlot,[ymin ymax]);
xlim(hPlot,[xmin xmax]);
box(hPlot,'off');
set(hPlot,'xtickmode','auto')
set(hPlot,'xticklabelmode','auto')

if use_model_itu 
    title(hPlot,['BLE ',num2str(tid),' in strong interference, ','$C=',num2str(PL_strong,'%2.2f'),'$, $\eta =',num2str(eta_strong,'%2.2f'),'$'],'Interpreter','Latex');
else
    title(hPlot,['8 tags broadcasting'],'Interpreter','Latex');
end
ylabel(hPlot,y_label_str,'Interpreter','Latex');
xlabel(hPlot,x_label_str,'Interpreter','Latex');

if use_model_itu 
    hl=legend(hPlot,'strong model fit','weak model fit','Mean RSS','Median','25$^{th}$ - 75$^{th}$ Percentile');
else
%     hl=legend(hPlot,'Log-distance model','ITU-R model','Mean RSS','Median','25$^{th}$ - 75$^{th}$ Percentile');
    hl=legend(hPlot,['Log dist fit ($P_r(d_0)$ estimated)'],['Log dist fit ($P_r(d_0)$ reported)'],['ITU-R fit'],'Mean RSS','Median','location','northeast');
end
set(hl,'interpreter','latex');
