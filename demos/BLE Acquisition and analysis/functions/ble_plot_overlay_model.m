%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%   TAMPERE UNIVERSITY OF TECHNOLOGY
%
%   Fitting BLE RSS measurements
%
%   This script creates anonymous
%   functions to the fitted and
%   ideal models
%
%   Pedro Silva
%   pedro.silva@tut.fi
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

try
    clear model_yy
end

model_xx  = theo_axis;
if use_model_itu
    
    % yy values
    model_yy{1} = model_itur(PL0,theo_axis,eta);
    model_yy{2} = model_itur(fitted_PL0,theo_axis,fitted_eta);
    
    plot(hPlot,model_xx,model_yy{1},'-','color',cstate,'linewidth',2);
    % plot(hPlot,theo_axis,model_itur(theo_axis),'c-','linewidth',2);
    if overlay_other
        plot(hPlot,model_xx,model_yy{2},'--','color',ncstate,'linewidth',2);
    end
    
else
    
    % yy values
    model_yy{1} = model_logdist(PLd0,theo_axis,eta);
    model_yy{2} = model_logdist(-77,theo_axis,1.0182);
    model_yy{4} = model_itur(fit_itu_PL_on,theo_axis,fit_itu_eta_on);
    
    % plotting
    sxx = [1:3:10,25:25:length(model_xx)];
    yy  = model_yy{1}(sxx);
    yy(isinf(yy)) = 0;
    plot(hPlot,model_xx(sxx),yy,'-','color',cstate,'marker',cmarker,'markerfacecolor',cstate,'linewidth',2);
    if overlay_other
        yy  = model_yy{2}(sxx);
        yy(isinf(yy)) = 0;
        plot(hPlot,model_xx(sxx),yy,'b-','marker','s','markerfacecolor','b','linewidth',2);
        yy  = model_yy{4}(sxx);
        yy(isinf(yy)) = 0;
        plot(hPlot,model_xx(sxx),yy,'c-','marker','^','markerfacecolor','c','linewidth',2);
    end
end

%EOF