function [nbl,nbc,mrk]=translate_ble_id(mac_id)
    
    len  = length(mac_id);
    nbl  = nan(len,1);
    nbc  = cell(len,1);
    mrk = cell(len,1);
    
    for k=1:len
        
        switch(mac_id(k))
            
            case 1
                nbl(k)  = 1;
                nbc{k}  = '54321-45350';
                mrk{k} = '*';
                continue;
                
                
            case 2
                nbl(k)  = 2;
                nbc{k}  = '31603-41982';
                mrk{k} = '^';
                continue;
                
                
            case 3
                nbl(k) = 3;
                nbc{k} = '32025-26801';
                mrk{k} = '+';
                continue;
                
                
            case 4
                nbl(k) = 4;
                nbc{k} = '41350-61607';
                mrk{k} = 's';
                continue;
                
                
            case 5
                nbl(k) = 5;
                nbc{k} = '27459-32550';
                mrk{k} = '.';
                continue;
                
                
            case 6
                nbl(k) = 6;
                nbc{k} = '64628-57921';
                mrk{k} = 'o';
                continue;
                
                
            case 7
                nbl(k) = 7;
                nbc{k} = '21723-46717';
                mrk{k} = '<';
                continue;
                
                
            case 8
                nbl(k) = 8;
                nbc{k} = '22245-7762';
                mrk{k} = 'd';
                continue;
                
        end
    end
end
