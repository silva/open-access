%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%   TAMPERE UNIVERSITY OF TECHNOLOGY
%
%   Plots the time series for each measurement file
%
%   Pedro Silva
%   pedro.silva@tut.fi
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% inputs
minx = +Inf(n_ble_files,1);
maxx = -Inf(n_ble_files,1);

miny = +Inf(n_ble_files,1);
maxy = -Inf(n_ble_files,1);

x_margin = 0;
y_margin = 10;

y_label_str = 'RSS (dBm)';
x_label_str = '$T-T_0$ (s)';

plot_legend = ble_leg;

% Plot control
hTM   = inout.output;
hTM.nfigure(n_ble_files,1);


for k=1:n_ble_files
    hPlot = hTM.hAxis(k);
    
    % Now for each entry
    for tid = minId:maxId
        if isempty(ble_rss{tid,k}), continue; end;
        yy = ble_rss{tid,k};
        xx = ble_time_ax{tid,k};
        len = min([length(xx) length(yy)]);
        plot(hPlot,xx(1:len),yy(1:len),'marker',ble_device_identity{tid,2},'linewidth',2,'MarkerFaceColor',ble_device_identity{tid,1},'color',ble_device_identity{tid,1});
        
        minx(k) = min([minx(k) xx]);
        maxx(k) = max([maxx(k) xx]);
        miny(k) = min([miny(k) yy]);
        maxy(k) = max([maxy(k) yy]);
    end
    
end


% Adjust axis
for k=1:n_ble_files
    try
    hPlot = hTM.hAxis(k);
    ylim(hPlot,[miny(k)-y_margin maxy(k)+y_margin]);
    
    % Add labels
    ylabel(hPlot,y_label_str,'Interpreter','Latex');
    xlabel(hPlot,x_label_str,'Interpreter','Latex');
    xlim(hPlot,[minx(k)-x_margin maxx(k)+x_margin]);
    
    title(hPlot,['Measurement file ',num2str(k)],'Interpreter','Latex');
    
    hl=legend(hPlot,plot_legend{k});
    set(hl,'Interpreter','Latex');
    catch
        continue;
    end
end

%EOF