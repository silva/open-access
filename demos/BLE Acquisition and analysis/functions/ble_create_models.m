%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%   TAMPERE UNIVERSITY OF TECHNOLOGY
%
%   Fitting BLE RSS measurements 
%
%   This script creates anonymous
%   functions to the fitted and 
%   ideal models
%
%   Pedro Silva
%   pedro.silva@tut.fi
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

model_itur    = @(PL,d,eta)  PL-20*eta*log10(d);                   % fitted model
model_ideal   = @(d)         Ptx+model_freq_loss-20*log10(d);      % ideal model 
model_diff    = @(PL,d,eta)  itur_model(PL,d,eta)-model_ideal(d);% difference
model_logdist = @(PL,d,eta)  PL-10*eta*log10(d);                % fitted model

model_itur_d     = @(RSS,PL,eta) 10^((PL -RSS)/(20*eta));
model_logdist_d  = @(RSS,PL,eta) 10^((PL -RSS)/(10*eta));

%EOF