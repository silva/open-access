%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%   TAMPERE UNIVERSITY OF TECHNOLOGY
%
%   Plots a CDF plot for the difference in RSS in weak and strong
%   interference environments
%
%   Pedro Silva
%   pedro.silva@tut.fi
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% tag to consider
tid    = ble_master_tag;

% figure handle
hCDFd  = inout.output;
hCDFd.nfigure(1);
hPlot = hCDFd.hAxis(1);

% plot parameters (others in ble_params.m)
cidx   = [1 fix(length(cmap)/2) length(cmap)-1];
scases = [10 11 12];
pstep  = 25;

% Drawing of the CDF plots for each of the SCASES (ONLY UNOTT)
k    = 0;
cLeg = [];
for pos = 1:3
    
    scn = scases(pos);
    k=k+1;
    len = min([length(ble_rss{tid,pos})  length(ble_rss{tid,scn})]);
    cdf_diff_data = abs(ble_rss{tid,pos}(1:len) - ble_rss{tid,scn}(1:len));
    
    lims = [min(round(abs(cdf_diff_data)))-1 max(round(abs(cdf_diff_data))+5)];
    
    [ecdf,acdf] = math.cdf(cdf_diff_data(:),[],500);
    plot(hPlot,[lims(1);acdf(1:pstep:end);lims(2)],[0;ecdf(1:pstep:end);1],'linewidth',1.5,'color',cmap(cidx(k),:),'marker',mmk{k});
    
    ylabel(hPlot,'Cumulative Probability','Interpreter','Latex');
    xlabel(hPlot,['$RSS_{in\, strong\, interference}-RSS_{in\, weak\, interference}$ (dBm)'],'Interpreter','Latex');
    xlim(hPlot,[lims(1) lims(2)]);
    
    cLeg = [cLeg, {['Reader at position (',reader_pos_str{pos},')']}];
    
end

% Legend and title
title(hPlot,'RSS difference for BLE tag 3','Interpreter','Latex');
hl = legend(cLeg(:),'Location','Southeast');
set(hl,'Interpreter','Latex');
