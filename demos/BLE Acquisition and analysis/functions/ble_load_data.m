%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%   TAMPERE UNIVERSITY OF TECHNOLOGY
%
%   Fitting BLE RSS measurements 
%
%   NOTE
%   This section requires the translation of the txt files to m files
%   Please tun ble_parser before hand and point datadir to storage/<target>
%
%   Pedro Silva
%   pedro.silva@tut.fi
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%input location
figdir       = ['results/figures-',datadir];
datadir      = ['storage/',datadir];
N_max        = 12;

mkdir(figdir);

% retrieve folder contents
hIn             = Parser(datadir);
[~,n_ble_files] = hIn.getMFileList('*ble*.mat');


n_files  = n_ble_files;
minId    = Inf;
maxId    = -Inf;


ble_mean_mat = NaN(n_files,N_max); % matrix with obs mean RSS value per file
ble_var_mat  = NaN(n_files,N_max); % matrix with obs mean RSS value per file

for k=1:n_files
    load([datadir,'parsed_ble_',num2str(k),'.mat']);
    
    db        = rss_ble;
    n_devices = n_ble;
    [mac_id,mac_map,markers,idcolor]=translate_ble_mac(mac_ble);
    
    if isempty(db), continue, end;
    if db{1}{1} < 100,
        continue;
    end;
    
    zero_time = db{1}{3}(1);
    nvec      = 1:max(mac_id);
    clear cLeg
    cnt  = 0;

    disp(['FILE CASE ',num2str(k)])
    
   
    for tid=nvec
        rss = [];
        seq = nvec(tid==mac_id);
        if isempty(seq), continue; end
        cnt       = cnt+1;
        cLeg{cnt} = mac_map{seq};
        tmaxis    = db{seq}{3}-zero_time;
        rss       = db{seq}{2}(1:end);
        
        minId = min([minId tid]);
        maxId = max([maxId tid]);
        
        
        disp(['ID: ',num2str(tid),' Var: ',num2str(var(rss)),' Mean: ',num2str(mean(rss))]);
        
        ble_mean_mat(tid,k)        = nanmean(rss);
        ble_var_mat(tid,k)         = nanvar(rss);
        ble_rss{tid,k}             = rss;
        ble_id{tid,k}              = ['BLE ',num2str(tid)];
        
        ble_time_db{tid,k}         = db{seq}{3};
        ble_time_ax{tid,k}         = tmaxis;
        
        ble_device_identity{tid,1} = idcolor(seq,:);
        ble_device_identity{tid,2} = markers{seq};
        
    end
    
    ble_leg{k,:}             = cLeg(:);
    
end

assert(n_files>0);

%EOF