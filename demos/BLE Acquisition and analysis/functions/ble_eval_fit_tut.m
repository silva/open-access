%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%   TAMPERE UNIVERSITY OF TECHNOLOGY
%
%   This script evaluates the model fit for the TUT observations
%
%   Pedro Silva
%   pedro.silva@tut.fi
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

try
    clear de_emo
end

mval  = ble_mean_mat(ble_master_tag,:);  
mval(isnan(mval)) = []; %trim the fat
coord = tag_coordinates_TUT;


% get indexes
N     = size(coord(:,1),1);
mnum  = 1:length(model_xx);
for k=1:N
    idx(k)        = mnum(model_xx == coord(k,1)); 
end

% compute difference
for n=1:length(model_yy)
    for k=1:N
        d_emo(k,1)   = coord(k);
        try
        y_pred       = model_yy{n}(idx(k));
        catch
            continue;
        end
        y_obs        = mval(k); 
        d_emo(k,n+1) = sqrt(mean((y_obs-y_pred).^2));
    end
end


d_emo(1,:) = [];
d_emo(1,:) = [];
k=N(1)-1;
d_emo(k,:) = mean(d_emo,1);


if use_model_itu
    disp(['    xx | itu_r | itu_r (previous) for case ',num2str(rcase)])
    disp(d_emo)
else
    disp(['    xx | log_dist | log_dist (previous) | itur (off) | itur (on) for case ',num2str(rcase)])
    disp(d_emo)
end
