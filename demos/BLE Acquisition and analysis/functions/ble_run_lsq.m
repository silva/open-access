
if use_model_itu
    C = [ones(Nmeas,1), (Ptx+model_freq_loss-20*log10(mdist))];
%     x = lsqlin(C,d(:),eye(2),[0 1],eye(2),[-15 1],lbe,ube);
    x = lsqlin(C,d(:),[],[],[],[],lbe,ube);
    try
        L(tid,col_idx)   = x(1);
        eta(tid,col_idx) = x(2);
    catch
        L   = x(1);
        eta = x(2);
    end
    
    % Pathloss experienced in the channel
    assert(Ptx <0 & all(all(L <=0)) & model_freq_loss <0)
    PL0   = Ptx+L+model_freq_loss;
    
else
    C   = (PLd0-10*log10(mdist));
    x   = lsqlin(C,d(:),[],[],[],[],lbe(2),ube(2));
    L   = 0;
    try
        L(tid,col_idx)   = 0;
        eta(tid,col_idx) = x(1);
    catch
        L   = 0;
        eta = x(1);
    end
    
    
    % Pathloss experienced in the channel
    assert(PLd0 <0)
    PL0   = PLd0;
end






