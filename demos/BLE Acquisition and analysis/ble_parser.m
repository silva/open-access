
clear variables
dataset     = 'Dynamic';
results_dir = ['storage/results',dataset];
mkdir(results_dir);

% Path options
logdir =  ['\\intra.tut.fi\home\figueire\My Documents\MATLAB\projects\ble_scan_nott\storage\log',dataset,'\'];
inObj  = Parser(logdir);
inObj.getMFileList('*.dump');
inObj.list;

for fidx=2:length(inObj.listing)
    disp(fidx);
    inObj.openfidx(fidx);
    cnt=0;
    while 1
        cnt = cnt+1;
        toparse = inObj.getline();
        if ~isempty(strfind(upper(toparse),'EXIT')), 
            break; 
        end;
        inObj.parse(toparse);
    end
    disp(['Parsed lines : ',num2str(cnt)]);
    inObj.close();
    
    [rss_ble,mac_ble,n_ble] = inObj.getRSS('BLE');
    [rss_wfi,mac_wfi,n_wfi] = inObj.getRSS('WFI');
    
    save(['./',results_dir,'/parsed_ble_',num2str(fidx),'.mat'],'rss_ble','mac_ble','n_ble');
    save(['./',results_dir,'/parsed_wfi_',num2str(fidx),'.mat'],'rss_wfi','mac_wfi','n_wfi');
    
    %clears object
    inObj.clear()
    
    disp('done');
end
