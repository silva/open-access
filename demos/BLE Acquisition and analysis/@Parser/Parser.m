classdef Parser < inout.input
    %PARSER Summary of this class goes here
    %   Detailed explanation goes here
    
    
    properties
        nb_mac_ble
        mac_ble
        rss_ble
        
        nb_mac_wfi
        mac_wfi
        rss_wfi
    end
    
    
    methods(Static)
        function num=translate(type)
            switch upper(type)
                case 'WFI'
                    num=0;
                case 'BLE'
                    num=1;
                otherwise
                    num = NaN;
            end
            
        end
    end
    
    methods
        function obj=Parser(inDir)
            obj=obj@inout.input(inDir);
            obj.nb_mac_ble = 0;
            obj.nb_mac_wfi = 0;
            rss_ble{1}(1) = 0;
            rss_wfi{1}(1) = 0;
        end
        
        
        function parse(obj,content)
            fields = obj.getStrFields(content);
            id     = Parser.translate(fields{1}); %header
            tm     = str2num(fields{2})*60*60 ... %time entry
                +str2num(fields{3})*60+str2num(fields{4})+str2num(fields{5})*1e-09;
            
            switch id
                
                case 0 %WIF WFI,18:33:27:178956437,54:78:1A:88:C0:A3,11,-91
                    mac = [fields{6},':',fields{7},':',fields{8},':',fields{9},':',fields{10},':',fields{11}];
                    chn = str2num(fields{12});
                    rss = str2num(fields{13});
                    
                    dId = obj.append_mac_wfi(mac);
                    obj.append_rss_wfi(dId,rss,chn,tm);
                    
                case 1 %BLE BLE,18:33:27:061538421,F7826DA6-4FA2-4E98-8024-BC5B71E0893E,54321,45350,-77,-86
                    mac    = [fields{6},'-',fields{7},'-',fields{8}];
                    %                     major  = fields{7};
                    %                     minor  = fields{8};
                    chn  = NaN;
                    rss  = str2num(fields{end});
                    dId  = obj.append_mac_ble(mac);
                    obj.append_rss_ble(dId,rss,chn,tm);
            end
            
            
            
            
        end
        
        function idx = append_mac_ble(obj,mac)
            
            % searches for an hit
            idx=[];
            for k=1:obj.nb_mac_ble
                if strcmp(obj.mac_ble{k},mac)
                    idx = k;
                    break
                end
            end
            
            % if not found insert and returns dId
            if isempty(idx)
                obj.nb_mac_ble   = obj.nb_mac_ble+1;
                idx              = obj.nb_mac_ble;
                obj.mac_ble{idx} = mac;
            end
            
        end
        
        function idx = append_mac_wfi(obj,mac)
            
            % searches for an hit
            idx=[];
            for k=1:obj.nb_mac_wfi
                if strcmp(obj.mac_wfi{k},mac)
                    idx = k;
                    break
                end
            end
            
            % if not found insert and returns dId
            if isempty(idx)
                obj.nb_mac_wfi   = obj.nb_mac_wfi+1;
                idx              = obj.nb_mac_wfi;
                obj.mac_wfi{idx} = mac;
            end
            
        end
        
        
        function append_rss_ble(obj,idx,rss,ch,tm)
            try
                cnt = obj.rss_ble{idx}{1};
                cnt = cnt(1);
            catch
                cnt = 0;
            end
            cnt = cnt+1;
            try
                vec  = obj.rss_ble{idx}{2};
                tvec = obj.rss_ble{idx}{3};
            catch
                vec  = [];
                tvec = [];
            end
            vec  = [vec,rss];
            tvec = [tvec,tm];
            obj.rss_ble{idx}{3} = tvec;
            obj.rss_ble{idx}{2} = vec;
            obj.rss_ble{idx}{1} = cnt;
        end
        
        function append_rss_wfi(obj,idx,rss,ch,tm)
            try
                cnt = obj.rss_wfi{idx}{1};
                cnt = cnt(1);
            catch
                cnt = 0;
            end
            cnt = cnt+1;
            try
                vec  = obj.rss_wfi{idx}{2};
                tvec = obj.rss_wfi{idx}{3};
            catch
                vec  = [];
                tvec = [];
            end
            
            vec  = [vec,rss];
            tvec = [tvec,tm];
            obj.rss_wfi{idx}{3} = tvec;
            obj.rss_wfi{idx}{2} = vec;
            obj.rss_wfi{idx}{1} = cnt;
        end
        
       
        function [rss,mac,nb] = getRSS(obj,type)
            switch upper(type)
                
                case 'WFI' %WIF WFI,18:33:27:178956437,54:78:1A:88:C0:A3,11,-91
                    rss = obj.rss_wfi;
                    mac = obj.mac_wfi;
                    nb  = obj.nb_mac_wfi;
                case 'BLE' %BLE BLE,18:33:27:061538421,F7826DA6-4FA2-4E98-8024-BC5B71E0893E,54321,45350,-77,-86
                    rss = obj.rss_ble;
                    mac = obj.mac_ble;
                    nb  = obj.nb_mac_ble;
            end
        end
        
        function clear(obj)
            obj.rss_ble    = {};
            obj.mac_ble    = {};
            obj.nb_mac_ble = 0;
            obj.rss_wfi    = {};
            obj.mac_wfi    = {};
            obj.nb_mac_wfi = 0;
        end
        
    end
    
end

