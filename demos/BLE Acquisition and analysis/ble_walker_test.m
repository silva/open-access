%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%   TAMPERE UNIVERSITY OF TECHNOLOGY
%
%   This script has a simple ranging and positioning appliction for BLE
%   signals
%
%   Pedro Silva
%   pedro.silva@tut.fi
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clear variables
close all
addpath('./functions/');

%% Some necessary calls (do not change)
ble_retrieve_PLd0; % (.m) retrieves PL at d0 (1 m) for given tags

% colormap scheme (must be here, ble_retrieve clear everything)
global cmap;
h=figure;
cmap = jet;
close(h);

%% Cleanup [IMPORTANT!]
clearvars -except tag_PLd0 cmap

%% INPUTS
ble_master_tag  = 3;      % (#)   BLE tag number

use_model_itu   = 0;      % (b) changes model fit
pl_manufacturer = 0;      % (b) 1 - Manufacter value for PLd0, otherwise the measured one is used
overlay_other   = 1;      % (b) overlays fitted model parameters

print_figures   = 0;      % (b) print out figures
fig_out_type    = '-png'; % ([s]) figure format (supported by export_fig/matlab)
hfigdir         = 'pub/'; % ([s]) output for figures


% Parameters estimated beforehand
fitted_PL0_off  = -79.64;
fitted_eta_off  = 1.0410;

fitted_PL0_on   = -76.81;
fitted_eta_on   = 1.083;

corridor_PL0_w  = -71.34;
corridor_eta_w  = 1;

logdist_eta     = 0.99;  % from logdist model

% figure handle for subplot
hBox  = inout.output;
hBox.figure(1);


%% Retrieving the data
datadir = 'resultsdynamic/';   % ([s]) matfile location
ble_params;                         % (.m)  constants, coordinates and others
ble_load_data;                      % (.m)  reads m files to workspace
ble_create_models;      % (.m) build anon fct (requires PL0 from fit_model)

%%
rss = ble_rss{3,1};
wind = 100/2;
idx  = 0;
for t=15000:25000
    idx=idx+1;
    d(idx,1) = model_itur_d(mean(rss(t-wind:t)),-24.76-40.0460-12,1.08);
    d(idx,2) = model_logdist_d(mean(rss(t-wind:t)),-77,1.0182);
    d(idx,3) = model_logdist_d(mean(rss(t-wind:t)),-79.73,0.9820);
end

hRanges = inout.output;
hRanges.figure;
subplot(hRanges.hAxis);
hPlot    = subplot(2,1,1);
hold(hPlot,'on');
box(hPlot,'off');
grid(hPlot,'on');

taxis = 0.04:0.04:0.04.*length(d(:,1));
plot(hPlot,taxis,d(:,1),'b')
plot(hPlot,taxis,d(:,2),'g')
plot(hPlot,taxis,d(:,3),'c')
xlim(hPlot,[0,taxis(end)]);

rms_itu=sqrt(mean((d(:,1)-1)).^2);
rms_ldr=sqrt(mean((d(:,2)-1)).^2);
rms_lde=sqrt(mean((d(:,3)-1)).^2);


hl=legend(hPlot,['ITU-R: $\mathrm{RMSE_{ITU-R}} = ',num2str(rms_itu,'%0.2f'),'\,\mathrm{m}$'],...
    ['log dist (reported): $\mathrm{RMSE_{log dist (reported)}} = ',num2str(rms_ldr,'%0.2f'),'\,\mathrm{m}$'],...
    ['log dist (estimated): $\mathrm{RMSE_{log dist (estimated)}} = ',num2str(rms_lde,'%0.2f'),'\,\mathrm{m}$']);
set(hl,'interpreter','latex','location','northeast');
xlabel(hPlot,'Time (s)','interpreter','latex');
ylabel(hPlot,'Distance (m)','interpreter','latex');
title('\textbf{1 tag broadcasting}','interpreter','latex');
ylim([0 4]);

rss = ble_rss{3,2};
wind = 100/2;
idx  = 0;
for t=15000:25000
    idx=idx+1;
    d(idx,1) = model_itur_d(mean(rss(t-wind:t)),-24.76-40.0460-12,1.08);
    d(idx,2) = model_logdist_d(mean(rss(t-wind:t)),-77,1.0182);
    d(idx,3) = model_logdist_d(mean(rss(t-wind:t)),-79.73,0.9820);
end

hPlot    = subplot(2,1,2);
hold(hPlot,'on');
box(hPlot,'off');
grid(hPlot,'on');

plot(hPlot,taxis,d(:,1),'b')
plot(hPlot,taxis,d(:,2),'g')
plot(hPlot,taxis,d(:,3),'c')
xlim(hPlot,[0,taxis(end)]);

rms_itu=sqrt(mean((d(:,1)-1)).^2);
rms_ldr=sqrt(mean((d(:,2)-1)).^2);
rms_lde=sqrt(mean((d(:,3)-1)).^2);


hl=legend(hPlot,['ITU-R: $\mathrm{RMSE_{ITU-R}} = ',num2str(rms_itu,'%0.2f'),'\,\mathrm{m}$'],...
    ['log dist (reported): $\mathrm{RMSE_{log dist (reported)}} = ',num2str(rms_ldr,'%0.2f'),'\,\mathrm{m}$'],...
    ['log dist (estimated): $\mathrm{RMSE_{log dist (estimated)}} = ',num2str(rms_lde,'%0.2f'),'\,\mathrm{m}$']);
set(hl,'interpreter','latex','location','northeast');
xlabel(hPlot,'Time (s)','interpreter','latex');
ylabel(hPlot,'Distance (m)','interpreter','latex');
title('\textbf{8 tag broadcasting}','interpreter','latex');

hRanges.hAxis = hPlot;
ylim([0 10]);
clear d
% title(hPlot,'2 seconds average','interpreter','latex');

%% Positioning
hPos = inout.output;
hPos.figure;
hPlot = hPos.hAxis;

for mdidx = 1:3
    for cidx = 3:5
        for tid=1:8
            rss = ble_rss{tid,cidx};
            idx=0;
            wind = 20;
            for t=200:1000
                idx=idx+1;
                iturd(idx,tid) = model_itur_d(mean(rss(t-wind:t)),-15.96-40.0460-12,1.09);
                logdR(idx,tid) = model_logdist_d(mean(rss(t-wind:t)),-77,0.99);
                logdE(idx,tid) = model_logdist_d(mean(rss(t-wind:t)),-79.73,0.94);
            end
        end
        
        estimate_in = [0,0,0];
        for t=1:size(iturd,1)
            
            
%             estimate_in(3) = 0.5;
            
            
            %         [ epos, err, V, ign] = lsq( estimate_in, reader_pos(1,:), tag_coordinates_UNOTT, logdR(t,:));
            
            if mdidx == 1
                li=iturd(t,:);
            elseif mdidx == 2
                li=logdR(t,:);
            else
                li=logdE(t,:);
            end
            
            if cidx==3
                xr = reader_pos(1,:);
            elseif cidx==4
                xr = reader_pos(2,:);
            else
                xr = reader_pos(3,:);
            end
            
            
            [ epos, err, V, ign] = nlsq( estimate_in, xr, tag_coordinates_UNOTT, li);
            estimate_in = epos;
            if cidx==3
                store_epos_1(t,:) = epos;
                store_err_1(t,mdidx)    = err;
            elseif cidx == 4
                store_epos_2(t,:) = epos;
                store_err_2(t,mdidx)    = err;
            else
                store_epos_3(t,:) = epos;
                store_err_3(t,mdidx)    = err;
            end
        end
    end
        
%     store_epos = [store_epos_1;store_epos_2;store_epos_3];
    
    if mdidx == 1
        mm = 'o';
    elseif mdidx == 2
        mm = '^';
    else
        mm = 'd';
    end
    
%     plot3(hPlot,store_epos_1(:,1),store_epos_1(:,2),store_epos_1(:,3),['k',mm],'markerfacecolor','b','marker',mm,'linewidth',0.4)
%     plot3(hPlot,store_epos_2(:,1),store_epos_2(:,2),store_epos_2(:,3),['k',mm],'markerfacecolor','g','marker',mm,'linewidth',0.4)
%     plot3(hPlot,store_epos_3(:,1),store_epos_3(:,2),store_epos_3(:,3),['k',mm],'markerfacecolor','c','marker',mm,'linewidth',0.4)
%     
    clear store_epos_1;
    clear store_epos_2;
    clear store_epos_3;
    
%     clear store_err_1;
%     clear store_err_2;
%     clear store_err_3;
end

hl=legend('ITU-R (A)','ITU-R (B)','ITU-R (C)',...
       '$\mathrm{Log dist_{rep.}}$ (A)',...
       '$\mathrm{Log dist_{rep.}}$ (B)',...
       '$\mathrm{Log dist_{rep.}}$ (C)',...
       '$\mathrm{Log dist_{est.}}$ (A)',...
       '$\mathrm{Log dist_{est.}}$ (B)',...
       '$\mathrm{Log dist_{est.}}$ (C)');
set(hl,'interpreter','latex');
ridx = 1;
plot3(hPlot,reader_pos(ridx,1),reader_pos(ridx,2),reader_pos(ridx,3),'y','markerfacecolor','y','marker','o','markersize',15)
text(reader_pos(ridx,1)-0.15,reader_pos(ridx,2)-0.01,reader_pos(ridx,3),'A');
ridx = 2;
plot3(hPlot,reader_pos(ridx,1),reader_pos(ridx,2),reader_pos(ridx,3),'y','markerfacecolor','y','marker','o','markersize',15)
text(reader_pos(ridx,1)-0.15,reader_pos(ridx,2)-0.01,reader_pos(ridx,3),'B');
ridx = 3;
plot3(hPlot,reader_pos(ridx,1),reader_pos(ridx,2),reader_pos(ridx,3),'y','markerfacecolor','y','marker','o','markersize',15)
text(reader_pos(ridx,1)-0.15,reader_pos(ridx,2)-0.01,reader_pos(ridx,3),'C');
view(-39,24)


%% %% Map
%%% Corridor
plot3(hPlot,linspace(-0.5,10,20),ones(20,1).*corr_width,ones(20,1).*0,'k','linewidth',2)
plot3(hPlot,linspace(-0.5,10,20),zeros(20,1),ones(20,1).*0,'k','linewidth',2)
plot3(hPlot,ones(10,1)*6,linspace(0,corr_width,10),ones(10,1).*0,'k','linewidth',2)

%Northern wall
plot3(hPlot,-0.5*ones(20,1).*linspace(1,1.6,20)',linspace(corr_width,5,20),ones(20,1).*0,'k','linewidth',2)
plot3(hPlot,[(-0.5*ones(20,1).*linspace(0.6,1,20)');-0.5],linspace(-5,0,21),ones(21,1).*0,'k','linewidth',2)

%opposite room
plot3(hPlot,-2.5*ones(20,1),linspace(-5,5,20),ones(20,1).*0,'k','linewidth',2)
ylim(hPlot,[-2 5]);

xlabel('x (m)','interpreter','latex');
ylabel('y (m)','interpreter','latex');
zlabel('z (m)','interpreter','latex');

vert = [-4 -2;10 -2;10 5;-4 5];
fac = [1 2 3 4]; % vertices to connect to make square
patch('Faces',fac,'Vertices',vert,'FaceColor',[238 233 233]/255,'facealpha',0.2)
%%

hRanges.print('./pub/TUT_ranges','-png');
hPos.print('./pub/UNOTT_pos','-png');