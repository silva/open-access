%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%   TAMPERE UNIVERSITY OF TECHNOLOGY
%
%   This script is an office experiment equivalent to the UNOTT, but the
%   tags where left in the office
%
%   NOTE
%   This section requires the translation of the txt files to m files
%   
%   Please tun ble_parser before hand and point datadir to storage/<target>
%
%   Pedro Silva
%   pedro.silva@tut.fi
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clear variables
close all
addpath('./functions/');

%% Some necessary calls (do not change)
ble_retrieve_PLd0; % (.m) retrieves PL at d0 (1 m) for given tags

% colormap scheme (must be here, ble_retrieve clear everything)
global cmap;
h=figure;
cmap = jet;
close(h);

%% Cleanup [IMPORTANT!]
clearvars -except tag_PLd0 cmap

%% INPUTS
ble_master_tag  = 3;      % (#)   BLE tag number

use_model_itu   = 0;      % (b) changes model fit
pl_manufacturer = 0;      % (b) 1 - Manufacter value for PLd0, otherwise the measured one is used
overlay_other   = 1;      % (b) overlays fitted model parameters

print_figures   = 0;      % (b) print out figures
fig_out_type    = '-png'; % ([s]) figure format (supported by export_fig/matlab)
hfigdir         = 'pub/'; % ([s]) output for figures


% Parameters estimated beforehand
fitted_PL0_off  = -79.64;
fitted_eta_off  = 1.0410;

fitted_PL0_on   = -76.81;
fitted_eta_on   = 1.083;

corridor_PL0_w  = -71.34;
corridor_eta_w  = 1;

logdist_eta     = 0.99;  % from logdist model

% figure handle for subplot
hBox  = inout.output;
hBox.figure(1);


%% Retrieving the data
datadir = 'resultsTUT_multiple_8/'; % ([s]) matfile location
ble_params;                         % (.m)  constants, coordinates and others
ble_load_data;                      % (.m)  reads m files to workspace

% prepare handle for subplot
subplot(hBox.hAxis);
hPlot = subplot(2,1,1);
hold(hPlot,'on');
ble_plot_box_multiple;              % (.m)  plot measurement data
title(['8 tags broadcasting,  $\mu_{tag 3}$ = ',num2str(ble_mean_mat(3,1),'%2.2f'),' $\sigma^2_{tag 3}$ = ',num2str(var(ble_rss{3,1}),'%2.2f')],'interpreter','latex');

% re-fetch data
clearvars -except tag_PLd0 cmap hBox  ble_master_tag hfigdir use_model_itu  overlay_other fig_out_type pl_manufacturer print_figures 

datadir = 'resultsTUT_multiple_3/'; % ([s]) matfile location
ble_params;                         % (.m)  constants, coordinates and others
ble_load_data;                      % (.m)  reads m files to workspace

% switch plot
hPlot    = subplot(2,1,2);
ble_plot_box_multiple;              % (.m)  plot measurement data
title(['3 tags broadcasting,  $\mu_{tag 3}$ = ',num2str(ble_mean_mat(3,1),'%2.2f'),' $\sigma^2_{tag 3}$ = ',num2str(var(ble_rss{3,1}),'%2.2f') ],'interpreter','latex');


% For printing purposes
hBox.hAxis = hPlot;

if print_figures == 1
    mkdir(hfigdir);
    hBox.print(['pub/TUT_multi_boxplot'],fig_out_type);
end

%EOF