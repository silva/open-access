clear variables
close all

hFig = inout.output;
hFig.figure;

for k=1:20
    m = 3; % frequencies
    s = k; % number of tags 

    Prb(k) = 1-((m-1)/m)^(s-1);
end
plot(hFig.hAxis,1:20,Prb*100,'^-','Linewidth',2);
xlabel('Number of BLE tags broadcasting');
ylabel('Probability of picking an occupied channel (%)');
title('3 channels available');
hFig.print('./pub/prob','-png');
ylim([0 100]);
xlim([1 20]);

disp(['Probability of picking an ocuppied channel: ',num2str(round(Prb(k-1)*100),'%d'),'%'])

Pt =-12;
B = 2e6;
% Intersystem interference model for frequency hopping systems
P = [ 1/m 1/m 1/m;
      1/m 1/m 1/m;
      1/m 1/m 1/m];
Ni = Pt - log10(B); 
N  = [ Ni 0 0;
       Ni 0 0;
       Ni 0 0];


