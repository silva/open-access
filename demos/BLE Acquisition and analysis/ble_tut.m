%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%   TAMPERE UNIVERSITY OF TECHNOLOGY
%
%   Fitting BLE RSS measurements
%
%   NOTE
%   This section requires the translation of the txt files to m files
%
%   Please tun ble_parser before hand and point datadir to storage/<target>
%
%   Pedro Silva
%   pedro.silva@tut.fi
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
clc
clear variables
close all
addpath('./functions/');

%% Some necessary calls (do not change)
ble_retrieve_PLd0; % (.m) retrieves PL at d0 (1 m) for given tags

% colormap scheme (must be here, ble_retrieve clear everything)
global cmap;
h=figure;
cmap = jet;
close(h);

%% Inputs
ble_master_tag  = 3;      % (#) BLE tag to analyse

use_model_itu   = 1;      % (b) changes model fit
pl_manufacturer = 0;      % (b) 1 - Manufacter value for PLd0, otherwise the measured one is used
overlay_other   = 1;      % (b) overlays fitted model parameters

print_figures   = 1;      % (b) print out figures
fig_out_type    = '-png'; % ([s]) figure format (supported by export_fig/matlab)
hfigdir         = 'pub/'; % ([s]) output for figures

%%% model values acquired for TUT acquisition in JULY 2015
fitted_PL0_off  = -79.64;
fitted_eta_off  = 1.0206;

fitted_PL0_on   = -76.81;
fitted_eta_on   = 1.083;

eta_on_manuf    = 1.0182;
eta_off_manuf   = 1.0322;

fit_itu_PL_on   = -76.81;
fit_itu_eta_on  = 1.083;

fit_itu_PL_off  = -79.64;
fit_itu_eta_off = 1.0206;

for rcase   = 1:2;
    
    % switches between the two directories
    switch rcase
        case 1
            datadir         = 'resultsTUT_ON/';  % ([s]) results directory
            wifi_state      = 'on';              % ([s]) wi-fi state for acquisiton
            wifi_nstate     = 'off';             % ([s]) opposite of wi-fi state
            cstate          = 'g';
            ncstate         = 'b';
            cmarker         = 's';
            ncmarker        = 'd';
            
        case 2
            datadir         = 'resultsTUT_OFF/'; % ([s]) results directory
            wifi_state      = 'off';             % ([s]) wi-fi state for acquisiton
            wifi_nstate     = 'on';              % ([s]) opposite of wi-fi state
            cstate          = 'b';
            ncstate         = 'g';
            cmarker         = 'o';
            ncmarker        = 'd';
    end
    
    % Creates figure handle
    hBox  = inout.output;
    hBox.figure(1);
    hPlot = hBox.hAxis;
    box(hPlot,'off');
    
    
    %% Retrieving the data
    ble_params;             % (.m) constants, coordinates and others
    ble_load_data;          % (.m) reads m files to workspace
    
    %% Processing the data
    ble_fit_model_tut;      % (.m) fits model to data
    ble_create_models;      % (.m) build anon fct (requires PL0 from fit_model)
    
    
    %% values for overlaying other model over boxplots
    switch rcase
        case 1
            if pl_manufacturer == 1
                fitted_PL0 = PLd0;
                fitted_eta = eta_off_manuf;
            else
                fitted_PL0 = fitted_PL0_off;
                fitted_eta = fitted_eta_off;
            end
            
        case 2
            if pl_manufacturer == 1
                fitted_PL0 = PLd0;
                fitted_eta = eta_on_manuf;
            else
                fitted_PL0 = fitted_PL0_on;
                fitted_eta = fitted_eta_on;
            end
    end
    
    %% Plotting
    ble_plot_overlay_model; % (.m) overlays model (assumes handle in hPlot)
    ble_plot_box_tut;       % (.m) plots box plots with distance
    ble_eval_fit_tut;
    %     ble_plot_time_series;   % (.m) plots the time series for each measurement file
    
    %% Storing and tweaking plots
%     if use_model_itu
%         title(hPlot,['Wi-Fi interface ',wifi_state,', $C=',num2str(L,'%2.2f'),'$, $\eta =',num2str(eta,'%2.2f'),'$'],'Interpreter','Latex');
%     else
%         title(hPlot,['Wi-Fi interface ',wifi_state,', $P_r(d_0)=',num2str(PLd0,'%2.2f'),'$, $\eta =',num2str(eta,'%2.2f'),'$'],'Interpreter','Latex');
%     end
    ylabel(hPlot,'RSS (dBm)','Interpreter','Latex');
    xlabel(hPlot,'Distance (m)','Interpreter','Latex');
    if overlay_other
        if use_model_itu
            hl=legend(hPlot,['Model fit Wi-Fi ',wifi_state],['Model fit Wi-Fi ',wifi_nstate],'Mean RSS','Median','location','best');
        else
            hl=legend(hPlot,['Log dist fit ($P_r(d_0)$ estimated)'],['Log dist fit ($P_r(d_0)$ reported)'],['ITU-R fit'],'Mean RSS','Median','location','northeast');
        end
    else
        hl=legend(hPlot,['Model fit Wi-Fi ',wifi_state],'Mean RSS','Median','location','best');
    end
    set(hl,'Interpreter','Latex');
    
    % save for box_plot comparison
    store_rss{rcase} = ble_rss;
    store_hdl{rcase} = hBox;
    try
        store_tdl{rcase} = hTM;
    end
    store_L(rcase)   = L;
    store_PLO(rcase) = PL0;
    store_eta(rcase) = eta;
end

% Single
hDist  = inout.output;
hDist.figure(1);
ble_plot_box_comparison_tut; % (.m) plots boxplot vs distance


%% PRINTING
if print_figures == 1
    mkdir(hfigdir);
    
    for k=1:2
        hBox = store_hdl{k};
        hBox.print([hfigdir,'TUT_box_',num2str(k)],fig_out_type);
        try
            hTM = store_tdl{k};
            hTM.print([hfigdir,'TUT_box_',num2str(k)],fig_out_type);
        end
    end
    
    hDist.print(['pub/TUT_box_compare'],fig_out_type);
end
%EOF