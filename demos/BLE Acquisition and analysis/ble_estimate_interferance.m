%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%   TAMPERE UNIVERSITY OF TECHNOLOGY
%
%   This script is an office experiment equivalent to the UNOTT, but the
%   tags where left in the office
%
%   NOTE
%   This section requires the translation of the txt files to m files
%   
%   Please tun ble_parser before hand and point datadir to storage/<target>
%
%   Pedro Silva
%   pedro.silva@tut.fi
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clear variables
close all
addpath('./functions/');

%% Some necessary calls (do not change)
ble_retrieve_PLd0; % (.m) retrieves PL at d0 (1 m) for given tags

% colormap scheme (must be here, ble_retrieve clear everything)
global cmap;
h=figure;
cmap = jet;
close(h);

%% Cleanup [IMPORTANT!]
clearvars -except tag_PLd0 cmap

%% INPUTS
ble_master_tag  = 3;      % (#)   BLE tag number

use_model_itu   = 0;      % (b) changes model fit
pl_manufacturer = 0;      % (b) 1 - Manufacter value for PLd0, otherwise the measured one is used
overlay_other   = 1;      % (b) overlays fitted model parameters

print_figures   = 0;      % (b) print out figures
fig_out_type    = '-png'; % ([s]) figure format (supported by export_fig/matlab)
hfigdir         = 'pub/'; % ([s]) output for figures


% Parameters estimated beforehand
fitted_PL0_off  = -79.64;
fitted_eta_off  = 1.0410;

fitted_PL0_on   = -76.81;
fitted_eta_on   = 1.083;

corridor_PL0_w  = -71.34;
corridor_eta_w  = 1;

logdist_eta     = 0.99;  % from logdist model

% figure handle for subplot
hBox  = inout.output;
hBox.figure(1);


%% Retrieving the data
datadir = 'resultsTUT_single_3/';   % ([s]) matfile location
ble_params;                         % (.m)  constants, coordinates and others
ble_load_data;                      % (.m)  reads m files to workspace

% idx = 160000; %is tag 2 dead?
idx1 = 8000; %is tag 2 dead?
tid  = 3;
b1tag=ble_rss{3,1};
b2tag=ble_rss{3,2};
b3tag=ble_rss{3,3};
b4tag=ble_rss{3,4};
b5tag=ble_rss{3,5};
b6tag=ble_rss{3,6};
b7tag=ble_rss{3,7};
b8tag=ble_rss{3,8};
inter_comp = mean(b2tag(1:idx1)-b1tag(1:idx1));

hPlot = hBox.hAxis(1);
% disp(['Interference component: ',num2str(inter_comp,'%2.2f'),' dB']);

idx1 = 22e3;
idx2 = 8000;
m1=mean(b1tag(1:idx1));
m2=mean(b2tag(1:idx1));
m3=mean(b3tag(1:idx1));
m4=mean(b4tag(1:idx2));
m5=mean(b5tag(1:idx2));
m6=mean(b6tag(1:idx1));
m7=mean(b7tag(1:idx1));
m8=mean(b8tag(1:idx1));

var(b1tag(1:idx1))
var(b2tag(1:idx1))
var(b3tag(1:idx1))
var(b4tag(1:idx2))
var(b5tag(1:idx2))
var(b6tag(1:idx1))
var(b7tag(1:idx1))
var(b8tag(1:idx1))

med_marg=0.2;
for k=1:8
    if k == 1
        plot(hPlot,k,nanmean(b1tag(1:idx1)),'xk','Markersize',8,'Linewidth',2); % mean
        plot(hPlot,[(k-med_marg):0.1:(k+med_marg)],([(k-med_marg):0.1:(k+med_marg)].*0+1)*nanmedian(b1tag(1:idx1)),'-k','Markersize',8,'Linewidth',3); % mean
        boxplot(hPlot,b1tag(1:idx1),'symbol','k.','medianstyle','line','position',k);%,'labelorientation','horizontal');
        
    elseif k == 2
        plot(hPlot,k,nanmean(b2tag(1:idx1)),'xk','Markersize',8,'Linewidth',2); % mean
        plot(hPlot,[(k-med_marg):0.1:(k+med_marg)],([(k-med_marg):0.1:(k+med_marg)].*0+1)*nanmedian(b2tag(1:idx1)),'-k','Markersize',8,'Linewidth',3); % mean
        boxplot(hPlot,b2tag(1:idx1),'symbol','k.','medianstyle','line','position',k);%,'labelorientation','horizontal');
        
    elseif k == 3
        plot(hPlot,k,nanmean(b3tag(1:idx1)),'xk','Markersize',8,'Linewidth',2); % mean
        plot(hPlot,[(k-med_marg):0.1:(k+med_marg)],([(k-med_marg):0.1:(k+med_marg)].*0+1)*nanmedian(b3tag(1:idx1)),'-k','Markersize',8,'Linewidth',3); % mean
        boxplot(hPlot,b3tag(1:idx1),'symbol','k.','medianstyle','line','position',k);%,'labelorientation','horizontal');
        
    elseif k == 4
        plot(hPlot,k,nanmean(b4tag(1:idx2)),'xk','Markersize',8,'Linewidth',2); % mean
        plot(hPlot,[(k-med_marg):0.1:(k+med_marg)],([(k-med_marg):0.1:(k+med_marg)].*0+1)*nanmedian(b4tag(1:idx2)),'-k','Markersize',8,'Linewidth',3); % mean
        boxplot(hPlot,b4tag(1:idx2),'symbol','k.','medianstyle','line','position',k);%,'labelorientation','horizontal');
        
    elseif k == 5
        plot(hPlot,k,nanmean(b5tag(1:idx2)),'xk','Markersize',8,'Linewidth',2); % mean
        plot(hPlot,[(k-med_marg):0.1:(k+med_marg)],([(k-med_marg):0.1:(k+med_marg)].*0+1)*nanmedian(b5tag(1:idx2)),'-k','Markersize',8,'Linewidth',3); % mean
        boxplot(hPlot,b5tag(1:idx2),'symbol','k.','medianstyle','line','position',k);%,'labelorientation','horizontal');
    
    elseif k == 6
        plot(hPlot,k,nanmean(b6tag(1:idx1)),'xk','Markersize',8,'Linewidth',2); % mean
        plot(hPlot,[(k-med_marg):0.1:(k+med_marg)],([(k-med_marg):0.1:(k+med_marg)].*0+1)*nanmedian(b6tag(1:idx1)),'-k','Markersize',8,'Linewidth',3); % mean
        boxplot(hPlot,b6tag(1:idx1),'symbol','k.','medianstyle','line','position',k);%,'labelorientation','horizontal');
    
    elseif k == 7
        plot(hPlot,k,nanmean(b7tag(1:idx1)),'xk','Markersize',8,'Linewidth',2); % mean
        plot(hPlot,[(k-med_marg):0.1:(k+med_marg)],([(k-med_marg):0.1:(k+med_marg)].*0+1)*nanmedian(b7tag(1:idx1)),'-k','Markersize',8,'Linewidth',3); % mean
        boxplot(hPlot,b7tag(1:idx1),'symbol','k.','medianstyle','line','position',k);%,'labelorientation','horizontal');
    
    elseif k == 8
        plot(hPlot,k,nanmean(b8tag(1:idx1)),'xk','Markersize',8,'Linewidth',2); % mean
        plot(hPlot,[(k-med_marg):0.1:(k+med_marg)],([(k-med_marg):0.1:(k+med_marg)].*0+1)*nanmedian(b8tag(1:idx1)),'-k','Markersize',8,'Linewidth',3); % mean
        boxplot(hPlot,b8tag(1:idx1),'symbol','k.','medianstyle','line','position',k);%,'labelorientation','horizontal');
    end
    h = findobj(hPlot,'Tag','Box');
    
    
%     tid = 3;
    cnt = 1;
    patch(get(h(cnt),'XData'),get(h(cnt),'YData'),ble_device_identity{tid,1},'FaceAlpha',.5,'Parent', hPlot);
%     plot(hPlot,[(tid-0.3):0.1:(tid+0.3)],([(tid-0.3):0.1:(tid+0.3)].*0+1)*nanmedian(rss_cell(:,cnt)),'-k','Markersize',8,'Linewidth',2); % mean

end
xlim(hPlot,[0 9]);
ylim(hPlot,[-95 -65]);
set(hPlot,'xtickmode','auto')
set(hPlot,'xticklabelmode','auto')
ylabel(hPlot,'RSS (dBm)','Interpreter','Latex');
xlabel(hPlot,'Number of tags broadcasting','Interpreter','Latex');
hl = legend('RSS mean','RSS median');
set(hl,'interpreter','latex');

hBox.print('./pub/TUT_impact_others','-png');
%EOF