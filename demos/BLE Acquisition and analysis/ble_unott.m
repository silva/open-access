%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%   TAMPERE UNIVERSITY OF TECHNOLOGY
%
%   Fitting BLE RSS measurements
%
%   NOTE
%   This section requires the translation of the txt files to m files
%   
%   Please tun ble_parser before hand and point datadir to storage/<target>
%
%   Pedro Silva
%   pedro.silva@tut.fi
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clear variables
close all
addpath('./functions/');

%% Some necessary calls (do not change)
ble_retrieve_PLd0; % (.m) retrieves PL at d0 (1 m) for given tags

% colormap scheme (must be here, ble_retrieve clear everything)
global cmap;
h=figure;
cmap = jet;
close(h);

%% INPUTS
ble_master_tag  = 3;      % (#)   BLE tag number

use_model_itu   = 1;      % (b) changes model fit
pl_manufacturer = 0;      % (b) 1 - Manufacter value for PLd0, otherwise the measured one is used
overlay_other   = 1;      % (b) overlays fitted model parameters

print_figures   = 1;      % (b) print out figures
fig_out_type    = '-png'; % ([s]) figure format (supported by export_fig/matlab)
hfigdir         = 'pub/'; % ([s]) output for figures


% Parameters estimated beforehand
fitted_PL0_off  = -79.64;
fitted_eta_off  = 1.0410;

fitted_PL0_on   = -76.81;
fitted_eta_on   = 1.083;

corridor_PL0_w  = -71.34;
corridor_eta_w  = 1;

corridor_PL0_s  = -68.0133;
corridor_eta_s  = 1.0948;

logdist_eta     = 0.99;  % from logdist model

%% Retrieving the data
datadir = 'resultsUNOTT/'; % ([s]) matfile location
ble_params;                % (.m)  constants, coordinates and others
ble_load_data;             % (.m)  reads m files to workspace

eta_log_dist =[ ...
    0.9418    0.8907;
    1.0103    0.9517;
    0.9911    0.9675;
    1.0502         0;
    0.9084         0;
    0.9981         0;
    0.9993    1.0503;
    0.9406    1.0575];


%% Processing the data
ble_fit_model_unott;       % (.m)  fits model to data
ble_create_models;         % (.m)  build anon fct (requires PL0 from fit_model)

PL_log_dist_strong  = PLd0; 
PL_log_dist_weak    = PLd0;

if use_model_itu == 1
    eta_log_dist_strong = eta_log_dist(ble_master_tag,1);
    eta_log_dist_weak   = eta_log_dist(ble_master_tag,2);
else
    eta_log_dist_strong = eta(ble_master_tag,1);
    eta_log_dist_weak   = eta(ble_master_tag,2);
end
% For future use
PL_strong  = PL0(ble_master_tag,1);
eta_strong = eta(ble_master_tag,1);
PL_weak    = PL0(ble_master_tag,2);
eta_weak   = eta(ble_master_tag,2);

PL_tut     = fitted_PL0_off; % from the office measurements
eta_tut    = fitted_eta_off; % ...


%% Plotting
ble_plot_box_unott;     % (.m) plots boxplot for each probe location => hBox
ble_plot_box_scenarios; % (.m) plots boxplot with model for a given tag => hBoxModel
ble_plot_cdf_diff;      % (.m) plots a CDF with the difference in RSS for weak and strong interference
% ble_plot_time_series;   % (.m) plots the time series for each measurement file
ble_eval_fit_unott;
%% PRINTING
if print_figures == 1
    hfigdir = 'pub/';
    outtype = '-png';
    mkdir(hfigdir);
    
    hBox.print([hfigdir,'UNOTT_boxplot'],fig_out_type);
    hBoxModel.print([hfigdir,'UNOTT_boxplot_comparison'],fig_out_type);
    hCDFd.print([hfigdir,'UNOTT_cdf'],fig_out_type);
    
    try
        hTM.print([hfigdir,'UNOTT_tm'],fig_out_type);
    end
end

%EOF