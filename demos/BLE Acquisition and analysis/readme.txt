%
%   TAMPERE UNIVERSITY OF TECHNOLOGY
%
%   This folder contains all the scripts used for the acquisition of BLE
%   RSS at UNOTT and TUT
%
%   Author:
%   Pedro Silva
%   pedro.silva@tut.fi
%
%
%   Year: 2015
%
%   NOTE: all the data was always acquired with the same equipment and the ble tags were set to power level 3 (-12 dBm)



%% Preparing the setup (LOGS)
The logs are assumed to be (by default) in ./storage and should follow the output of linuz_script/ble_acq.sh
You can change this behaviour in ble_parser.m. 


%% Brief desription
ble_parser.m converts the text files to several matrices necessary for the following scripts

1. ble_unott
2. ble_tut
3. ble_tut_multiple
4. ble_estimate_interferance

Since these are mostly scripts please be careful and respect the execution flow in them :) 
Anyway, for reference you should guarantee the following,

. clean or dirty matlab workspace with ble_parser executed at least once!
  | ble_unott
  
  | ble_tut

  | ble_tut_multiplse

  (you can execute these three in any given order)


%% 1. ble_unott
This script looks at the data in the workspace assuming it is from the nottingham measurement campaign, as follows,

(check map attached for A, B and C and coordinate zero)

Case 1:   All on (1 � 8) @ A
Case 2:   All on (1 � 8) @ B
Case 3:   All on (1 � 8) @ C
Case 4:   Only  7 & 8 Broadcasting @ A (Failed)
Case 5:   Only  7 & 8 Broadcasting @ B
Case 6:   Only  7 & 8 Broadcasting @ C
Case 7:   Only  1 & 2 Broadcasting @ A
Case 8:   Only  1 & 2 Broadcasting @ B
Case 9:   Only  1 & 2 Broadcasting @ C
Case 10:  Only  3 Broadcasting @ A
Case 11:  Only  3 Broadcasting @ B
Case 12:  Only  3 Broadcasting @ C



%% 2. ble_tut
This script looks at the data in the workspace assuming it is from the TUT office measurement campaign, as follows,

tag_coordinates_TUT = [ 0.05   0; %1
                        0.10   0; %2
                        0.50   0; %3
                        1      0; %4
                        1.5    0; %5
                        2      0; %6
                        2.5    0; %7
                        3      0];%8


%% 3. ble_tut_multiple
This script is meant to see the impact of more tags broadcasting at the same time. The signal metrics should be worse when more tags are transmitting and the tag under observation (3) was kept at 1 meter.


%% 4. ble_estimate_interferance
This script allows you to look at the RSS of tag 3 when in presence of several other tags (office at TUT)

%% Other interesting files
You might also want to take a look into the ble_params.m and ble_create_models to get a better understanding of some of the parameters being used.


%% Messing with the main files
There are a few parameters that you should take into consideration, 

use_model_itu   = 0;      % (b) changes model fit
pl_manufacturer = 0;      % (b) 1 - Manufacter value for PLd0, otherwise the measured one is used
overlay_other   = 1;      % (b) overlays fitted model parameters

print_figures   = 0;      % (b) print out figures
fig_out_type    = '-png'; % ([s]) figure format (supported by export_fig/matlab)
hfigdir         = 'pub/'; % ([s]) output for figures

Most of these are flags that enable a certain model or overlay a certain model over the boxplots.
The most relevant flag might be the use_model_itu, which changes between the indoor ITU-R recommendation and the log-distance model.

Another interesting one is the pl_manufacturer which switches between the apparent power reported by the tag or the power measured at the office (at 1 m).


%% Remarks
Any comment or question can be placed to pedro.silva@tut.fi