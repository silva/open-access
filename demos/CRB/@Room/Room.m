classdef Room < build.scenario
    %UNTITLED Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        outfiles
        
    end
    
    methods
        function obj = Room(len,wid,dep,ndim,unit,dir,matfilename,forceOW)
            obj = obj@build.scenario(len,wid,dep,ndim,unit,dir,matfilename,forceOW);
        end
        
        
        function create2DWalk(obj,nbSteps,lenSteps,nbWalks,maxEm)
            %CREATE2DWALK generates N walks of size nbSteps with a step
            %size defined by lenSteps.
            % The walks are stored in a matfile.
            
            obj.storage.walk(nbSteps,2,nbWalks)=NaN;
            obj.storage.number_of_walks = nbWalks;
            obj.storage.number_of_steps_per_walk = nbSteps;
            obj.storage.ermse(nbSteps,6,nbWalks,maxEm,maxEm,maxEm,maxEm,4)=NaN; %this is to specificy for a general implementation
            obj.storage.estimates(nbSteps,obj.ndim,6,nbWalks,maxEm,maxEm,maxEm,maxEm,4)=NaN; %this is to specificy for a general implementation
            head = 1;
            len  = nbSteps;
             for k=1:nbWalks
                wpos = build.walk.gridRamdomWalk(nbSteps,obj.limits(:,1:2),lenSteps);
                obj.storage.walk(head:head+len-1,:) = wpos;
                head = head+len;
            end
        end
        

        function ranging(obj,Freq,BW,G,Nfig,F2,model,To,clk_off_max)
            N = obj.storage.nbObjs;
            S = obj.storage.number_of_steps_per_walk;
            P = obj.storage.number_of_walks;
            
            obj.storage.ranges       = zeros(N*S*P,1);
            obj.storage.cno          = zeros(N*S*P,1);
            obj.storage.svar         = zeros(N*S*P,1);
            obj.storage.measurements = zeros(N*S*P,1);
            
            ranges(N,S)               = NaN;
            cno(N,S)                  = NaN;
            svar(N,S)                 = NaN;
            measurements(N,S)         = NaN;
            
            % for each path
            head = 1;
            len  = S*N;
            for p = 1:P
                walk = obj.storage.walk(:,:,p);
                for s = 1:S
                    ranges(:,s)   = math.distance(walk(s,:),obj.objs(:,1:2));
                    for n=1:N %Computes CN0 and retrieves variance for each signal
                        [cno(n,s),svar(n,s)] = radio.pathloss.dist2cno(ranges(n,s),Freq(n),BW(n),G(n),Nfig(n),F2(n),model(n,:),To);
%                         meas_err(n,s)        = normrnd(0,sqrt(svar(n,s)));
                        measurements(n,s)    = ranges(n,s) + normrnd(0,sqrt(svar(n,s)));% + math.drawnumber(0,clk_off_max,1)*299792458;
                        if  measurements(n,s) < 0 
                             measurements(n,s) =  abs(measurements(n,s));
                        end
                    end
                end
                
%                 obj.storage.merr(head:(head+len-1),1)         = meas_err(:);
                obj.storage.ranges(head:(head+len-1),1)       = ranges(:);
                obj.storage.cno(head:(head+len-1),1)          = cno(:);
                obj.storage.svar(head:(head+len-1),1)         = svar(:);
                obj.storage.measurements(head:(head+len-1),1) = measurements(:);
                
                %updates head
                head = head+len;
            end
            
            clear ranges
            clear cno
            clear svar
            clear measurements
            % vectorises storage
            % obtain correct matrix format by reshaping to
            % ranges = reshape(ranges,N,S,P)
            % cno    = reshape(cno,N,S,P)
            % svar   = reshape(svar,N,S,P)
            %eg, A=reshape(obj.storage.ranges,N,S,P)
        end
        
        function [ranges,cno,svar,measurements] = getMatrices(obj)
            N = obj.storage.nbObjs;
            S = obj.storage.number_of_steps_per_walk;
            P = obj.storage.number_of_walks;
            
            ranges       = reshape(obj.storage.ranges,N,S,P);
            cno          = reshape(obj.storage.cno,N,S,P);
            svar         = reshape(obj.storage.svar,N,S,P);
            measurements = reshape(obj.measurements.svar,N,S,P);
        end
        
        function [idx] = getMatIndex(obj,x,y,z)
            N = obj.storage.nbObjs;
            S = obj.storage.number_of_steps_per_walk;
            P = obj.storage.number_of_walks;
            
            idx = sub2ind([N,S,P],x,y,z);
        end
        
        function [aml_err,lsq_err] = extract_estimates(obj,sim_id)
           
            % for a given simulation there are
            % N walks of M steps, therefore
            % the chunk to extract will be of size
            N = obj.storage.number_of_walks;
            M = obj.storage.number_of_steps_per_walk;
            L = N*M*3; % three entries [(x,y) error]
           
            % Since the goal is to extract the data for sim_id
            h = (sim_id-1)*L+1;
            t = (sim_id)*L;
            
            est_aml = obj.storage.aml_estimates(h:t,1);
            est_lsq = obj.storage.lsq_estimates(h:t,1);
                        
            % now I need to unfold it
            state = 0;
            ch    = 1;
            ct    = M;
            head  = 1;
            len   = M;
            for k=1:N*3 % number of blocks
                % h is the start, so 
                % extract chunk of nbSteps
                chunk  = est_aml(head:head+len-1); % takes chunk of M size
                chunk2 = est_lsq(head:head+len-1); % takes chunk of M size
                switch(state) % puts it to the right place
                    case 0, %x
                    aml_pos(ch:ch+ct-1,1) = chunk;
                    lsq_pos(ch:ch+ct-1,1) = chunk2;
                    state = 1;
                    case 1, %y
                    aml_pos(ch:ch+ct-1,2) = chunk;
                    lsq_pos(ch:ch+ct-1,1) = chunk2;
                    state = 2;    
                    case 2, %err
                    aml_err(ch:ch+ct-1,1) = chunk;
                    lsq_err(ch:ch+ct-1,1) = chunk2;
                    ch = ch+ct; % updates indexes
                    state = 0;
                end
                head = head+len;    
            end
            
            % the same as the thing up
            est_aml = reshape(est_aml,M,3,N);
            est_lsq = reshape(est_lsq,M,3,N);
        end
    end
    
end

