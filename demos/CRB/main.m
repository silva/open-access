%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%         TUT         %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%   AML POSITIONING   %%%
%%%                     %%%
%%%   Pedro Silva       %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%

clear variables
close all

GetNewTracks = true;
warning('off','all')
saving_dir   = './results/10x10/';
mkdir(saving_dir);

%% run switchers
run_aml       = true;
run_nlsq      = true;
run_lsq       = true;
run_animation = true;

%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Signal   Parameters %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%
CN0       = 5:5:50;         % carrier to noise ratio, (dB)
fs        = 100e6;          % sampling frequency, (Hz)
simTime   = 1e-03;          % simulation time, (s)
Ts        = 1/fs;           % sampling period, (s)
c         = 3e8;            % speed of light (m/s)

To        = simTime;%*fs;
B         = fs;

go        = 3;              % delta coefficient, 2 in free space
ko        = 1;              % constant parameter (?)
D         = 1:1:10;         % distance (m)
plotCN0   = 40;
alpha     = ko./(sqrt(D.^(go)));

cno       = 10.^(CN0./10);  % linear conversion of CN0

%NOTE all bounds are vars, lets stick to this
% Bounds equations
F2      = @(f,s) sum( (2.*pi.*f).^2 .* abs(s).^2)./ ( sum(abs(s).^2));
% output/input control
hOut   = inout.output();

% CDMA based
flag_cdma = 1;
flag_ofdm = 0;
generateWCDMA;
createbounds;
wcdma     = cdma;
F2wcdma   = F2cdma;

generateWIFIb;
createbounds;
wifib     = cdma;
F2wifib   = F2cdma;

% OFDM based
flag_cdma = 0;
flag_ofdm = 1;
generateWIFIac;
createbounds;
wifiac     = ofdm;
F2wifiac   = F2ofdm;
CRBwifiac  = CRBofdm;
CRBDwifiac = dpCRBDofdm;
generateWIFIg;
createbounds;
wifig     = ofdm;
F2wifig   = F2ofdm;



F_2(1) = F2wifib;
F_2(2) = F2wifig;
F_2(3) = F2wifiac;
F_2(4) = F2wcdma;

%% Signal prep
Freq(1) = 2.4e9; %wifib
Freq(2) = 2.4e9; %wifig
Freq(3) = 5.0e9; %wifiac
Freq(4) = 2.1e9; %fwcdma


% (d,n,L,f,s)

NF(1) = 8;
NF(2) = 8;
NF(3) = 8;
NF(4) = 6.5;
% NF(1:4) = 0;

RBW = 100e6; %receiver bandwith
BW(1) = 22e6;
BW(2) = 20e6;
BW(3) = 40e6;
BW(4) = 5e6;

G(1:3) = 0;
G(4)   = 0;

%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Scenario Parameters %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%
room_length = 10;
room_width  = 10;
room_depth  = [];
ndim        = 2;
unit        = 1; % 1 entry == 1 meter
nbSteps     = 100;%room_length*room_width;
lenStep     = 1;
nbWalks     = 1;

maxEmitters    = 10;
nbTechnologies = 4;
totEm          = nbTechnologies*maxEmitters;
techtype       = NaN(totEm,1);
type_counter   = 0;
for k=1:maxEmitters:totEm
    type_counter = type_counter + 1;
    techtype(k:k+maxEmitters-1) = type_counter;
end

PLModel{1} = [{@radio.pathloss.ITU_indoor_rss};{@wifi_model_params}];
PLModel{2} = [{@radio.pathloss.ITU_indoor_rss};{@wifi_model_params}];
PLModel{3} = [{@radio.pathloss.ITU_indoor_rss};{@wifi_model_params}];
PLModel{4} = [{@radio.pathloss.ITU_indoor_rss};{@umts_indoor_model_params;}];

hFuncs     = {PLModel{techtype}}.';
clk_offset = 0;


for nbscenarios = 1:1
    %%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%% Scenario Creation %%%
    %%%%%%%%%%%%%%%%%%%%%%%%%%%
    disp('Preparing scenario')
    room = Room(room_length,room_width,room_depth,ndim,1,saving_dir,['Journal_',num2str(nbWalks),'_',num2str(room_length),'x',num2str(room_width),'_WalksAndMeasurements'],GetNewTracks);% generate a room
    %     if GetNewTracks && ~room.exists
    disp('... building new data')
    room.placeObjects(totEm,techtype,[]);%place stations randomly in the room
    room.create2DWalk(nbSteps,lenStep,nbWalks,maxEmitters)% generate walks
    room.ranging(Freq(techtype),BW(techtype),G(techtype),NF(techtype),F_2(techtype),hFuncs,simTime,clk_offset); % computes measurement for each equipment
    %     end
    % YES, coordinates are starting at zero!
    
    
    %% from here, select the scenario and apply it for each track
    %clearvars -except room saving_dir nbWalks room_length room_width nbscenarios;
    %close all;
    disp(['Starting positioning scenario : ',num2str(nbscenarios)])
    
    % Output/input
    hOut = inout.output();
    xlim([0 room.wid])
    ylim([0 room.len])
    hold(gca,'on');
    
    
    % get data
    emmitters   = room.storage.objs;   % emitters location
    start       = mean(emmitters(:,1:2));  % mean of emitter location
    nbSteps     = room.storage.number_of_steps_per_walk;
    nbWalks     = room.storage.number_of_walks;
    
    % technology selectors
    techtype  = room.storage.objs(:,3);
    nbVec     = 1:length(techtype);
    sel_b     = nbVec(techtype==1);
    sel_g     = nbVec(techtype==2);
    sel_ac    = nbVec(techtype==3);
    sel_wc    = nbVec(techtype==4);
    
    % parameters
    tol       = 1e-3;
    maxit     = 20;
    minCN0    = 0;%5;
    
    
    % for one particular selection
    mfile       = room.storage;
    nbEmmitters = size(emmitters,1);
    head        = 1;
    len         = nbSteps*nbEmmitters;
    mfile.walk_val = zeros(nbWalks,2);
    for walk_num=1:nbWalks
        mfile.walk_val(walk_num,1:2) =  [head (head+len-1)];
        head      = head + len;
    end
    
    try
    clear sel_tech
    clear sel_em
    end
    % Selects technology cases
    n=0;
    for b_i = [ 0 1 3 5 10]%5 10 25];%10 25 50 100 ];% 10 25];%[ 1 5 10 25 50 100]
        for g_i =  [ 0 1 3 5 10]% 5 10 25];%10 25 50 100];% 10 25];%[ 1 5 10 25 50 100 ]
            for ac_i = [ 0 1 3 5 10];%25 50 100];%[1 5 10 25 50 100]
                for wc_i =  [0 1 3 5 10];%25 50 100];%[1 5 10 25 50 100]
                    if ((b_i==0) && (g_i==0) && (ac_i==0) && (wc_i==0) ), continue; end;
                    if ((b_i + g_i + ac_i + wc_i)<2 ), continue; end;
                    n=n+1;
                    sel_tech(n,:) = {[b_i, g_i, ac_i, wc_i]};
                    sel_em(n,:)   = {[sel_b(1:b_i), sel_g(1:g_i), sel_ac(1:ac_i), sel_wc(1:wc_i)]};
                end
            end
        end
    end
    room.storage.sim_id_cases = sel_em;
    
    nbSims      = size(sel_em,1);
    head        = 1;
    len         = nbSteps; %storing 3 values
    for walk_num=1:nbWalks*nbSims
        mfile.est_val(walk_num,1:2) =  [head (head+len-1)];
        head      = head + len;
    end
    
    
    % create nfiles to store the data
    room.outfiles = [];
    for k=1:size(sel_em,1)
        room.outfiles{k} = inout.output.createMatfile(saving_dir,['sim_output_',num2str(k),'_for_',num2str(nbWalks),'_',num2str(room_length),'x',num2str(room_width),'_',num2str(nbscenarios)],true,true);
        room.outfiles{k}.tech_id        = k;
        room.outfiles{k}.em_ids         = sel_em;
        room.outfiles{k}.tech           = sel_tech;
        room.outfiles{k}.aml_estimates  = NaN(nbSteps*nbWalks,1);
        room.outfiles{k}.lsq_estimates  = NaN(nbSteps*nbWalks,1);
        room.outfiles{k}.nlsq_estimates = NaN(nbSteps*nbWalks,1);
    end
    
    
    %% Compute Estimates
    disp('starting estimation')
    for k=1:size(sel_em,1)
        
        sim_output = room.outfiles{k};
        sel_tech   = sel_em{k};
        
        for walk_num=1:nbWalks
            
            % INPUTS
            track         = mfile.walk(:,:,walk_num);
            wval          = mfile.walk_val(walk_num,1):mfile.walk_val(walk_num,2);
            pointer       = mfile.est_val(walk_num,1):mfile.est_val(walk_num,2);
            cno           = mfile.cno(wval,1);
            meas          = mfile.measurements(wval,1);
            aml_estimate  = NaN(nbSteps,1);
            lsq_estimate  = NaN(nbSteps,1);
            nlsq_estimate = NaN(nbSteps,1);
            

            E_in          = emmitters(sel_tech,:); % indexes are valid for these ones as well
            init          = mean(E_in(:,1:2));
            
            %take into account walk_num
            nbEm          = size(E_in,1);
            len           = nbEmmitters;
            head          = 1;
            V             = eye(nbEm);
            
            
            start_aml  = [init];
            start_lsq  = [init];
            start_nlsq = [init];
            
            for step=1:nbSteps
                % retrieves information for the Nth walk
                pos = track(step,:);
                mes = meas(head:head+len-1,1);
                
                %sanity checking
                assert(all(mes>=0));
                
                L_in  = mes(sel_tech);
                
                
                % call estimators
                if run_nlsq
                    %nlsq id
                    ign = 0;
                    [epos,ermse,~,ign]  = nlsq( start_nlsq, pos, E_in(:,1:2), L_in, tol, maxit);
                    
                    if ~ign
                        start_nlsq = epos;
                        nlsq_estimate(step,:) = ermse;
                    else
                        start_nlsq = [init,0];
                    end
                    if isnan(ermse)
                        start_nlsq = [init,0];
                    end
                end
                
                if run_lsq
                    %lsq id
                    ign = 0;
                    [epos,ermse,~,ign] = lsq( start_lsq, pos, E_in(:,1:2), L_in, tol, maxit);
                    if ~ign
                        start_lsq            = epos;
                        lsq_estimate(step,:) = ermse;
                    else
                        start_lsq = [init,0];
                    end
                    if isnan(ermse)
                        start_lsq = [init,0];
                    end
                end
                
                if run_aml
                    %aml id
                    ign = 0;
                    [epos,ermse,~,ign] = aml( start_aml, pos, E_in(:,1:2), L_in, 1, tol, maxit,[]);
                    if ~ign
                        start_aml            = epos;
                        aml_estimate(step,:) = ermse;
                    else
                        start_aml = init;
                    end
                    if isnan(ermse)
                        start_aml = init;
                    end
                end
                ign = 0;
                if run_animation
                    hOut.animateEstimation(step,track, E_in(:,1:2),epos,V,50,50)
                end
                head = head + len; % overall increase
            end
            
            sim_output.aml_estimates(pointer,1)  = aml_estimate(:);
            sim_output.lsq_estimates(pointer,1)  = lsq_estimate(:);
            sim_output.nlsq_estimates(pointer,1) = nlsq_estimate(:);
            
            sum(~isnan(aml_estimate(:)))
            sum(~isnan(lsq_estimate(:)))
            sum(~isnan(nlsq_estimate(:)))
            
        end
        
        
        
    end
    
end