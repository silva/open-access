% Tampere University of Technology
%
%   This script implements a simple MLA approach to solve a TOA problem for
%   the case when OFDM and CDMA emmitters are present in the vicinity.
%   These two stations are characterised by different variances.
%
%   Pedro Silva, 2014
%
%   TODO
%   I want candle plots done for this
%
function callcpe(nbsim,variance_CDMA,variance_OFDM,clk_bias_CDMA,clk_bias_OFDM, interface_name)
    
    % Loop parameters
    number_simulations   = nbsim;     % (#) number of simulations
    Ndim                 = 2;       % (#) 2D or 3D dimensions
    IT_MAX               = 10;      % (#) Max number of iterations
    break_val            = 1e-4;    % (#) Breaking condition
    
    % Operational parameters
    
    % Animation parameters
    record_video         = 0;       % (?) 1/0 to/skip video recording
    plotscenario         = 0;       % (?) animates and plots the scenario
    animation_delay      = 0.5;     % (s) increase to slow down plotting in each run
    
    
    % Saving parameters
    save_data            = 0;       % (?) 1/0 to/skip sav
    number_data_store    = 6;       % (#) allocation for saving matrix
    savetofolder         = 'resultsWithVar/';
    mkdir(savetofolder);
    
    
    % Plot parameters
    marker_value_user    = 1;       % Marker for user
    marker_value_cdma    = 2;       % Marker for cdma
    marker_value_ofdm    = 3;       % Marker for ofdm
    idx_figure           = 0;
    
    % Grid parameters
    grid_size            = 25;      % (m^2) size of grid (NxN)
    nb_CDMA_bs           = 0:5;     % ([])  vector with number of CDMA to test
    nb_OFDM_bs           = 0:5;     % ([])  vector with number of OFDM to test
    
    
    warning('off','all')
    mean_rmse = NaN(numel(variance_CDMA),numel(variance_OFDM),numel(nb_CDMA_bs),numel(nb_OFDM_bs));
    
    if record_video == 1
        vobj = createVideo('mla',2);
    else
        vobj = [];
    end
    mean_counter = 0;
    
    
    for_each_var_CDMA = 0;
    for_each_var_OFDM = 0;
    
    while for_each_var_CDMA < length(variance_CDMA) && for_each_var_OFDM < length(variance_OFDM)
        for_each_var_CDMA = for_each_var_CDMA + 1;
        for_each_var_OFDM = for_each_var_OFDM + 1;
        
        for for_each_nb_CDMA = 1:length(nb_CDMA_bs)
            for for_each_nb_OFDM = 1:length(nb_OFDM_bs)
                
                % create file here
                filename = [savetofolder,'run_vCDMA_',num2str(variance_CDMA(for_each_var_CDMA)),...
                    '_vOFDM_',num2str(variance_OFDM(for_each_var_OFDM)),...
                    '_NCDMA_',num2str(nb_CDMA_bs(for_each_nb_CDMA)),...
                    '_NOFDM_',num2str(nb_OFDM_bs(for_each_nb_OFDM)),'.mat'];
                %                     disp('Processing data for')
                %                     disp(filename);
                
                % preparing loop variables
                N_CDMA  = nb_CDMA_bs(for_each_nb_CDMA); % CDMA number of base stations
                N_OFDM  = nb_OFDM_bs(for_each_nb_OFDM); % OFDM number of base stations
                bs_cdma = zeros(N_CDMA,Ndim);           % CDMA base stations positions
                bs_ofdm = zeros(N_OFDM,Ndim);           % OFDM base stations positions
                
                % sanity check
                if (N_CDMA+N_OFDM) < 2
                    continue;
                end
                
                % SIM LOOP
                skip_save     = 0;
                break_counter = 0;
                saveid        = 0;
                storeData     = zeros(number_simulations,number_data_store);
                for simid=1:number_simulations
                    
                    % Variance
                    var_cdma = variance_CDMA(for_each_var_CDMA);
                    var_ofdm = variance_OFDM(for_each_var_OFDM);
                    
                    % Create grid
                    grid = zeros(grid_size);
                    
                    % create vector for true ranges
                    R_cdma = zeros(N_CDMA,1);
                    l_cdma = zeros(N_CDMA,1);
                    R_ofdm = zeros(N_OFDM,1);
                    l_ofdm = zeros(N_OFDM,1);
                    
                    % Get random position for user
                    userxy(1,:)               = unidrnd(grid_size,Ndim,1);
                    grid(userxy(1),userxy(2)) = marker_value_user;
                    
                    
                    % CDMA: Generate positions and ranges
                    for k=1:N_CDMA
                        bs_cdma(k,:)                    = unidrnd(grid_size,Ndim,1);
                        grid(bs_cdma(k,1),bs_cdma(k,2)) = marker_value_cdma;
                        R_cdma(k,1)                     = norm(userxy-bs_cdma(k,:));
                        l_cdma(k,1)                     = R_cdma(k,1) + clk_bias_CDMA + normrnd(0,sqrt(var_cdma));
                    end
                    
                    
                    % OFDM: Generate positions and ranges
                    for k=1:N_OFDM
                        bs_ofdm(k,:)                    = unidrnd(grid_size,Ndim,1);
                        grid(bs_ofdm(k,1),bs_ofdm(k,2)) = marker_value_ofdm;
                        R_ofdm(k,1)                     = norm(userxy-bs_ofdm(k,:));
                        l_ofdm(k,1)                     = R_ofdm(k,1) + clk_bias_OFDM + normrnd(0,sqrt(var_ofdm));
                    end
                    
                    % Grid control
                    if plotscenario == 1
                        gridshow(grid);
                    end
                    
                    % Create global vectors (CDMA first then OFDM)
                    nbs         = N_CDMA+N_OFDM;
                    bs_all      = [ bs_cdma(:,1) bs_cdma(:,2);...
                                    bs_ofdm(:,1) bs_ofdm(:,2)];
                    ri          = [ R_cdma(:); R_ofdm(:)];
                    li          = [ l_cdma(:); l_ofdm(:)];
                    var_all     = ones(size(bs_all,1),1);%[repmat(var_cdma,size(bs_cdma,1),1);repmat(var_ofdm,size(bs_ofdm,1),1)];
                    init_userxy = mean(bs_all);
                    
                    % Run estimation algorithm
                    [xy, flag_save, break_counter ] = doAML(init_userxy, userxy, bs_all, var_all, ri, li, Ndim, IT_MAX, simid, break_counter, break_val, record_video, plotscenario, animation_delay,vobj);
                    
                    % RMSE
                    try
                        if ~isrow(xy)
                            xy = xy';
                        end
                        xe    = xy;
                        xr    = userxy;
                        ermse = rmse(xr,xe);
                        if ermse > 100
                            ermse = 0;
                            flag_save = 0;
                        end
                    catch
                        xe    = [NaN NaN];
                        xr    = userxy;
                        ermse = NaN;
                    end
                    
                    % Data to save
                    storeData(simid,:) = [xr, xe, ermse, flag_save];
                    
                end %END of simulation
                
                % compute and add mean
                idx_figure = idx_figure+1;
                allvalid   = storeData(:,end)==1;
                mrse       = mean(storeData(allvalid,end-1));
                if isnan(mrse)
                    continue;
                end
                mean_rmse(for_each_var_CDMA,for_each_var_OFDM,for_each_nb_CDMA,for_each_nb_OFDM) = mrse;
                if save_data == 1 && skip_save == 0;
                    save(filename,'storeData');
                end
                %disp('done for now');
                %disp(['Break % : ',num2str(break_counter/number_simulations*100)])
            end
        end
        % Cleanup
        if save_data == 1;
            save([savetofolder,'mean_rmse'],'mean_rmse');
        end
        disp('finished');
        if record_video == 1
            close(vobj);
        end
        
        %results with Var
        varCDMAidx = 1;
        varOFDMidx = 1;
        
        result_matrix  = NaN(nb_CDMA_bs(end)+1,nb_OFDM_bs(end)+1);
        % varCDMA, varOFDM, ctrCDMA, ctrOFDM
        fprintf('CDMA : OFDM | RMSE \n')
        for idxO = 1:length(nb_OFDM_bs)
            for idxC = 1:length(nb_CDMA_bs)
                if nb_CDMA_bs(idxC)+nb_OFDM_bs(idxO) < 2, continue, end;
                if result_matrix(nb_CDMA_bs(idxC)+1,nb_OFDM_bs(idxO)+1) >= 1, continue, end;
                fprintf('%d : %d | %2.2f\n',nb_CDMA_bs(idxC), nb_OFDM_bs(idxO), mean_rmse(varCDMAidx,varOFDMidx,idxC,idxO))
                result_matrix(nb_CDMA_bs(idxC)+1,nb_OFDM_bs(idxO)+1) = mean_rmse(varCDMAidx,varOFDMidx,idxC,idxO);
            end
            disp('-');
        end
        
        
        
        % do a plot of this with scatter
        x            = repmat(nb_CDMA_bs,size(result_matrix,1),1);
        y            = repmat(nb_OFDM_bs',1,size(result_matrix,1));
        dump_to_file = [[NaN;nb_CDMA_bs'], [nb_OFDM_bs;result_matrix]];
        dump_to_file = [dump_to_file;dump_to_file(end,:)];
        dump_to_file(end,:) = 0;
        dump_to_file(end,1) = var_cdma;
        dump_to_file(end,2) = var_ofdm;
        dump_to_file(end,3) = nbsim;
        
        
        xlswrite([savetofolder,'results_AML_',interface_name,'_',num2str(nbsim),'CDMAV_',num2str(for_each_var_CDMA),'OFDMV_',num2str(for_each_var_CDMA)],dump_to_file);
    end
end
%EOF