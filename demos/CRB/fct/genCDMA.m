function [ tx, spCode ] = genCDMA(fb, Nc, spFactor, modulation, modSelect, spCode, fc, fs)
    %GENCDMA Generates a CDMA signal
    %   USAGE
    %   [ tx ] = genCDMA(nbSamples, overSample, spreadingFactor, ...
    %                    modulation, modSelect, sgPower, ...
    %                    spreadingCode, txbits, hadamardSeq, ...
    %                    hadamardSize);
    %
    %   DESCRIPTION
    %   A cdma signal is returned with the desired CNR and MODULATION.
    %   The signal is also oversampled by the OVERSAMPLE factor, which should
    %   be a positive number bigger than one.
    %   NBSAMPLES controls the size of the random generated binary message
    %   that is used throughout the process, if TXBITS is empty.
    %   A 11 bit Barker code is used for the spreading process.
    %
    %   INPUT
    %   nbSamples       - Number of random data samples
    %   overSample      - Over sample factor
    %   spreadingFactor - Size of pseudorandom code to be used
    %   modulation      - Modulation size
    %   [modSelect]     - Modulation type ('psk' (by default) or 'qam')
    %   [spreadingCode] - Use the specified spreading code (802 for barker)
    %   [txBits]        - Message to be used (ignores nbSamples)
    %
    %   OUTPUT
    %   tx             - CDMA signal
    %
    %   NOTE:
    %   This function uses by default the Barker code for 1 - 2 Mbit/s 802.11
    %   signals. To generate Walsh codes, obtain an walsh-hadamard window and
    %   each row will serve as a orthogonal code.
    %   Regarding spreading factor, the CDMA code spreading factor is given
    %   by the ratio of the chipping rate over the data rate. This means
    %   that for a 802.11b signal we have 11 Mchips/s since the data rate
    %   is 1 Mbit/s, leaving us with a SF of 11.
    %
    %
    %   Example:
    %       codeSize = 8
    %       hdwindow = hadamard( getpoweroftwo(codeSize) );
    %       hdwindow = [ 1     1     1     1     1     1     1     1;
    %                    1    -1     1    -1     1    -1     1    -1;
    %                    1     1    -1    -1     1     1    -1    -1;
    %                    1    -1    -1     1     1    -1    -1     1;
    %                    1     1     1     1    -1    -1    -1    -1;
    %                    1    -1     1    -1    -1     1    -1     1;
    %                    1     1    -1    -1    -1    -1     1     1;
    %                    1    -1    -1     1    -1     1     1    -1;
    %                   ];
    %
    %   Pedro Silva, Tampere University of Technology, 2013
    
    % Input parameter assertion
    %     assert(nbSamples  > 0);
    %     assert(overSample > 0);
    assert(modulation > 0);
    
    Ns             = fs/fc;           %oversampling factor
    
    % Defaults
    if ~exist('modSelect','var') || isempty(modSelect)
        modSelect = 1; %qam default
    end
    
    if ~exist('spFactor','var') || isempty(spFactor)
        spFactor = 11;
    end
    
    
    if spFactor == 11
        spCode = [1 -1 1 1 -1 1 1 1 -1 -1 -1];
        data   = randsrc(1,Nc,0:modulation-1);
        data   = psk(data,modulation); %10 in, 10 out
        data   = kron(data,spCode);
        
    elseif spFactor == 8
        % Encoding
        data = randsrc(1,Nc*4,0:1);
        data = cckmod(data); %spread and modulate
    elseif ~exist('spCode','var') || isempty(spCode)
        data   = randsrc(1,Nc,0:modulation-1);
        if modSelect == 1
            data   = qammod(data,modulation,[],'gray'); %10 in, 10 out
        else
            data   = psk(data,modulation); %10 in, 10 out
        end
        spCode = randsrc(1,spFactor,-1:2:1); %Generates random code
        data   = kron(data,spCode);
    end
        
    Ts             = 1/fs;                      % sampling period in seconds
    Tc             = 1/fc;                      % chip period in seconds
    tx             = repmat(data,1, ceil(Ns));  % oversample 
    SamplesPerCode = round(fs/fc*spFactor);     
    CodeValueIndex = ceil(Ts/Tc*[1:Nc*SamplesPerCode]);
    tx             = tx(CodeValueIndex);
    
    % Normalise signal power
    tx = tx(:)./(sqrt(mean(abs(tx(:)).^2)));
end


function data = psk(data,N)
    %PSK maps the input symbols to N chips
    %   USAGE
    %   data = psk(data,N)
    %
    %   INPUT
    %   data - symbol stream
    %   N    - constellation mapping size
    %
    %   OUTPUT
    %   data - BPSK or DQPSK modulated signal
    %
    %   Pedro Silva, Tampere University of Technology, 2013
    if N == 2 % QPSK
        data(data==0) = -1;
        
    elseif N == 4 % DQPSK
        data(data==1) = 1i;
        data(data==3) = -1;
        data(data==2) = -1i;
        data(data==0) = 1;
    else
        error('Available: BPSK and DPSK');
    end
    
end


% For reference
%
% if (exist('hadamardSeq','var') && exist('hadamardSize','var'))
%     assert(hadamardSeq<=hadamardSize);
%     [E,F] = log2(hadamardSize);
%     if E==0.5 %see help log2
%         wd = hadamard(hadamardSize);
%     elseif E < 0.5
%         wd = hadamard(2^(F+1));
%     else % E > 0.5
%         wd = hadamard(2^(F+1));
%     end
%     code = wd(hadamardSeq,:);
% end
%     if exist('code','var')
%         sdata = kron(mdata,code); %Apply channel code
%         sdata = kron(sdata,spreadingCode);
%     else
%         sdata = kron(mdata,spreadingCode);
%     end
%

%%% EOF
