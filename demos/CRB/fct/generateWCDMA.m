% parameters for WCDMA
% http://www.umtsworld.com/technology/wcdma.htm

cdma_mod      = 4;                    % Modulation order
cdma_mod_tp   = 0;                    % Modulation type (0:qpsk,1:qam)
cdma_fc       = 3.84e6;               % Chip rate
cdma_fb       = 2e6;                  % Binary rate
cdma_spfact   = 2560;%11;             % Spreading factor
cdma_nc       = ceil(simTime*cdma_fc/cdma_spfact); % Number of code epochs
cdma_theocycl = cdma_fc;              % Theorectical period of cf
cdma_symbrate = cdma_fc/(cdma_spfact*1e6); % symbol rate


inputs = [                              %%% Matrix with (#DATA SYMBOLS, OVERSAMPLING)
    cdma_fb*simTime, cdma_nc,...        % CDMA: Input symbol vector length | oversampling
    cdma_spfact, cdma_mod,...           %  "" : Spreading factor randi(RANGE,Lines,Cols)
    cdma_mod_tp, cdma_fc,...
    fs,NaN, cdma_symbrate,...
    cdma_theocycl;...                   %KEEP AS LAST ONE
    ];


cdma = genCDMA(inputs(1,1),... % Number of samples
    inputs(1,2),... % Number of code Epochs
    inputs(1,3),... % Spreading factor
    inputs(1,4),... % Modulation
    inputs(1,5),... % Modulation selection QAM 1 PSK 0
    [],...
    inputs(1,6),... % Chip frequency
    fs);
% cdma = cdma./max(cdma);
% P    = sum(abs(cdma).^2)*Ts;
% cdma = cdma/P;
