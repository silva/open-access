% Tampere University of Technology
%
%   This script computes OFDM and CDMA CRLB
%
%   Pedro Silva, 2014

close all
clear variables

% % According to Markku, root-raised cosine roll-off G(f) leads to
% alpha     = 0.7;
CN0       = 0:1:100;
No        = 1;
c         = 3e08;
fs        = 30e6;
simTime   = 10e-03; 
Kfc       = 2;    % Chipping rate multiplier
Ts        = 1/fs;


cno = 10.^(CN0/10);

% CDMA
cdma_input    = 1;                    % Activate (1) CDMA signal
cdma_fref     = 11e6;                 % CDMA: Signal reference frequency
cdma_mod      = 4;                   % Modulation order
cdma_mod_tp   = 0;                    % Modulation type (0:qpsk,1:qam)
cdma_fc       = 11e6*Kfc;                 % Chip rate
cdma_fb       = 2e6;                  % Binary rate
cdma_spfact   = 1023;%11;               % Spreading factor
cdma_nc       = ceil(simTime*cdma_fc/cdma_spfact); % Number of code epochs
cdma_theocycl = cdma_fc;              % Theorectical period of cf
cdma_symbrate = cdma_fc/(cdma_spfact*1e6); % symbol rate
cdma_stem     = 0;                    % Display cyc. frequencies

% OFDM
ofdm_input    = 1;                    % Activate (1) OFDM signal
ofdm_fref     = 10e6;                 % OFDM: Signal reference frequency
ofdm_Ntotal   = 64;                   % Number of carriers
ofdm_Ndata    = 48; % 4,8, 16, 32     % Guard interval factor NFFT/gi
ofdm_Npilot   = 8;
ofdm_Nsub     = ofdm_Ndata + ofdm_Npilot;
ofdm_bw       = 10e6;                 % OFDM bandwith
ofdm_ppower   = 1;                    % Pilot power boost
ofdm_mod      = 16;                   % Modulation
ofdm_mod_tp   = 1;                    % Modulation type (forced to QAM)

ofdm_deltaF   = ofdm_bw/ofdm_Ntotal;
ofdm_Tfft     = 1/ofdm_deltaF;
ofdm_Tgi      = ofdm_Tfft/4;
ofdm_sybtime  = ofdm_Tfft+ofdm_Tgi;

ofdm_symbols  = fix(simTime/ofdm_sybtime); % Number of OFDM symbols
ofdm_theocycl = ofdm_sybtime;         % Theorectical period of cyc. frequencies
ofdm_stem     = 1;                    % Display cyc. frequencies

inputs = [                              %%% Matrix with (#DATA SYMBOLS, OVERSAMPLING)
    cdma_fb*simTime, cdma_nc,...        % CDMA: Input symbol vector length | oversampling
    cdma_spfact, cdma_mod,...           %  "" : Spreading factor randi(RANGE,Lines,Cols)
    cdma_mod_tp, cdma_fc,...
    fs,NaN, cdma_symbrate,...
    cdma_theocycl;...                   %KEEP AS LAST ONE
    ofdm_symbols, ofdm_Ntotal,...
    ofdm_Ndata, ...
    ofdm_deltaF, ofdm_ppower,...
    ofdm_Nsub, ...
    ofdm_mod,...
    ofdm_mod_tp,...
    ofdm_sybtime,...
    ofdm_theocycl,... %KEEP AS LAST ONE
    ];


cdma = genCDMA(inputs(1,1),... % Number of samples
    inputs(1,2),... % Number of code Epochs
    inputs(1,3),... % Spreading factor
    inputs(1,4),... % Modulation
    inputs(1,5),... % Modulation selection QAM 1 PSK 0
    [],...
    inputs(1,6),... % Chip frequency
    fs);

% Generate OFDM
ofdm   = genOFMD(inputs(2,1),... % Number of symbols
    inputs(2,2),...        % Total number of carriers
    inputs(2,3),...        % Number of DATA carriers
    inputs(2,6),...        % Number of Pilot and Data carriers
    inputs(2,4),...        % Carriers separation (1/dF)
    inputs(2,5),...        % Pilot power
    inputs(2,7),...        % Modulation
    inputs(2,8),...        % Modulation selection QAM 1 PSK 0
    fs);                   % Sampling frequency


[Gcdma,Fcdma] = pwelch(cdma,[],[],[],fs,'centered');
Gcdmaabs      = abs(Gcdma).^2;

[Gofdm,Fofdm] = pwelch(ofdm,[],[],[],fs,'centered');
Gofdmabs      = abs(Gofdm).^2;


% Nofdm = length(ofdm);
% Fofdm = ((0:Nofdm-1)-fix(Nofdm/2)) * (fs/Nofdm); % [Hz] Frequency axis
% Fofdm = Fofdm(:);
% 
% Ncdma = length(cdma);
% Fcdma = ((0:Ncdma-1)-fix(Ncdma/2)) * (fs/Ncdma); % [Hz] Frequency axis
% Fcdma = Fcdma(:);
% 
% Gofdm=fftshift(fft(ofdm)).*Ts;
% Gcdma=fftshift(fft(cdma)).*Ts;







% 
% f=Fofdm;
% s=Gofdm;
% A=sum( (2.*pi.*f).^2 .* abs(s).^2)./ ( sum(abs(s).^2));
% stdRangeCRLB1 = sqrt(1./(cno.*1/2.*A.*simTime)) * c;
% 
F2   = @(f,s) sum( (2.*pi.*f).^2 .* abs(s).^2)./ ( sum(abs(s).^2));
CRBT = @(f2,no,e) sqrt(1./(cno.*1/2.*f2.*simTime)).*c;


F2ofdm  = F2(Fofdm,Gofdm);
CRBofdm = CRBT(F2ofdm,cno,rms(ofdm));

F2cdma  = F2(Fcdma,Gcdma);
CRBcdma = CRBT(F2cdma,cno,rms(cdma));



% Power should be one
%disp(rms(ofdm)^2);
%disp(rms(cdma)^2);
% Ecdma   = std(cdma);

% hold on;
figure;
subplot(2,1,1)
semilogy(CN0,CRBcdma);
title('CRLB_{CDMA}');
ylabel('Estimation error variance');
xlabel('CN0 [dB]');
subplot(2,1,2)
semilogy(CN0,CRBofdm);
title('CRLB_{OFDM}');
ylabel('Estimation error variance');
xlabel('CN0 [dB]');

figure;
semilogy(CN0,CRBcdma);hold on;
semilogy(CN0,CRBofdm,'r');
h=legend('$CRLB_{CDMA}$','$CRLB_{OFDM}$');
set(h,'interpreter','latex');
ylabel('Estimation error variance');
xlabel('CN0 [dB]');
title('CRLB for CDMA and OFDM signals');

figure;
subplot(2,1,1)
pwelch(cdma,[],[],[],fs,'centered')

subplot(2,1,2)
pwelch(ofdm,[],[],[],fs,'centered')

%%%% Distance related
% http://iwin.sjtu.edu.cn/yiyinwang/papers/ICASSP09.pdf
dlo = 2;    % delta coefficient, 2 in free space
ko  = 1;    % constant parameter (?)
D   = 1:100; % distance (m) 

% function
CRBD = @(d,icno,f) c^2.*d.^(dlo+2) ./ ( 2.*ko.^2.*icno.*( (dlo.^2.*c.^2)./4 + d.^2.*f ) );

for k=1:length(cno)
   CRBDofdm(k,:) = CRBD(D,cno(k),F2ofdm);
   CRBDcdma(k,:) = CRBD(D,cno(k),F2cdma);
end



% Plots
dcno      = 10^(40/10);
dCRBDofdm  = CRBD(D,dcno,F2ofdm);
dCRBDcdma  = CRBD(D,dcno,F2cdma);


figure;
semilogy(D,dCRBDcdma);hold on;
semilogy(D,dCRBDofdm,'r');
xlabel 'Distance (m)';
ylabel 'CRB(D)'
title(['CRB(D) @ CN0 : ',num2str(log10(dcno)*10),' dB']);

