function [xy, flag_save, break_counter ] = doAML( init_xy, xr, bs_all, var_all, ri, li, Ndim, IT_MAX, simid, break_counter, break_val, record_video, plotscenario, animation_delay,vobj)
%DOAML solves an approximate maximum likelihood system
    %   Detailed explanation goes here
    
    % Compute constans
    xi = bs_all(:,1);
    yi = bs_all(:,2);
    ki = sum([xi yi].^2,2);
    sl = var_all;
    D  = sl.*ri(:).*(ri(:)+li(:)); % in book, var is also multiplied
    
    % M
    xy = init_xy';
    quadfnc = @(a,b,c) [-b + sqrt(b^2-4*a*c)/2; -b - sqrt(b^2-4*a*c)/2];
    geodist = @(x,y)   sqrt(x.^2+y.^2);
    
    % Sanity check
    assert(ki(1) == (xi(1)^2+yi(1)^2));
    
    % LOOP
    minJ          = 0;
    est           = zeros(IT_MAX,Ndim);
    idx_iteration = 0;
    xy_p          = [inf; inf];
    flag_save     = 0;
    while idx_iteration < IT_MAX
        % Count
        idx_iteration = idx_iteration +1;
        
        % to ease with reading
        x = xy(1);
        y = xy(2);
        
        % Plot estimate
        if plotscenario == 1
            hold on;
            plot(x,y,'ok','MarkerSize',8);
        end
        % build s
        s = x.^2+y^2;
        
        % build g (changes)
        gi = (x-xi)./D; % D does not change. Look up.
        hi = (y-yi)./D; % D does not change. Look up.
        
        % Build A
        A = [sum(gi.*xi) sum(gi.*yi);...
            sum(hi.*xi) sum(hi.*yi)];
        
        % Build b
        b = [sum( gi.*( s+ki-(li.^2) ) );...
             sum( hi.*( s+ki-(li.^2) ) )];
        
        % update x, inv(A)*B is eq to A\B, see help slash;
        est(idx_iteration,:) = (A'*A)\A'*(b);
        est(idx_iteration,:) = est(idx_iteration,:).*1/2;
        xy                   = est(idx_iteration,:)';
        
        xsol = quadfnc(1,0,xy(2)^2);
        ysol = quadfnc(1,0,xy(1)^2);
        
        if all(~isreal(xsol)) && all(~isreal(ysol)) || all(xsol < 0) && all(ysol < 0)
            xy(1) = abs(ysol(1));
            xy(2) = abs(xsol(1));
        elseif all(xsol > 0) && all(ysol > 0)
            r = geodist(xy(1)-xi,xy(2)-yi);
            J(idx_iteration,:) = [li - r]'*diag(var_all)*[li-r];
            minJ = 1;
        end
        
        r = geodist(xy(1)-xi,xy(2)-yi);
        J(idx_iteration,:) = (li - r)'/diag(var_all)*(li-r);

        % Check error and break if conditions met
        ermse = sqrt(mean(abs(xy - xy_p).^2));
        if all(ermse <= break_val)
            break_counter = break_counter +1;
            flag_save = 1;
            break;
        elseif ermse > 100 && idx_iteration > 1 || isnan(ermse)
            flag_save = 0;
            break;
        end
        xy_p = xy;
        

        if record_video == 1
            title(['Run ',num2str(simid)]);
            captureframe(vobj, gcf);
        else
            if plotscenario == 1
                title(['Run ',num2str(simid)]);
                pause(animation_delay);
            end
        end
    end
    
    ermse     = rmse(xr,xy');
    
    if  ~isnan(J(1)) && (ermse > 10 || flag_save == 0 )
        [~,idx]   = min(J);
        xy        =  est(idx,:);
        flag_save = 1;        
        break_counter = break_counter +1;
%         plot(xy(1),xy(2),'g*','MarkerSize',10);
%         pause(animation_delay);
        xy = xy';
    end
    if any(isnan(xy))
        flag_save=0;
    end
    % Compute J
    %                     T = li/3e8;
    %                     J = (T - r/3e8)'/Q*(T-r/3e8);
    
end

