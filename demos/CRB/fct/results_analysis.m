close all;
clear all;

b_i  = 1;
g_i  = 2;
ac_i = 3;
wc_i = 4;

wid    = 10;
len    = 10;
steps  = 10000;
walks  = 1;
run    = 1;
skip_plot = 1;

N = 81;
% dirpath = 'storage/25x25/';%'\\intra.tut.fi\home\figueire\My Documents\MATLAB\projects\crlb\storage\t_25x25\';
basepath = '\\intra.tut.fi\home\figueire\My Documents\MATLAB\warehouse\';%'D:\warehouse\aml_grid\';%'\\intra.tut.fi\home\figueire\My Documents\MATLAB\warehouse\';%'D:\warehouse\aml_grid\';%'\\intra.tut.fi\home\figueire\My Documents\MATLAB\warehouse\25x25\';
basepath = './results_bw_final/rerun_';
dirpath = [basepath,num2str(wid),'x',num2str(len),'\'];

if ~skip_plot
    hOut = inout.output;
    hOut.nfigure(N*2,1);
end

rmse_aml = zeros(N,run);
rmse_lsq = zeros(N,run);
rmse_nls = zeros(N,run);

for r=1:run
    k=0;
    for selrun = 1:2:N*2;
        k= k+1;
        load([dirpath,'sim_output_',num2str(k),'_for_',num2str(walks),'_',num2str(wid),'x',num2str(len),'_',num2str(r),'.mat'])
        
        %     ls = reshape(abs(lsq_estimates),steps*walks,1);
        %     am = reshape(abs(aml_estimates),steps*walks,1);
        
        nbEm = tech{k};
        ls   = lsq_estimates(:);
        am   = aml_estimates(:);
        nl   = nlsq_estimates(:);
        
        maml = nanmean(am(:));
        mlsq = nanmean(ls(:));
        mnls = nanmean(nl(:));
        stda = nanstd(am(:));
        stdl = nanstd(ls(:));
        stdn = nanstd(nl(:));
        
        rmse_aml(k,r) = maml;%mean(am(~isnan(am)));
        rmse_lsq(k,r) = mlsq;%mean(ls(~isnan(ls)));
        rmse_nls(k,r) = mnls;%mean(nl(~isnan(nl)));
        
        
        if ~skip_plot
            plot(hOut.hAxis(selrun),am(:),'o','linewidth',2,'MarkerSize',4)
            plot(hOut.hAxis(selrun),ls(:),'.','linewidth',2,'MarkerSize',4)
            xlim(hOut.hAxis(selrun),[0 walks*steps]);
            title(hOut.hAxis(selrun),['Case ',num2str(k),': B: ', num2str(nbEm(b_i)),' G: ', num2str(nbEm(g_i)),' AC: ', num2str(nbEm(ac_i)),' WC: ', num2str(nbEm(wc_i)),' , Mean (AML|LSQ) = ',num2str(maml,'%2.2f'),' | ',num2str(mlsq,'%2.2f')])
            
            legend(hOut.hAxis(selrun),'AML','LSQ');
            xlabel(hOut.hAxis(selrun),'Steps');
            ylabel(hOut.hAxis(selrun),'RMSE');
        end
        
        % trim ls
        e_ls = ls(:);
        [ecdf_ls,acdf_ls] = math.cdf(e_ls);
        
        % fix aml (if need be)
        e_am = am(:);
        [ecdf_am,acdf_am] = math.cdf(e_am);
        
        if ~skip_plot
            % cdf plots
            plot(hOut.hAxis(selrun+1),acdf_am,ecdf_am,'o-','linewidth',2)
            plot(hOut.hAxis(selrun+1),acdf_ls,ecdf_ls,'x-','linewidth',2)
            legend(hOut.hAxis(selrun+1),'AML','LSQ');
            xlim(hOut.hAxis(selrun+1),[0 5]);
            ylim(hOut.hAxis(selrun+1),[0 1]);
            ylabel(hOut.hAxis(selrun+1),'CDF');
            xlabel(hOut.hAxis(selrun+1),'distance error (m)');
            
            title(hOut.hAxis(selrun+1),['Case ',num2str(k),': B: ', num2str(nbEm(b_i)),' G: ', num2str(nbEm(g_i)),' AC: ', num2str(nbEm(ac_i)),' WC: ', num2str(nbEm(wc_i)),' , Mean (AML|LSQ) = ',num2str(maml,'%2.2f'),' | ',num2str(mlsq,'%2.2f')]);
        end
        % histograms
%             figure
%             hist(e_am(~isnan(e_am)),100)
%             hold on
%             hist(e_ls(~isnan(e_ls)),100)
%             xlim([0 5])
        
    end
    
end
% hOut.print('meeting_prep')
m_rmse_aml = nanmean(rmse_aml,2);
m_rmse_lsq = nanmean(rmse_lsq,2);
m_rmse_nls = nanmean(rmse_nls,2);

%%
% k=0;
% em_cases = tech_sel{1};
xlabels{1} = 'b,g,ac,wc';
for k=1:N
    xlabels{k+1} =  [num2str(tech{k},'%2d(-) ')];
    idx=strfind(xlabels{k+1},'-');
    xlabels{k+1}(idx)=['b','g','a','w'];
end


figure;
bar(m_rmse_aml)
set(gca,'XTick',[-1, 1:N])
set(gca,'XTickLabel',xlabels)
title('RMSE with AML')
h=gca;
set(h,'position',[0.13 0.35 0.775 0.55])
rot=90;
tools.rotateticklabel(h,rot);


m_rmse_lsq(m_rmse_lsq>wid*len)=NaN;
figure;
bar(m_rmse_lsq)
set(gca,'XTick',[-1, 1:N])
set(gca,'XTickLabel',xlabels)
title('RMSE with LSQ')
h=gca;
set(h,'position',[0.13 0.35 0.775 0.55])
rot=90;
tools.rotateticklabel(h,rot);

figure;
bar(m_rmse_nls)
set(gca,'XTick',[-1, 1:N])
set(gca,'XTickLabel',xlabels)
title('RMSE with NLSQ')
h=gca;
set(h,'position',[0.13 0.35 0.775 0.55])
rot=90;
tools.rotateticklabel(h,rot);

%% Table plots
ab_cases = [ 0 3 5 10];
for ab_idx=1:length(ab_cases)
    try
        idx_a=1;
        idx_b=2;
        idx_c=3;
        idx_d=4;
        
        fix_a=ab_cases(ab_idx);
        fix_b=ab_cases(ab_idx);
        tech_interfaces = [{'802.11b'},{'802.11g'},{'802.11ac'},{'wcdma'}];
        [tab_aml,tab_lsq,tab_nls,xx,yy] = output_xls_result_matrix( idx_a,idx_b,fix_a,fix_b,rmse_aml,rmse_lsq,rmse_nls,tech,tech_interfaces,['rmse_val_',num2str(wid),'x',num2str(len),'_matrix_a',num2str(fix_a),'_b',num2str(fix_b),'.xlsx']);
    
    catch
        continue;    
    end
    
    hOut = inout.output;
    hOut.nfigure(1,1);
    hold on;
    fidx=1;
    try
        plot(hOut.hAxis(fidx),yy,tab_aml(:,1),'--ob','linewidth',2)
        plot(hOut.hAxis(fidx),yy,tab_aml(:,2),'--sk','linewidth',2)
        plot(hOut.hAxis(fidx),yy,tab_aml(:,3),'--^c','linewidth',2)
        plot(hOut.hAxis(fidx),yy,tab_aml(:,4),'--dg','linewidth',2)
        plot(hOut.hAxis(fidx),yy,tab_nls(:,1),'-ob','linewidth',2)
        plot(hOut.hAxis(fidx),yy,tab_nls(:,2),'-sk','linewidth',2)
        plot(hOut.hAxis(fidx),yy,tab_nls(:,3),'-^c','linewidth',2)
        plot(hOut.hAxis(fidx),yy,tab_nls(:,4),'-dg','linewidth',2)
    catch
    end
%     try
%         legend(hOut.hAxis(fidx),['AML:',tech_interfaces{idx_d}, ' #',num2str(xx(1))],['AML:',tech_interfaces{idx_d}, ' #',num2str(xx(2))],['AML:',tech_interfaces{idx_d}, ' #',num2str(xx(3))],['AML:',tech_interfaces{idx_d}, ' #',num2str(xx(4))],['NLS:',tech_interfaces{idx_d}, ' #',num2str(xx(1))],['NLS:',tech_interfaces{idx_d}, ' #',num2str(xx(2))],['NLS:',tech_interfaces{idx_d}, ' #',num2str(xx(3))],['NLS:',tech_interfaces{idx_d}, ' #',num2str(xx(4))])
legend(hOut.hAxis(fidx), ...
                        ['AML:',tech_interfaces{idx_d}, ' #',num2str(xx(1))], ...
                        ['AML:',tech_interfaces{idx_d}, ' #',num2str(xx(2))], ...
                        ['AML:',tech_interfaces{idx_d}, ' #',num2str(xx(3))], ...
                        ['NLS:',tech_interfaces{idx_d}, ' #',num2str(xx(1))], ...
                        ['NLS:',tech_interfaces{idx_d}, ' #',num2str(xx(2))], ...
                        ['NLS:',tech_interfaces{idx_d}, ' #',num2str(xx(3))]);
%     catch
%     end
    xlabel(hOut.hAxis(fidx),['# of ',tech_interfaces{idx_c}, ' devices'])
    ylabel(hOut.hAxis(fidx),'RMSE (m)')
    title(hOut.hAxis(fidx),['Area of ',num2str(wid),' by ',num2str(len),' m (Avg. ',num2str(run),' scn.) (a)=',num2str(fix_a),' (b)=',num2str(fix_b)])
    
    
%     fidx=2;
%     try
%         plot(hOut.hAxis(fidx),xx,tab_aml(1,:),'--ob','linewidth',2)
%         plot(hOut.hAxis(fidx),xx,tab_aml(2,:),'--sk','linewidth',2)
%         plot(hOut.hAxis(fidx),xx,tab_aml(3,:),'--^c','linewidth',2)
%         plot(hOut.hAxis(fidx),xx,tab_aml(4,:),'--dg','linewidth',2)
%     catch
%     end
%     try
%         legend(hOut.hAxis(fidx),[tech_interfaces{idx_c}, ' #',num2str(yy(1))],[ tech_interfaces{idx_c}, ' #',...
%             num2str(yy(2))],[tech_interfaces{idx_c}, ' #',num2str(yy(3))],[tech_interfaces{idx_c},...
%             ' #',num2str(yy(4))])
%     catch
%     end
%     xlabel(hOut.hAxis(fidx),['# of ',tech_interfaces{idx_d}, ' devices'])
%     ylabel(hOut.hAxis(fidx),'RMSE (m)')
%     title(hOut.hAxis(fidx),['Area of ',num2str(wid),' by ',num2str(len),' m (Avg. ',num2str(run),' scn.) (a)=',num2str(fix_a),' (b)=',num2str(fix_b)])
%     
    hOut.print(['rmse_with_NLS_',num2str(wid),'x',num2str(len),'_',num2str(fix_a),'_',num2str(fix_b)]);
end

%% % create xls files
xlswrite(['rmse_val_',num2str(wid),'x',num2str(len),'.xlsx'],xlabels','rmse',['A1:A',num2str(N)])
xlswrite(['rmse_val_',num2str(wid),'x',num2str(len),'.xlsx'],rmse_aml,'rmse',['B2:B',num2str(N)])
xlswrite(['rmse_val_',num2str(wid),'x',num2str(len),'.xlsx'],rmse_lsq,'rmse',['C2:C',num2str(N)])
xlswrite(['rmse_val_',num2str(wid),'x',num2str(len),'.xlsx'],rmse_nls,'rmse',['D2:D',num2str(N)])
xlswrite(['rmse_val_',num2str(wid),'x',num2str(len),'.xlsx'],{'AML'},'rmse','B1:B1')
xlswrite(['rmse_val_',num2str(wid),'x',num2str(len),'.xlsx'],{'LSQ'},'rmse','C1:C1')
xlswrite(['rmse_val_',num2str(wid),'x',num2str(len),'.xlsx'],{'NLSQ'},'rmse','D1:D1')
return;


%% density plots

room_dim   = [5];
runs  = 1;
for nbDev = [0 2 5 10];
fix_b = nbDev;
fix_g = nbDev;
fix_a = nbDev;
fix_w = nbDev;

for k=1:length(tech)
    if all(tech{k} == [fix_b fix_g fix_a fix_w])
        break;
    end
end
idx=1;
clear maml
clear mlsq
clear mnls
% Load that for
wid     = room_dim(idx);
len     = room_dim(idx);
dirpath = [basepath,num2str(wid),'x',num2str(len),'/'];
for run=1:runs
    load([dirpath,'sim_output_',num2str(k),'_for_',num2str(walks),'_',num2str(wid),'x',num2str(len),'_',num2str(run),'.mat'])
    ls   = lsq_estimates(:);
    am   = aml_estimates(:);
    nl   = nlsq_estimates(:);
    maml(run) = nanmean(am(:));
    mlsq(run) = nanmean(ls(:));
    mnls(run) = nanmean(nl(:));
end
av_aml(idx)=mean(maml);
av_lsq(idx)=mean(mlsq);
av_nls(idx)=mean(mnls);

% idx = 2;
% clear maml
% clear mlsq
% clear mnls
% wid     = room_dim(idx);
% len     = room_dim(idx);
% dirpath = [basepath,num2str(wid),'x',num2str(len),'/'];
% for run=1:runs
%     load([dirpath,'sim_output_',num2str(k),'_for_',num2str(walks),'_',num2str(wid),'x',num2str(len),'_',num2str(run),'.mat'])
%     ls   = lsq_estimates(:);
%     am   = aml_estimates(:);
%     nl   = nlsq_estimates(:);
%     maml(run) = nanmean(am(:));
%     mlsq(run) = nanmean(ls(:));
%     mnls(run) = nanmean(nl(:));
% end
% av_aml(idx)=mean(maml);
% av_lsq(idx)=mean(mlsq);
% av_nls(idx)=mean(mnls);
% 
% idx=3;
% clear maml
% clear mlsq
% clear mnls
% wid     = room_dim(idx);
% len     = room_dim(idx);
% dirpath = [basepath,num2str(wid),'x',num2str(len),'\'];
% for run=1:runs
%     load([dirpath,'sim_output_',num2str(k),'_for_',num2str(walks),'_',num2str(wid),'x',num2str(len),'_',num2str(run),'.mat'])
%     ls   = lsq_estimates(:);
%     am   = aml_estimates(:);
%     nl   = nlsq_estimates(:);
%     maml(run) = nanmean(am(:));
%     mlsq(run) = nanmean(ls(:));
%     mnls(run) = nanmean(nl(:));
% end
% av_aml(idx)=mean(maml);
% av_lsq(idx)=mean(mlsq);
% av_nls(idx)=mean(mnls);

hbar = inout.output;
hbar.nfigure(1,1);
hold on;
% bar(dim,av_lsq,'b')
bar(hbar.hAxis,room_dim,av_nls,'k')
bar(hbar.hAxis,room_dim,av_aml,'g')
% hl=legend('lsq','nlsq','aml');
hl=legend(hbar.hAxis,'nlsq','aml','location','northwest');
set(hl,'interpreter','latex');
% xlabel('Density ($m$)','interpreter','latex');
ylabel(hbar.hAxis,'Mean distance error (m)','interpreter','latex');
clear xlabels;
for k=1:length(room_dim)
    val=room_dim(k);
    xlabels{k}=[num2str([val val],'%2dx%2d m')];
end
ylim(hbar.hAxis,[0 120]);
set(hbar.hAxis,'XTick',room_dim)
set(hbar.hAxis,'XTickLabel',xlabels)
title(hbar.hAxis,['Mean error with floor density assuming (b,g,a,w) = [',num2str([fix_b fix_g fix_a fix_w]),']'],'interpreter','latex');
hbar.print(['Density_(',num2str(nbDev),')']);
end
