function start = saveRMSE(obj,epos,ermse,nStep,estimator_id,nWalk,technology_id,N_Devices,estimate,init,ign)
    
    if ~ign
        start           = estimate;
    else
        start           = init;
        return;
    end
    
    epos=epos(:)';
    
    % N_Emitters_Per_Tech contains N_b, N_g, N_ac, N_wcdma
    obj.storage.ermse(nStep,estimator_id,nWalk,N_Devices(1),N_Devices(2),N_Devices(3),N_Devices(4),technology_id) = ermse;
    obj.storage.estimates(nStep,:,estimator_id,nWalk,N_Devices(1),N_Devices(2),N_Devices(3),N_Devices(4),technology_id) = epos;
    
end