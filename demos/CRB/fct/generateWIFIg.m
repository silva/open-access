% parameters for Fast WIFI 802.11g

% CDMA
ofdm_input    = 1;                    % Activate (1) OFDM signal
ofdm_fref     = 10e6;                 % OFDM: Signal reference frequency
ofdm_Ntotal   = 64;                   % Number of carriers
ofdm_Ndata    = 48; % 4,8, 16, 32     % Guard interval factor NFFT/gi
ofdm_Npilot   = 8;
ofdm_Nsub     = ofdm_Ndata + ofdm_Npilot;
ofdm_bw       = 20e6;                 % OFDM bandwith
ofdm_ppower   = 1;                    % Pilot power boost
ofdm_mod      = 16;                   % Modulation
ofdm_mod_tp   = 1;                    % Modulation type (forced to QAM)

ofdm_deltaF   = ofdm_bw/ofdm_Ntotal;
ofdm_Tfft     = 1/ofdm_deltaF;
ofdm_Tgi      = ofdm_Tfft/4;
ofdm_sybtime  = ofdm_Tfft+ofdm_Tgi;

ofdm_symbols  = fix(simTime/ofdm_sybtime); % Number of OFDM symbols
ofdm_theocycl = ofdm_sybtime;         % Theorectical period of cyc. frequencies
ofdm_stem     = 1;                    % Display cyc. frequencies



inputs = [                              %%% Matrix with (#DATA SYMBOLS, OVERSAMPLING)
    ofdm_symbols, ofdm_Ntotal,...
    ofdm_Ndata, ...
    ofdm_deltaF, ofdm_ppower,...
    ofdm_Nsub, ...
    ofdm_mod,...
    ofdm_mod_tp,...
    ofdm_sybtime,...
    ofdm_theocycl,... %KEEP AS LAST ONE
    ];


ofdm   = genOFMD(inputs(1,1),... % Number of symbols
    inputs(1,2),...        % Total number of carriers
    inputs(1,3),...        % Number of DATA carriers
    inputs(1,6),...        % Number of Pilot and Data carriers
    inputs(1,4),...        % Carriers separation (1/dF)
    inputs(1,5),...        % Pilot power
    inputs(1,7),...        % Modulation
    inputs(1,8),...        % Modulation selection QAM 1 PSK 0
    fs);                   % Sampling frequency

% ofdm = ofdm./max(ofdm);
% P = sum(abs(ofdm).^2)*Ts;
% ofdm = ofdm/P;