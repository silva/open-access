% Tampere University of Technology
%
%   This script implements a simple MLA approach to solve a TOA problem for
%   the case when OFDM and CDMA emmitters are present in the vicinity.
%   These two stations are characterised by different variances.
%
%   Pedro Silva, 2014
%
%   TODO
%   I want candle plots done for this
%
function callcpe_crbd(nbsim,variance_s1,variance_s2,clk_bias_s1, clk_bias_s2, interface_name, s1_waveform, s2_waveform,s1_f,s2_f,s1_type,s2_type,offset,s1_nf,s1_bw,s2_nf,s2_bw)
    
    % Bound to be used
    c         = 3e8;            % speed of light (m/s)
    go        = 2;              % delta coefficient, 2 in free space
    ko        = 1;              % constant parameter (?)
    CRBD    = @(d,icno,f) c^2.*d.^(go+2) ./ (( 2.*ko.^2.*icno.*( (go.^2.*c.^2)./4 + d.^2.*f )));
    
    % Loop parameters
    number_simulations   = nbsim;     % (#) number of simulations
    Ndim                 = 2;       % (#) 2D or 3D dimensions
    IT_MAX               = 20;      % (#) Max number of iterations
    break_val            = 1e-4;    % (#) Breaking condition
    
    % Operational parameters
    
    % Animation parameters
    record_video         = 0;       % (?) 1/0 to/skip video recording
    plotscenario         = 0;       % (?) animates and plots the scenario
    animation_delay      = 0.5;     % (s) increase to slow down plotting in each run
    
    
    % Saving parameters
    save_data            = 0;       % (?) 1/0 to/skip sav
    number_data_store    = 6;       % (#) allocation for saving matrix
    savetofolder         = 'resultsWithVar/';
    mkdir(savetofolder);
    
    
    % Plot parameters
    marker_value_user    = 1;       % Marker for user
    marker_value_cdma    = 2;       % Marker for cdma
    marker_value_ofdm    = 3;       % Marker for ofdm
    idx_figure           = 0;
    
    % Grid parameters
    grid_size            = 30;      % (m^2) size of grid (NxN)
    nb_s1_bs           = 0:5;     % ([])  vector with number of CDMA to test
    nb_s2_bs           = 0:5;     % ([])  vector with number of OFDM to test
    
    
    % Data storage
    mean_rmse            = NaN(numel(variance_s1),numel(variance_s2),numel(nb_s1_bs),numel(nb_s2_bs));
    
   
    % Idx helpers
    for_each_var_s1 = 0;
    for_each_var_s2 = 0;
    
    % Generate data

    
    % output helper
    disp('Starting AML sim');
    while for_each_var_s1 < length(variance_s1) && for_each_var_s2 < length(variance_s2)
        for_each_var_s1 = for_each_var_s1 + 1;
        for_each_var_s2 = for_each_var_s2 + 1;
        
        for for_each_nb_s1 = 1:length(nb_s1_bs)
            for for_each_nb_s2 = 1:length(nb_s2_bs)
                
                % create file here
                filename = [savetofolder,'run_vs1_',num2str(variance_s1(for_each_var_s1)),...
                    '_vs2_',num2str(variance_s2(for_each_var_s2)),...
                    '_Ns1_',num2str(nb_s1_bs(for_each_nb_s1)),...
                    '_Ns2_',num2str(nb_s2_bs(for_each_nb_s2)),'.mat'];
                
                % preparing loop variables
                N_s1  = nb_s1_bs(for_each_nb_s1); % CDMA number of base stations
                N_s2  = nb_s2_bs(for_each_nb_s2); % OFDM number of base stations
                bs_s1 = zeros(N_s1,Ndim);           % CDMA base stations positions
                bs_s2 = zeros(N_s2,Ndim);           % OFDM base stations positions
                
                % sanity check
                if (N_s1+N_s2) < 2
                    continue;
                end
                
                % SIM LOOP
                skip_save     = 0;
                break_counter = 0;
                saveid        = 0;
                storeData     = zeros(number_simulations,number_data_store);
                for simid=1:number_simulations
                    
                    % Variance
                    var_s1 = variance_s1(for_each_var_s1);
                    var_s2 = variance_s2(for_each_var_s2);
                    
                    % Create grid
%                     grid = zeros(grid_size);
                    
                    % create vector for true ranges
                    R_s1 = zeros(N_s1,1);
                    l_s1 = zeros(N_s1,1);
                    R_s2 = zeros(N_s2,1);
                    l_s2 = zeros(N_s2,1);
                    
                    % Get random position for user
                    userxy(1,:) = drawnumber( 1,grid_size, Ndim );%unidrnd(grid_size,Ndim,1);
                    
                    minx = saturateDown(userxy(1)-offset,1);
                    maxx = saturateUp(userxy(1)+offset,grid_size);
                    
                    miny = saturateDown(userxy(2)-offset,1);
                    maxy = saturateUp(userxy(2)+offset,grid_size);
                    
                    
                    % CDMA: Generate emitter positions
                    for k=1:N_s1
                        bs_s1(k,1) = drawnumber( minx,maxx, 1 );%randi([minx maxx],1,1);
                        bs_s1(k,2) = drawnumber( miny,maxy, 1 );%randi([miny maxy],1,1);
                        R_s1(k,1)  = norm(userxy-bs_s1(k,:));
                    end
                    
                    
                    % OFDM: Generate positions and ranges
                    % inside the grid space
                    for k=1:N_s2
                        bs_s2(k,1) = drawnumber( minx,maxx, 1 );%randi([minx maxx],1,1);
                        bs_s2(k,2) = drawnumber( miny,maxy, 1 );%randi([miny maxy],1,1);
                        R_s2(k,1)  = norm(userxy-bs_s2(k,:));
                    end
                    
                    % Generate CDMA and OFDM variance by using distance
                    % information and CN0
                    
                    % GENERATES ranging information
                    for k=1:N_s1
                        d = R_s1(k,1);
                        rss_s1 = RadioSpectrum.Pathloss.dist2rss(d,s1_f,s1_type); % f in MHz
                        np_s1  = RadioSpectrum.Pathloss.noisePower(s1_nf,s1_bw,0); %0 dB gain
                        cno_s1 = rss_s1 - np_s1;
                        
                        var_s1 = abs(CRBD(d,cno_s1,s1_waveform));
                        l_s1(k,1)  = d + clk_bias_s1 + normrnd(0,sqrt(var_s1));
                        if l_s1(k,1) < 0
                            l_s1(k,1) = l_s1(k,1)*-1;
                        end
                    end
                    
                    for k=1:N_s2
                        d = R_s2(k,1);
                        rss_s2 = RadioSpectrum.Pathloss.dist2rss(d,s2_f,s2_type); % f in MHz
                        np_s2  = RadioSpectrum.Pathloss.noisePower(s2_nf,s2_bw,0); %0 dB gain
                        cno_s2 = rss_s2 - np_s2;
                        var_s2 = abs(CRBD(R_s2(k,1),cno_s2,s2_waveform));
                        l_s2(k,1)  = R_s2(k,1) + clk_bias_s2 + normrnd(0,sqrt(var_s2));
                        if l_s2(k,1) < 0
                            l_s2(k,1) = l_s2(k,1)*-1;
                        end
                    end
                    
                    
                    % Create global vectors (CDMA first then OFDM)
                    bs_all      = [ bs_s1(:,1) bs_s1(:,2);...
                                    bs_s2(:,1) bs_s2(:,2)];
                    ri          = [ R_s1(:); R_s2(:)];
                    li          = [ l_s1(:); l_s2(:)];
                    var_all     = ones(size(bs_all,1),1);
                    init_userxy = mean(bs_all);
                    
                    % Run estimation algorithm
                    [xy, flag_save, break_counter ] = doAML(init_userxy, userxy, bs_all, var_all, ri, li, Ndim, IT_MAX, simid, break_counter, break_val, record_video, plotscenario, animation_delay);
                    
                    % RMSE
                    try
                        if ~isrow(xy)
                            xy = xy';
                        end
                        xe    = xy;
                        xr    = userxy;
                        ermse = rmse(xr,xe);
                        if ermse > 100
                            ermse = 0;
                            flag_save = 0;
                        end
                    catch
                        xe    = [NaN NaN];
                        xr    = userxy;
                        ermse = NaN;
                    end
                    
                    % Data to save
                    storeData(simid,:) = [xr, xe, ermse, flag_save];
                    
                end %END of simulation
                
                % compute and add mean
                idx_figure = idx_figure+1;
                allvalid   = storeData(:,end)==1;
                mrse       = mean(storeData(allvalid,end-1));
                if isnan(mrse)
                    continue;
                end
                mean_rmse(for_each_var_s1,for_each_var_s2,for_each_nb_s1,for_each_nb_s2) = mrse;
                if save_data == 1 && skip_save == 0;
                    save(filename,'storeData');
                end
                %disp('done for now');
                %disp(['Break % : ',num2str(break_counter/number_simulations*100)])
            end
        end
        % Cleanup
        if save_data == 1;
            save([savetofolder,'mean_rmse'],'mean_rmse');
        end
        
        
        %results with Var
        varCDMAidx = 1;
        varOFDMidx = 1;
        
        result_matrix  = NaN(nb_s1_bs(end)+1,nb_s2_bs(end)+1);
        % varCDMA, varOFDM, ctrCDMA, ctrOFDM
        fprintf('S1 : S2 | RMSE \n')
        for idxO = 1:length(nb_s2_bs)
            for idxC = 1:length(nb_s1_bs)
                if nb_s1_bs(idxC)+nb_s2_bs(idxO) < 2, continue, end;
                if result_matrix(nb_s1_bs(idxC)+1,nb_s2_bs(idxO)+1) >= 1, continue, end;
                fprintf('%d : %d | %2.2f\n',nb_s1_bs(idxC), nb_s2_bs(idxO), mean_rmse(varCDMAidx,varOFDMidx,idxC,idxO))
                result_matrix(nb_s1_bs(idxC)+1,nb_s2_bs(idxO)+1) = mean_rmse(varCDMAidx,varOFDMidx,idxC,idxO);
            end
            disp('-');
        end
        
        
        
        % do a plot of this with scatter
        x            = repmat(nb_s1_bs,size(result_matrix,1),1);
        y            = repmat(nb_s2_bs',1,size(result_matrix,1));
        dump_to_file = [[NaN;nb_s1_bs'], [nb_s2_bs;result_matrix]];
        dump_to_file = [dump_to_file;dump_to_file(end,:)];
        dump_to_file(end,:) = 0;
        dump_to_file(end,1) = var_s1;
        dump_to_file(end,2) = var_s2;
        dump_to_file(end,3) = nbsim;
        
        
        xlswrite([savetofolder,'results_AML_',interface_name,'_',num2str(nbsim),'_onradiusof_',num2str(offset)],dump_to_file);
    end
end

function in = saturateUp(in,sat)
    if in > sat
        in = sat;
    end
end

function in = saturateDown(in,sat)
    if in < sat
        in = sat;
    end
end

%EOF