function  compute_estimates( hMatfile, pointer, track, CN0, meas, sel_tech, emmitters, nbEm, nbSteps, start, tol, maxit, cno_min, hOut,sim_outfile,room_wid,room_len)
    % hMatfile - matfile object
    % walk_num - number of walk
    % this function computes for one walk the AML and LSQ estimates
    aml_estimate = NaN(nbSteps,1);
    lsq_estimate = NaN(nbSteps,1);
    nlsq_estimate = NaN(nbSteps,1);
    %take into account walk_num
    len  = nbEm;
    head = 1;
    V    = eye(nbEm);
    
    
    
    E_in  = emmitters(sel_tech,:); % indexes are valid for these ones as well
    init  = mean(E_in(:,1:2));
    
    start_aml  = [init];
    start_lsq  = [init];
    start_nlsq = [init];
    
    for step=1:nbSteps
        % retrieves information for the Nth walk
        pos = track(step,:);
        mes = meas(head:head+len-1,1);
        
        %sanity checking
        assert(all(mes>=0));

        L_in  = mes(sel_tech);
        
        
%         % call estimators
%         %nlsq id
%         ign = 0;
%         [epos,ermse,~,ign]  = nlsq( start_nlsq, pos, E_in(:,1:2), L_in, tol, maxit);
%         
%         if ~ign
%             start_nlsq = epos;
%             nlsq_estimate(step,:) = ermse;
%         else
%             start_nlsq = [init,0];
%         end
%         if isnan(ermse)
%             start_nlsq = [init,0];
%         end
%         
%         %lsq id
%         ign = 0;
%         [epos,ermse,~,ign] = lsq( start_lsq, pos, E_in(:,1:2), L_in, tol, maxit);
%         if ~ign
%             start_lsq            = epos;
%             lsq_estimate(step,:) = ermse;
%         else
%             start_lsq = [init,0];
%         end
%         if isnan(ermse)
%             start_lsq = [init,0];
%         end
%         
        %aml id
        ign = 0;
        [epos,ermse,~,ign] = aml( start_aml, pos, E_in(:,1:2), L_in, 1, tol, maxit,[]);
%         [epos,ermse,~,ign] = aml_grid( start_aml, pos, E_in(:,1:2), L_in, 1, tol, maxit,room_wid,room_len);
        if ~ign
            start_aml            = epos;
            aml_estimate(step,:) = ermse;
        else
            start_aml = init;
        end
        if isnan(ermse)
            start_aml = init;
        end
        ign = 0;
        hOut.animateEstimation(step,track, E_in(:,1:2),epos,V,50,50)
        head = head + len;
    end
    
    sim_outfile.aml_estimates(pointer,1)  = aml_estimate(:);
    sim_outfile.lsq_estimates(pointer,1)  = lsq_estimate(:);
    sim_outfile.nlsq_estimates(pointer,1) = nlsq_estimate(:);
    
end

%     walk  = room.storage.walk(:,:,k);   % [Nsteps x 2]
%     wvar  = room.storage.svar(:,:,k);   % [Nemitters x Nsteps]
%     wmeas = room.storage.meas(:,:,k);   % [Nemitters x Nsteps]
%     wrag  = room.storage.ranges(:,:,k); % [Nemitters x Nsteps]
%     wcno  = room.storage.cno(:,:,k);    % [Nemitters x Nsteps]

