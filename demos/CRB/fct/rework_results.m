%%% Reads matfiles
hOut = inout.output;
hOut.nfigure(3);

nWalk        = room.storage.number_of_walks;
nStep        = room.storage.number_of_steps_per_walk;

technology_id = 1; %1:6
estimator_id = [2 3]; %2 lsq, 4 aml
N_B = 5;
N_G = 5;
N_A = 5;
N_W = 5;
walk_stat_single  = zeros(nWalk,length(estimator_id),6);
walk_stat_overall = zeros(length(estimator_id),6);


% mean per track
for k=1:nWalk
    for alg = 1:2
        for tech=1:6
            walk_stat_single(k,alg,tech) = mean(room.storage.ermse(:,estimator_id(alg),k,N_B,N_G,N_A,N_W,tech));
        end
    end
end

figure;
hold on;
for tech=1:6
    
    plot(walk_stat_single(:,1,tech))
    switch tech
        case 1,
            str_tech = 'b and g';
        case 2,
            str_tech = 'b and ac';
        case 3,
            str_tech = 'b and wcdma';
        case 4,
            str_tech = 'g and ac';
        case 5,
            str_tech = 'g and wcdma';
        case 6,
            str_tech = 'b,g,ac,wcdma';
    end
    
    title(['Mean RMS over each track for ',str_tech]);
    xlabel('Tracks');
    ylabel('ERMS (m)');
end

legend( 'b,g', 'b, ac', 'b , wc', 'g, ac','g, wc', 'all');


% mean all tracks per estimator
for alg = 1:2
    for tech=1:6
        walk_stat_overall(alg,tech) = mean(mean(room.storage.ermse(:,estimator_id(alg),:,N_B,N_G,N_A,N_W,tech)));
    end
end


figure;
hold on;
for algo=1:2
    figure
    bar(walk_stat_overall(algo,:))
    
    switch tech
        case 1,
            str_tech = 'b and g';
        case 2,
            str_tech = 'b and ac';
        case 3,
            str_tech = 'b and wcdma';
        case 4,
            str_tech = 'g and ac';
        case 5,
            str_tech = 'g and wcdma';
        case 6,
            str_tech = 'b,g,ac,wcdma';
    end
    
    switch(algo)
        case 1
            str_algo = 'LSQ: ';
        case 2
            str_algo = 'AML: ';
    end
    
    title([str_algo,'Mean RMS over each track for ',str_tech]);
    xlabel('Tech cases (see previous slide)');
    ylabel('ERMS (m)');
ylim([ 0 0.16]);
end

legend( 'b,g', 'b, ac', 'b , wc', 'g, ac','g, wc', 'all');



for tech=1:6
    plot(hOut.hAxis(1),1:nStep*nWalk,reshape(room.storage.ermse(:,estimator_id(2),:,N_B,N_G,N_A,N_W,tech),nWalk*nStep,1));    
    series(:,tech) = reshape(room.storage.ermse(:,estimator_id(2),:,N_B,N_G,N_A,N_W,tech),nWalk*nStep,1);
end


hOut.candle('Emitters','L (m)', series(:,1),'802.11b', series(:,2), '802.11g', series(:,3), '802.11ac', series(:,4), 'UMTS',series(:,5),'Tech 5',series(:,6),'Tech 6');


for tech=1:6
%     gca(hOut.hAxis(3));
figure
   cdfplot(series(:,3)) 
   switch tech
        case 1,
            str_tech = 'b and g';
        case 2,
            str_tech = 'b and ac';
        case 3,
            str_tech = 'b and wcdma';
        case 4,
            str_tech = 'g and ac';
        case 5,
            str_tech = 'g and wcdma';
        case 6,
            str_tech = 'b,g,ac,wcdma';
    end
    title(['CDF for ',str_tech]);
end



