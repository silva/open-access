function [ epos, err, V, ign] = lsq_toa( init_pos, rpos, emitter, Li, tol, maxit, max_err)
%DOLSQ solves a positioning least squares system


    if ~exist('max_err','var')
        max_err = Inf;
    end
    
    % functions
    
    
    % Compute constans
    epos  = [1;1];%init_pos(:);
%     xi    = emitter(:,1);
%     yi    = emitter(:,2);
    
    [~,idx] = min(Li);

    
    ref   = emitter(idx,:);
    emitter(ix,:) = [];
    H     = emitter(idx,:);
    
    ki    = sum(H.^2,2);
    
        cnt  = 0;
    ign  = false;
    estimate(maxit,2) = Inf;
    epos_p  = init_pos.*Inf;
    cnt = 0;
    rcvclk = 0;
    
     while cnt < maxit
        cnt = cnt+1;
        
        % radius computation
        r_ref = sum(epos.^2);
        ri    = (H(:,1)-epos(1)).^2+(H(:,2)-epos(2)).^2;
        b     = 1/2.*[ki-Li+r_ref];
        
        epos = (H'*H)\H'*b;
        
        
        err_p  = rmse(epos(:),epos_p(:));
        if err_p < tol
            break;
        end
        epos_p = epos;
        
     end
    
    err = rmse(rpos(:),epos(:));
    V   = inv(H'*H);
    ign = 0;
     
end

%EOF