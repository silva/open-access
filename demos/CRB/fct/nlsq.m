function [ epos, err, V, ign] = nlsq( init_pos, rpos, emitter, Li, tol, maxit, max_err)
    %UNTITLED5 Summary of this function goes here
    %   Detailed explanation goes here
    
    xi = emitter(:,1);
    yi = emitter(:,2);
    li = Li(:);
    
    res = @(pos) [(xi-pos(1)).^2 + (yi-pos(2)).^2 - li.^2];%-299792458.*pos(3)];
    
    [epos, Ssq, CNT, Res, XY] = LMFnlsq2(res,init_pos(1:2));
    
    err = rmse(rpos,epos(1:2));
    V = [];% Ssq'*Ssq;
    ign = (CNT > 100);
end

