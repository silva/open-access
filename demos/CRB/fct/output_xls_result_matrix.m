function [raml,rlsq,rnls,ax_x,ax_y] = output_xls_result_matrix( idx_a,idx_b,fix_a,fix_b,rmse_aml,rmse_lsq,rmse_nls,tech,tech_intf,fname)
    %% Table plots

    
    for k=1:length(tech);
        em_list(k,:) = tech{k};
    end
    
    
    m_rmse_aml = nanmean(rmse_aml,2);
    m_rmse_lsq = nanmean(rmse_lsq,2);
    m_rmse_nls = nanmean(rmse_nls,2);
    
    
    set_a = em_list(:,idx_a) == fix_a;
    set_b = em_list(:,idx_b) == fix_b;
    opts  = set_a & set_b;
    
    if ~isempty(opts)
        val_ids  = opts == 1;
        
        % retrieves tab values
        tab_val(:,1) = m_rmse_aml(val_ids);
        tab_val(:,2) = m_rmse_lsq(val_ids);
        tab_val(:,3) = m_rmse_nls(val_ids);
        
        tab_ems = em_list(val_ids,1:end);
        tab_ems(:,[idx_a idx_b]) = [];
        
    end
    
    % finally print it out
    fprintf('\nTABLE with (AML | LSQ | NLSQ) RMSE\n');
    row_change    = tab_ems(:,end)==tab_ems(1,end);
    row_change(1) = 0;
    k=0;
    disp(['Fixed ',tech_intf{idx_a},'=',num2str(fix_a),' & ',tech_intf{idx_b},'=',num2str(fix_b)]);
    int_idx = 1:length(tech_intf);
    int_idx([idx_a,idx_b]) = [];
    disp([tech_intf{int_idx(end-1)},' Vs ',tech_intf{int_idx(end)}]);
    header = [];
    row_hd = [];
    while 1
        k=k+1;
        if row_change(k) == 1
            break;
        end
        fprintf('%d\t\t\t|\t',tab_ems(k,end));
        header = [ header, tab_ems(k,end)];
    end
    
    row_change(1) = 1;
    c=1;
    l=0;
    line_header=[];
    for k=1:length(row_change)
        

        
        if row_change(k)== 1
            fprintf('\n'); %break line
            fprintf('%d\t\t|\t',tab_ems(k,end-1));
            if c>1
                if l == 1
                    xlswrite(fname,[ 0 header;tab_ems(k-1,end-1),results_aml_tab(l,:)],'aml',['A',num2str(1)]);
                    xlswrite(fname,[ 0 header;tab_ems(k-1,end-1),results_lsq_tab(l,:)],'lsq',['A',num2str(1)]);
                    xlswrite(fname,[ 0 header;tab_ems(k-1,end-1),results_nls_tab(l,:)],'nlsq',['A',num2str(1)]);
                else
                    xlswrite(fname,[tab_ems(k,end-1),results_aml_tab(l,:)],'aml',['A',num2str(l+1)]);
                    xlswrite(fname,[tab_ems(k,end-1),results_lsq_tab(l,:)],'lsq',['A',num2str(l+1)]);
                    xlswrite(fname,[tab_ems(k,end-1),results_nls_tab(l,:)],'nlsq',['A',num2str(l+1)]);
                end
            end
            line_header=[];
            l=l+1;
            c=1;
        end
        
       fprintf('%2.2f %2.2f %2.2f \t|\t',tab_val(k,1),tab_val(k,2),tab_val(k,3));
       results_aml_tab(l,c) = tab_val(k,1);
       results_lsq_tab(l,c) = tab_val(k,2);
       results_nls_tab(l,c) = tab_val(k,3);
       c=c+1;
        
    end
    
    xlswrite(fname,[tab_ems(k,end-1),results_aml_tab(l,:)],'aml',['A',num2str(l+1)]);
    xlswrite(fname,[tab_ems(k,end-1),results_lsq_tab(l,:)],'lsq',['A',num2str(l+1)]);
    xlswrite(fname,[tab_ems(k,end-1),results_nls_tab(l,:)],'nlsq',['A',num2str(l+1)]);
    
    disp('');
    ax_x=header;
    ax_y=tab_ems(1:length(header):end,end-1);
    
    
   raml = results_aml_tab;
   rlsq = results_lsq_tab;
   rnls = results_nls_tab;   
end

