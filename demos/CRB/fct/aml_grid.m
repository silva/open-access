function [ epos, err, V, ign] = aml_grid( init_pos, rpos, emitter, Li, V, tol, maxit, wid,len)
    %UNTITLED2 Summary of this function goes here
    %   Detailed explanation goes here
    % start, rpos, E_in, L_in, tol, maxit
    
    if ~exist('max_err','var')
        max_err = Inf;
    end
    
    % functions
    geodist = @(x,y)   sqrt(x.^2+y.^2);
    
    % Compute constans
    epos = init_pos(:);
    xi   = emitter(:,1);
    yi   = emitter(:,2);
    ki   = sum([xi yi].^2,2);
    
    % Initial guess
%     s = epos(1)^2+epos(2)^2;
%     M = 2*emitter;
%     b = s+ki(:)-Li(:).^2;
    %     [epos,~] = lsqr(M,b,tol,maxit);
    
    if isnan(any(epos))
        epos = init_pos;
    end
    
    kcount = 0;
    mcount = 0;
    itcount=0;
    kpos = 1:0.1:wid;
    mpos = 1:0.1:len;
    
    x    = kron(kpos,(kpos.*0+1)');
    y    = kron(mpos,(mpos.*0+1)')';
    
    for k=kpos
        kcount=kcount+1;
        for m=mpos
            mcount=mcount+1;
            itcount=itcount+1;
            ri     = geodist(k-xi,m-yi);
            J(mcount,kcount) = log((Li - ri)'*(Li-ri));
%             disp([kcount,mcount]);
%             assert(k==x(itcount) & m==y(itcount));
        end
        mcount = 0;
    end
    
    [vJ,idx] = min(J(:));
    pos  = [x(:) y(:)]; % J(:) is collumn wise!!!!
    epos = pos(idx,:); %only one, if many
    
    cnt  = 1;
    ign  = false;
    estimate = zeros(maxit+1,2);
    J        = Inf(maxit+1,1);
    
    estimate(cnt,:) = epos;
    J(cnt)          = vJ;
    
    neta = 0.74;
    conv_flag=0;
    while cnt < maxit
        cnt = cnt+1;
        
        % solve and check solution according to paper
        [epos] = checksol(epos,xi,yi,Li,ki,tol,maxit,V);
        estimate(cnt,:)     = epos;
        ri                  = geodist(epos(1)-xi,epos(2)-yi);
        J(cnt)              = log((Li - ri)'*(Li-ri));
        
        if cnt>2
            a=mean(J(1:cnt))-mean(J(1:cnt-1));
            b=neta*sqrt(1/cnt*nanvar(J(1:cnt)));
            if a < b
                conv_flag=1;
                break;
            end
        end
    end
    % Obtain the minimum J
    [vJ,idx] = min(J);
    epos     = estimate(idx,:);
    V        = [];%inv(H'*H);
    
    err = rmse(rpos(:),epos(:));
    if ~conv_flag
        ign  = true;
    end
    epos = epos(:);
end

function [A,b] = buildMatElements(epos,Li,ri,ki,xi,yi,v)
    % to ease with reading
    x = epos(1);
    y = epos(2);
    % build s
    s  = x.^2+y^2;
    D  = ri.*(ri(:)+Li(:));% ri.*(ri(:)+Li(:)); % var as identity
    gi = (x-xi)./D; % D does not change. Look up.
    hi = (y-yi)./D; % D does not change. Look up.
    
    % Build A and b
    A   = 2*[sum(gi.*xi) sum(gi.*yi);...
        sum(hi.*xi) sum(hi.*yi)];
    b   = [sum( gi.*( s + ki-(Li.^2) ) );...
        sum( hi.*( s + ki-(Li.^2) ) )];
end


function [sol,A] = checksol(sol,xi,yi,Li,ki,tol,maxit,v)
    %Case 1. The roots are real and there is only one positive root. Then
    %takes the value of this positive root and is estimated by applying (18).
    
    %Case 2. The roots are real and are both positive. Then Theta is calculated by
    %applying (18) for each root and the corresponding value of J is determined
    %by performing (13). The correct root is the one that leads to the minimum
    %value of J and the estimated (x,y) is the corresponding Theta.
    
    %Case 3. The roots are negative (or complex). In this case we take the
    %absolute value (of the real part) and process as in the Case 2.
    
    A=[];
    quadfnc = @(a,b,c) [-b + sqrt(b^2-4*a*c)/(2*a); -b - sqrt(b^2-4*a*c)/(2*a)];
    geodist = @(x,y)   sqrt(x.^2+y.^2);
    
    s     =  sol(1)^2 + sol(2)^2;
    xsol  = quadfnc(1,0,sol(2)^2 - s);
    xsign = xsol > 0;
    
    
    if ~all(isnan(xsol))
        % If only one root is positive, take it
        if  all(isreal(xsol)) &&  sum(xsign) == 1
            sol(1)   = xsol(xsign);
            
        elseif all(isreal(xsol)) && (sum(xsign) == 2 || sum(xsign)==0)
            % if negative
            xsol = abs(real(xsol));
            
            ri       = geodist(xsol(1)-xi,sol(2)-yi);
            [A,b]    = buildMatElements(xsol,Li,ri,ki,xi,yi,v);
            [temp,~] = lsqr(A,b,tol,maxit);
            xsol(1)  = temp(1);
            
            %compute J
            testx(1) = xsol(1); testx(2) = sol(2);
            ri       = geodist(testx(1)-xi,testx(2)-yi);
            J(1)     = (Li - ri)'*(Li-ri);
            
            
            testx(1) = xsol(2); testx(2) = sol(2);
            ri       = geodist(testx(1)-xi,testx(2)-yi);
            J(2)     = (Li - ri)'*(Li-ri);
            
            if J(1) < J(2)
                sol(1) = xsol(1);
            else
                sol(1) = xsol(2);
            end
            
                    ri      = geodist(sol(1)-xi,sol(2)-yi);
                    [A,b]    = buildMatElements(sol,Li,ri,ki,xi,yi,v);
            %         [temp,~] = lsqr(A(1,:),b(1),tol,maxit);
                    [temp,~] = lsqr(A,b,tol,maxit);
                    sol(1)   = temp(1);
            
        end
    end
    
    
    ysol = quadfnc(1,0,sol(1)^2 - s);
    ysign = ysol > 0;
    
    if ~all(isnan(ysol))
        % If only one root is positive, take it
        if sum(ysign) == 1 && all(isreal(ysol))
            sol(2) = ysol(ysign);
            
        elseif all(isreal(ysol)) && sum(ysign) == 2 || sum(ysign)==0
            % if negative
            ysol = abs(real(ysol));
            
            ri       = geodist(sol(1)-xi,ysol(2)-yi);
            [A,b]    = buildMatElements(ysol,Li,ri,ki,xi,yi,v);
            [temp,~] = lsqr(A,b,tol,maxit);
            ysol(2)  = temp(2);
            
            %compute J
            testy(1) = sol(1); testy(2) = ysol(1);
            ri       = geodist(testy(1)-xi,testy(2)-yi);
            J(1)     = (Li - ri)'*(Li-ri);
            
            
            testy(1) = sol(1); testy(2) = ysol(2);
            ri       = geodist(testy(1)-xi,testy(2)-yi);
            J(2)     = (Li - ri)'*(Li-ri);
            
            if J(1) < J(2)
                sol(2) = ysol(1);
            else
                sol(2) = ysol(2);
            end
            
                    ri       = geodist(sol(1)-xi,sol(2)-yi);
                    [A,b]    = buildMatElements(sol,Li,ri,ki,xi,yi,v);
            %         [temp,~] = lsqr(A(2,:),b(2),tol,maxit);
                    [temp,~] = lsqr(A,b,tol,maxit);
                    sol(2)   = temp(2);
            
        end
    end
    % finaly compute the solution
    ri      = geodist(sol(1)-xi,sol(2)-yi);
    [A,b]   = buildMatElements(sol,Li,ri,ki,xi,yi,v);
    [sol,flag] = lsqr(A,b,tol,maxit);
    
end
