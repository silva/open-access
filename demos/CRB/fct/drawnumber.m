function nb = drawnumber( a,b, N )
    %DRAW draws a random number from the range a,b
    if ~exist('N','var')
        N = 1;
    end
    assert(a<b);
    
    nb = (b-a).*rand(N,1) + a;
    
end

