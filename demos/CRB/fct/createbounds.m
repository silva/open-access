

if flag_cdma == 1
    [Gcdma,Fcdma] = pwelch(cdma,[],[],[],fs,'centered');
    
%     Gcdma = Gcdma./max(Gcdma)
    F2cdma        = F2(Fcdma,Gcdma);
    CRBcdma       = math.CRBT(cno,F2cdma,simTime);
    
    for k=1:length(cno)
        dpCRBDcdma(k,:) = math.CRBD(D,cno(k),F2cdma,go,ko,simTime);
    end
    
end

if flag_ofdm == 1
    [Gofdm,Fofdm] = pwelch(ofdm,[],[],[],fs,'centered');
%     Gofdm = Gofdm./max(Gofdm)
    F2ofdm        = F2(Fofdm,Gofdm);
    CRBofdm       = math.CRBT(cno,F2ofdm,simTime);
    
    for k=1:length(cno)
        dpCRBDofdm(k,:) = math.CRBD(D,cno(k),F2ofdm,go,ko,simTime);
    end    
end


% A=sum( (2.*pi.*f).^2 .* abs(s).^2)./ ( sum(abs(s).^2));
% stdRangeCRLB1 = sqrt(1./(cno.*1/2.*A.*simTime)) * c;