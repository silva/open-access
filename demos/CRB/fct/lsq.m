function [ epos, err, V, ign] = lsq( init_pos, rpos, emitter, Li, tol, maxit)
    %DOLSQ solves a positioning least squares system
    
    % Compute constans
    epos = init_pos(:);
    xi   = emitter(:,1);
    yi   = emitter(:,2);
    
    
    % Compute direction vectors to basestations
    [cosvec, nvec] = math.directorcos(epos(1:2),emitter);
    
    % Prepare least squares matrices
    H = [-cosvec];% ones(size(cosvec,1),1)];
    
    % Obtain the position
    Z       = Li - (nvec );
    [dX,flag] = lsqr(H,Z,1e-06,10);
    if length(dX(:)) == 2
        epos(1:2) = epos(1:2) + dX;
    else
        epos = epos + dX;
    end
    
    err = rmse(rpos(:),epos(1:2));
    V   = inv(H'*H);
    ign = (flag ~= 0);
    
end

%EOF