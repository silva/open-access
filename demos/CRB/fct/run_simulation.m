function run_simulation(mfile, nbWalks, emmitters, nbEmmitters, nbSteps, start, tol, maxit, minCN0, hOut, sel_tech,sim_output_fid,wid,len)
    %UNTITLED3 Summary of this function goes here
    %   Detailed explanation goes here
    
    
    
    for walk_num=1:nbWalks
        track     = mfile.walk(:,:,walk_num);  
        wval      = mfile.walk_val(walk_num,1):mfile.walk_val(walk_num,2);
        eval      = mfile.est_val(walk_num,1):mfile.est_val(walk_num,2);
        cno       = mfile.cno(wval,1);          
        mes       = mfile.measurements(wval,1);   
        compute_estimates( mfile, eval, track, cno, mes, sel_tech, emmitters, nbEmmitters, nbSteps, start, tol, maxit, minCN0, hOut,sim_output_fid,wid,len)
    %     [~] = arrayfun(@compute_estimates, mfile, walk_num, track, cno, mes, sel_tech, emmitters, nbEmmitters, nbSteps, start,tol, maxit, minCN0, hOut);
    end
%     disp('break me')
 
end

