function tx  = genOFMD(nbSym,N,Nsd,Nst,dF,pilotPw,modulation,modSelect,fs)
    %GENOFMD generates an OFDM signal
    %   An OFDM signal is returned with thhe desired CNR and MODULATION.
    %   The signal is also oversampled by the OVERSAMPLE factor, which should
    %   be a positive number bigger than one.
    %   NBSAMPLES controls the size of the random generated binary message
    %   that is used throughout the process, if TXBITS is empty.
    %
    %   INPUT
    %   nbCarriers - Number of carriers (FFT size)
    %   overSample - Over sample factor
    %   modulation - Modulation size (>0 QAM, <0 DPSK)
    %   [modSelect]     - Modulation type ('psk' (by default) or 'qam')
    %   [sgPower]       - Desired power in Watts (default: normalised)
    %   [spreadingCode] - Use the specified spreading code
    %   [txBits]        - Message to be used (ignores nbSamples)
    %   [Ts]            - Sampling period (default is 20M samples/s)
    %
    %   OUTPUT
    %   tx         - OFDM signal
    %
    %   Pedro Silva, Tampere University of Technology, 2013
    
    % Input parameter assertion
    assert(N > 0);
    assert(modulation > 0);
    
    % Defaults
    if ~exist('modSelect','var') || isempty(modSelect)
        modSelect = 'psk';
    end
    
    data = randsrc(N,nbSym,0:modulation-1);
    % Encoding
    if modSelect == 1,
        mdata = qammod(data,modulation,[],'gray');
    elseif modSelect == 0,
        mdata = psk(data,modulation);
    end
    
    flag = 0;
    % From table 18-7
    if modulation == 4
        kmod = 1/sqrt(2);
    elseif modulation == 16
        kmod = 1/sqrt(10);
    elseif modulation == 64
        kmod = 1/sqrt(42);
    else
        kmod = 0.1104;
        flag=1;
    end
    mdata = mdata.*kmod;
    
    % Parameters
    Ts                          = 1/fs;
    T_FFT                       = 1/dF*fs;
    T_GI                        = T_FFT/4;
    T_GI2                       = T_FFT/2;
    
    % Ignore data in Pilot sequence (18-24)
    map_freq = [-N/2:N/2-1]';
    
    mdata(map_freq<-Nst/2,:)= 0; % must
    
    if flag == 0
        mdata(map_freq==-21,:) = 1;
        mdata(map_freq==-7,:)  = 1;
        mdata(map_freq==0,:)   = 0; % must
        mdata(map_freq==7,:)   = 1;
        mdata(map_freq==21,:)  = -1;
    else
        mdata(map_freq==-53,:) = 1;
        mdata(map_freq==-25,:)  = 1;
        mdata(map_freq==-11,:)  = 1;
        mdata(map_freq==0,:)   = 0; % must
        mdata(map_freq==11,:)   = 1;
        mdata(map_freq==25,:)   = 1;
        mdata(map_freq==53,:)  = -1;
    end
    mdata(map_freq>Nst/2,:)= 0; % must
    
    non_pilots = ones(size(map_freq));
    if flag == 0
        non_pilots(map_freq == -21) = 0;
        non_pilots(map_freq == -7)  = 0;
        non_pilots(map_freq == 0) = 0;
        non_pilots(map_freq == 7) = 0;
        non_pilots(map_freq == 21) = 0;
    else
        non_pilots(map_freq == -53) = 0;
        non_pilots(map_freq == -25)  = 0;
        non_pilots(map_freq == -11) = 0;
        non_pilots(map_freq == 0) = 0;
        non_pilots(map_freq == 11) = 0;
        non_pilots(map_freq == 25) = 0;
        non_pilots(map_freq == 53) = 0;
    end
    non_pilots(1:6) = 0;
    non_pilots(end-4:end) = 0;
    
    %     % Create training sequences
    S           = [zeros(6,1);trainSequence(1);zeros(5,1)]; % Short sequence
    short_train = zeros((T_FFT/4)*10,1);
    for t = 1:((T_FFT/4)*10) % 10 periods of 0.8us
        short_train(t,:) = sum(S.*exp(1j*2*pi*Nst*dF*Ts*(t-1)));
    end
    
    L          = [zeros(6,1);trainSequence(2);zeros(5,1)]; % Long sequence
    long_train = zeros(T_GI2+T_FFT*2,1);
    for t = 1:(T_GI2+T_FFT*2) % 10 periods of 0.8us
        long_train(t,:) = sum(L.*exp(1j*2*pi*Nst*dF*Ts*(t-1-T_GI2)));
    end
    
    
    
    
    % bsxfun(@times,mdata(:,n),d_coeffs);
    % close all;for t=1:128, aux=exp(1j*2*pi*map_freq*dF*Ts*(t-1)); plot(imag(aux));hold on;drawnow;pause(0.01); end;
    % Create the mdata for the 48 symbols (18-22)
    %     t           = 1:(T_GI+T_FFT);
    %     d_coeffs    = exp(1j*2*pi*map_freq*dF*Ts*(t-1-T_GI));
    %     p_coeffs    = exp(1j*2*pi*map_freq*dF*Ts*(t-1-T_GI));
    %     ofdm_symbol = zeros(T_GI+T_FFT,nbSym); % Collumn wise for (:) to work
    
    t           = 1:(T_FFT);
    d_coeffs    = exp(1j*2*pi*map_freq*dF*Ts*(t-1));
    ofdm_symbol = zeros(T_GI+T_FFT,nbSym); % Collumn wise for (:) to work
    
    % For each symbol multiply with coeffs
    for n = 1:nbSym
        
        %         mdata(:,n) = mdata(:,n).*1/sqrt((Nsd+Nst)).*kmod;
        
        
        % PILOT polarity
        p = getPilotSignal(n).*(var(mdata(non_pilots==1))*pilotPw); % first entry is for signal symbol
        if flag == 0
            mdata(map_freq==-21,n) = p;
            mdata(map_freq==-7,n)  = p;
            mdata(map_freq==7,n)   = p;
            mdata(map_freq==21,n)  = -p;
        else
            mdata(map_freq==-53,n) = p;
            mdata(map_freq==-25,n)  = p;
            mdata(map_freq==-11,n)   = p;
            mdata(map_freq==11,n)  = p;
            mdata(map_freq==25,n)  = p;
            mdata(map_freq==53,n)  = -p;
        end
        % DATA & PILOT
        ofdm_data  = sum(bsxfun(@times,mdata(:,n),d_coeffs));
        
        % Merge the two
        ofdm_symbol(T_GI+1:end,n) = ofdm_data;%/T_FFT;
    end
    ofdm_symbol(1:T_GI,:) = ofdm_symbol(end-T_GI+1:end,:);
    
    %     tx =[];n=0;
    %     for k=1:6
    %
    %         aux = ofdm_symbol(:,1+111*n:(111)*(n+1));
    %     tx = [tx; short_train(:); long_train(:); aux(:)];
    %     n=n+1;
    %
    %     end
    
    tx = ofdm_symbol(:);
    tx = tx(:)./(sqrt(mean(abs(tx(:)).^2)));
    
end


function data = psk(data,N)
    %PSK maps the input symbols to N chips
    %   USAGE
    %   data = psk(data,N)
    %
    %   INPUT
    %   data - symbol stream
    %   N    - constellation mapping size
    %
    %   OUTPUT
    %   data - BPSK or DQPSK modulated signal
    %
    %   Pedro Silva, Tampere university of Technology, 2013
    
    
    if N == 2 % QPSK
        data(data==0) = -1;
        
    elseif N == 4 % DQPSK
        data(data==0) = -1 - 1i;
        data(data==2) =  1 - 1i;
        data(data==1) = -1 + 1i;
        data(data==3) =  1 + 1i;
    else
        error('Available: BPSK and DPSK');
    end
    
end

%         tx(Td+1,1) = sum(mdata.*exp(1i*2*pi.*getFreqOffset().*dF.*(Td.*Ts-Tgi))) ...
%             + getPilotSignal(n).*sum(getPilot().*exp(1i.*2.*pi.*k.*dF.*(Td.*Ts-Tgi)));

function polarity = getPilotSignal(nbSymbol)
    %GETPILOTSIGNAL returns the pilot polarity for symbol N
    %
    %   USAGE
    %   signal = getPilotSignal(0) %for OFDM signal polarity
    %   singal = getPilotSingal(N) %for OFDM data polariity (N>0)
    %
    %   Notice that the first entry (0) is mapped to the first entry in the
    %   MATLAB vector (vec(1)).
    %
    %   !!!
    %   This entry is the OFDM SIGNAL polarity and to retrieve it, the sequence
    %   number should be used.
    %   !!!
    %
    %   The function uses N+1 to access the internal polarity vector
    %
    %   INPUT
    %   N        - Symbol sequence number
    %
    %   OUTPUT
    %   polarity - Polarity of pilot data for given symbol sequence number
    %
    %   Pedro Silva, Tampere University of Technology, 2013
    
    % Polarity of the pilot sequence
    p = [ 1, 1, 1, 1,-1,-1,-1, 1,...
        -1,-1,-1,-1, 1, 1,-1, 1,...
        -1,-1, 1, 1,-1, 1, 1,-1,...
        1, 1, 1, 1, 1, 1,-1, 1,...
        1, 1,-1, 1, 1,-1,-1, 1,...
        1, 1,-1, 1,-1,-1,-1, 1,...
        -1, 1,-1,-1, 1,-1,-1, 1,...
        1, 1, 1, 1,-1,-1, 1, 1,...
        -1,-1, 1,-1, 1,-1, 1, 1,...
        -1,-1,-1, 1, 1,-1,-1,-1,...
        -1, 1,-1,-1, 1,-1, 1, 1,...
        1, 1,-1, 1,-1, 1,-1, 1,...
        -1,-1,-1,-1,-1, 1,-1, 1,...
        1,-1, 1,-1, 1, 1, 1,-1,...
        -1, 1,-1,-1,-1, 1, 1, 1,...
        -1,-1,-1,-1,-1,-1,-1];
    
    polarity = p(mod(nbSymbol-1,length(p))+1);
    
    
    %     if nbSymbol(end) < length(p-1)
    %
    %     else
    %         polarity = repmat(p(2:end),1,round(nbSymbol(end)/length(p(2:end))));
    %     end
    
end



%
function trainSeq = trainSequence(type)
    
    if type == 1
        tshort = sqrt(13/6).*[0, 0, 1+1i, 0, 0, 0, -1-1i,...
            0, 0, 0, 1+1i, 0, 0, 0, -1-1i,...
            0, 0, 0, -1-1i, 0, 0, 0, 1+1i,...
            0, 0, 0, 0, 0, 0, 0, -1-1i,...
            0, 0, 0, -1-1i, 0, 0, 0, 1+1i,...
            0, 0, 0, 1+1i, 0, 0, 0, 1+1i,...
            0, 0, 0, 1+1i, 0,0];
        trainSeq = tshort;
    else
        
        tlong = [1, 1, -1, -1, 1, 1, -1, 1, -1, 1, 1,...
            1, 1, 1, 1, -1, -1, 1, 1, -1, 1, -1,...
            1, 1, 1, 1, 0, 1, -1, -1, 1, 1, -1,...
            1, -1, 1, -1, -1, -1, -1, -1, 1, 1,...
            -1, -1, 1, -1, 1, -1, 1, 1, 1, 1];
        trainSeq = tlong;
    end
    
    trainSeq = trainSeq';
    
end
