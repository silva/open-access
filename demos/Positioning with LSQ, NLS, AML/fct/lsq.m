function [ epos, err, V, ign] = lsq( init_pos, rpos, emitter, Li, tol, maxit, max_err)
%DOLSQ solves a positioning least squares system


    if ~exist('max_err','var')
        max_err = Inf;
    end
    
    % functions
    geodist = @(x,y)   sqrt(x.^2+y.^2);
    
    % Compute constans
    epos = init_pos(:);
    xi   = emitter(:,1);
    yi   = emitter(:,2);
    ki   = sum([xi yi].^2,2);
   
  
    ign  = false;
    estimate(maxit,2) = Inf;
    epos_p  = init_pos.*Inf;
    cnt = 0;
    rcvclk = 0;
    
     while cnt < maxit
         cnt = cnt+1;
        % Compute direction vectors to basestations
        [cosvec, nvec] = math.directorcos(epos,emitter);

        % Prepare least squares matrices
        H = [-cosvec];

        % Obtain the position
        Z       = Li - nvec;
        dX      = (H'*H)\H'*Z;
        epos       = epos + dX;  
        
        err_p  = rmse(epos(:),epos_p(:));
        if err_p < tol
            break;
        end
        epos_p = epos;
        
     end
    
    err = rmse(rpos(:),epos(:));
    V   = inv(H'*H);
    ign = false;
     
end

%EOF