function [ epos, err, V, ign] = nlsq( init_pos, rpos, emitter, Li, tol, maxit, max_err)
    %UNTITLED5 Summary of this function goes here
    %   Detailed explanation goes here
    
    xi = emitter(:,1);
    yi = emitter(:,2);
    li = Li(:);
    
    res = @(pos) [(xi-pos(1)).^2 + (yi-pos(2)).^2 - li.^2];
    
    [epos, Ssq, CNT, Res, XY] = LMFnlsq2(res,init_pos);
    
    err = rmse(rpos,epos);
    V = [];% Ssq'*Ssq;
    ign = (CNT > 100);
end

