function  [e_aml,e_nls,e_lsq] = compute_estimates( track, meas, emmitters, nbEm, nbSteps, start, tol, maxit, hOut, showEst)
    
    % hMatfile - matfile object
    % walk_num - number of walk
    % this function computes for one walk the AML and LSQ estimates
    aml_estimate = NaN(nbSteps,3);
    lsq_estimate = NaN(nbSteps,3);
    nl_estimate  = NaN(nbSteps,3);
    %take into account walk_num
    len  = nbEm;
    head = 1;
    init = start;
    start_aml = init;
    start_lsq = init;
    start_nl  = init;
    
    m_idx = 1:nbEm:length(meas);
    
    for step=1:nbSteps
        
        % retrieves information for the Nth walk
        pos  = track(step,:);
        E_in = emmitters; % indexes are valid for these ones as well
        L_in = meas(m_idx(step):m_idx(step)+nbEm-1);
        
        % call estimators
        %nlsq id
        [epos,ermse,V_n,ign]  = nlsq( start_nl, pos, E_in(:,1:2), L_in, tol, maxit);        
        if ~ign
            start_nl = epos;
            nl_estimate(step,:) = [epos',ermse];
        else
            start_nl = init;
        end
%         lsq id
        [epos,ermse,V_l,ign] = lsq( start_lsq, pos, E_in(:,1:2), L_in, tol, maxit);
        if ~ign
            start_lsq = epos;
            lsq_estimate(step,:) = [epos',ermse];
        else
            start_lsq = init;
        end
        if ermse > 100 || isnan(ermse)
            start_lsq = init;
        end
        
        %aml id
        [epos,ermse,V_a,ign] = aml( start_aml, pos, E_in(:,1:2), L_in, 1, tol, maxit);
        if ~ign
            start_aml = epos;
            aml_estimate(step,:) = [epos',ermse];
        else
            start_aml = init;
        end
        if ermse > 100 || isnan(ermse)
            start_aml = init;
        end
        
        switch showEst
            case 1 % aml
                hOut.animateEstimation(step,track, E_in(:,1:2),aml_estimate(step,:),V_a,5,5)
            case 2 % nls
                hOut.animateEstimation(step,track, E_in(:,1:2),nl_estimate(step,:),V_n,5,5)
            case 3 % lsq
                hOut.animateEstimation(step,track, E_in(:,1:2),lsq_estimate(step,:),V_l,5,5)
            otherwise
                %do nothing
        end
        
        
        head = head + len;
    end
    
    e_aml = aml_estimate;
    e_lsq = lsq_estimate;
    e_nls = nl_estimate;
    
end

%     walk  = room.storage.walk(:,:,k);   % [Nsteps x 2]
%     wvar  = room.storage.svar(:,:,k);   % [Nemitters x Nsteps]
%     wmeas = room.storage.meas(:,:,k);   % [Nemitters x Nsteps]
%     wrag  = room.storage.ranges(:,:,k); % [Nemitters x Nsteps]
%     wcno  = room.storage.cno(:,:,k);    % [Nemitters x Nsteps]

