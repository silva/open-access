%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%         TUT         %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%   AML POSITIONING   %%%
%%%                     %%%
%%%   Pedro Silva       %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%
addpath('fct/');
warning('off','all');
GetNewTracks = true;
saving_dir   = './storage/';
if ~exist(saving_dir,'dir')
    mkdir(saving_dir)
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Signal   Parameters %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%

% range for std generation
min_std = 0; 
max_std = 1;


%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Scenario Parameters %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%
room_length = 10;
room_width  = 10;
room_depth  = [];
ndim        = 2;
unit        = 1; 
nbSteps     = 100;
lenStep     = 1;
nbWalks     = 2;

maxEmitters    = 5;
nbTechnologies = 1;
totEm          = nbTechnologies*maxEmitters;
techtype       = NaN(totEm,1);
type_counter   = 0;
for k=1:maxEmitters:totEm
    type_counter = type_counter + 1;
    techtype(k:k+maxEmitters-1) = type_counter;
end

PLModel = [{@radio.pathloss.ITU_indoor_rss};{@wifi_model_params}];
hFuncs = {PLModel{techtype}}.';

%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Scenario Creation %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%
disp('Preparing scenario')
room = Room(room_length,room_width,room_depth,ndim,1,saving_dir,['demo_',num2str(nbWalks),'_',num2str(room_length),'x',num2str(room_width),'_WalksAndMeasurements'],GetNewTracks);% generate a room
if GetNewTracks && ~room.exists
    disp('... building new data')
    room.placeObjects(totEm,techtype,[]);%place stations randomly in the room
    room.create2DWalk(nbSteps,lenStep,nbWalks,maxEmitters)% generate walks
    room.ranging(min_std,max_std); % computes measurement for each equipment
end
% YES, coordinates are starting at zero!

%% from here, select the scenario and apply it for each track
clearvars -except room saving_dir nbWalks room_length room_width;
close all;
disp('Starting positioning')

% Output/input
hOut = inout.output();
xlim([0 room.wid])
ylim([0 room.len])
hold(gca,'on');


% get data
emmitters   = room.storage.objs;   % emitters location
start       = mean(emmitters(:,1:2));  % mean of emitter location
nbSteps     = room.storage.number_of_steps_per_walk;
nbWalks     = room.storage.number_of_walks;

% for one particular selection
mfile = room.storage;
nbEm  = size(emmitters,1);

% technology selectors
techtype  = room.storage.objs(:,3);
nbVec     = 1:length(techtype);

% parameters
tol       = 1e-6;
maxit     = 5;


% show_est = 1; % animates with aml
show_est = 2; % animates with nls
% show_est = 3; % animates with lsq

switch show_est
    case 1
        title 'AML'
    case 2
        title 'NLSQ'
    case 3
        title 'LSQ'
end


for walk_num=1:nbWalks
    track = mfile.walk(:,:,walk_num);
    meas  = mfile.measurements(:,1);
    [e_aml,e_nls,e_lsq] = compute_estimates( track, meas, emmitters, nbEm, nbSteps, start, tol, maxit, hOut, show_est);
end

