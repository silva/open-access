classdef RadioMaps < radio.spectrum
    %RADIOMAPS
    %   This class implements accessors and getters for a database of radio
    %   measurements. The goal is to use it for fingerprinting purposes.
    
   
    properties
        PLparams
    end
    
    methods
        
        function obj = RadioMaps(unit,limits,id_list,tx_list,tx_model,createRM,mfile)
            
            % Call to constructor class
            obj       = obj@radio.spectrum(limits,unit,mfile,tx_model);
            
            if createRM
                [L0,n,s]  = radio.pathloss.commonParams(tx_list(:,end),'office');
                % Store information
                obj.setDB({id_list, tx_list, [L0,n,s],tx_model});
                obj.PLparams = [L0,n,s];
                obj.survey(mfile);
            else
                [L0,n,s] = radio.pathloss.commonParams(tx_list(:,end),'office');
                PL       = [L0,n,s];
                obj.setDB({id_list, tx_list,PL,tx_model});
                obj.PLparams = PL;
                
                % Store information
                obj.setDB({id_list, tx_list,PL,tx_model});
                obj.PLparams = PL;
            end
        end
    end
    
    methods
        
        
        
        function [heard,nbSeen] = dumpSelection(obj,sel_ids,sensitivity,mfile)
            
            if isempty(obj.radiomaps)
                obj=obj.storage;
            end
            nbSeen      = length(sel_ids);
            NR          = length(obj.radiomaps(:,1));
            NI          = length(sel_ids);
            mfile.heard = zeros(NR,NI);
            for k=1:NI
                heard  = obj.radiomaps(:,sel_ids(k))'+obj.shadowing(:,sel_ids(k))';
                
                if nargin >= 3 && ~isempty(sensitivity)
                    heard(heard < sensitivity(k)) = NaN;
                end
                mfile.heard(:,k) = heard(:);
            end
        end
        
        
        function [freq,sens,shw] = signalDetails(obj,type)
            
            switch(upper(type))
                
                case 'BLE'
                    freq = 2.4e9; %Hz
                    sens = obj.sen_wifi;
                    shw  = obj.shw_wifi;
                    
                case 'WIFI'
                    freq = 2.4e9; %Hz
                    sens = obj.sen_wifi;
                    shw  = obj.shw_wifi;
                    
                case 'RFID'
                    freq = 866e6; %Hz
                    sens = obj.sen_rfid;
                    shw  = obj.shw_rfid;
                    
                otherwise
                    error('RadioMaps:signalDetails','Type unknown');
            end
            
        end
        
        
        
        
        
    end
    
end

