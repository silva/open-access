%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%   Tampere University of Technology
%
%   POSTECH
%
%   This script allows for the deployment of several positioning
%   technologies over a grid.
%
%
%   Developed by,
%
%   Pedro Silva
%   pedro.silvha@tut.fi
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
close all
clearvars

% Device grid 
ble_grid      = [1 1 0]; % (m) deploys over every x y and z meters
wifi_grid     = [1 1 0]; % (m)

% Environment specification (where devives are deployed)
env_wid       = 5;  % (#) width
env_len       = 5;  % (#) length 
env_dep       = 0;  % (#) depth/height
env_unit      = 1;  % (#) env grid unit

% Walk params
nbSteps       = 10; % (#) Number of steps taken by the user
lenStep       = 1;  % (#) Length of the steps (RandomWalkGrid only!)
nbWalks       = 1;  % (#) Nb of walks

% Output modifiers
outDir        = 'storage/'; % where things are stored
outEnv        = ['grid_database_1x1_over_',num2str(env_wid),'x',num2str(env_len)]; % environment file 
overwrt       = true;   % overwrites disk mfile

% Tx and Rx params
tx_type           = [radio.spectrum.inter2id('WiFi'),...  % use inter2id to obtain tech id
                     radio.spectrum.inter2id('BLE')];
tx_model          = {@radio.pathloss.LogDistance;@radio.pathloss.shadowing}; % PL model to use for distance to RSS and for shadowing
wifi_sensitivity  = -110;  % limit below which the device does not hear the devices
ble_sensitivity   = -110;


%% create environment
disp('creating environment...');
env = Environment(env_len,env_wid,env_dep,3,env_unit,outDir,outEnv,overwrt);% generates an environment

% deploy devices over specified grid
disp('deploying devices in the environment...');
env.deploy(wifi_grid,[],tx_type(1)); %wifi
env.deploy(ble_grid ,[],tx_type(2)); %ble
env.turnON(tx_model); % radiomaps created and stored
% Create N random walks
env.create2DWalk(nbSteps,lenStep,nbWalks,env.storage);
disp('Devices On!(Radiomaps created...)');
%EOF