classdef Receiver < build.device
    %UNTITLED6 Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        fingerprints  % mfile with fingerprints
        % shadowing is addded to the ideal database
        % generated according to the number of track points
        output
        wifi_sensitivity
        rfid_sensitivity
        cdf_val
        cdf_axis
        pest
        disttoreal
        rmse
    end
    
    properties(Access=private)
        sigma_shadowing_global
        nbNeighbours
    end
    
    methods
        
        function obj = Receiver(hEnv,sensitivity,dir,filename,forceOW,nbNeighbours,sigma_global,tx_list)
            
            obj = obj@build.device(hEnv.spectrum,sensitivity,dir,[filename,'.mat'],forceOW);
            obj.nbNeighbours           = nbNeighbours;
            obj.sigma_shadowing_global = sigma_global;
            obj.spectrum.rm_pos        = tx_list;
            obj.storage.nbWalks        = hEnv.storage.nbWalks;
            obj.storage.nbSteps        = hEnv.storage.nbSteps;
            obj.storage.walk           = hEnv.storage.walk;
            
        end
        
        function runFingerprinting(obj,IntType,outfile)
            
            % Acquires fingerprints
            obj.acquire();
            
            % Estimates location
            if nargin == 2 || isempty(outfile)
                outfile = obj.storage;
            else
                outfile = inout.output.createMatfile(outfile,true,true);
            end
            obj.estimate(IntType,obj.sigma_shadowing_global,obj.nbNeighbours,outfile); % Obtains the estimated positions
            [obj.cdf_val,obj.cdf_axis]=math.cdf(obj.disttoreal,[],1000);
        end
        
      
        
        
    end
    
    
end

