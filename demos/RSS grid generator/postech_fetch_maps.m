%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%   Tampere University of Technology
%
%   POSTECH
%
%   This script allows for the deployment of several positioning
%   technologies over a grid.
%
%
%   Developed by,
%
%   Pedro Silva
%   pedro.silvha@tut.fi
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
close all
clearvars

% Device grid 
ble_grid      = [5 5 0]; % (m)
wifi_grid     = [5 5 0]; % (m)

% Environment specification
env_wid       = 5;   % (#) width
env_len       = 5;   % (#) length
env_dep       = 0;   % (#) depth/height
env_unit      = 1;   % (#) env grid unit
env_exclusion = [];  % (#) defines an exclusion zone

% Walk params
nbSteps       = 10;
lenStep       = 1;
nbWalks       = 1;

% Output modifiers
outDir        = 'storage/';
outEnv        = ['grid_database_1x1_over_',num2str(env_wid),'x',num2str(env_len)];
outRes        = ['fingerprints_output_'];
overwrtEnv    = false;
overwrtRes    = true;

showRadioMaps = false;
showWalk      = true;
showCDF       = true;

% Tx and Rx params
tx_type           = [radio.spectrum.inter2id('Wifi'),...
                     radio.spectrum.inter2id('BLE')];
tx_model          = {@radio.pathloss.LogDistance;@radio.pathloss.shadowing};
wifi_sensitivity  = -110;
ble_sensitivity   = -110;

% Algorithm control
shadowingEstimate = mean([ 4 10 ]);
nbNeighbours      = 5;


%% create environment
disp('creating environment...');
grid = Environment(env_len,env_wid,env_dep,3,env_unit,outDir,outEnv,overwrtEnv);% generate an environment

% creates the desired grid
grid.deploy(wifi_grid,[],tx_type(1)); %wifi
grid.deploy(ble_grid ,[],tx_type(2)); %ble


% now use this grid to index a greater/finer one
new_list = grid.tx_list;
glo_list = grid.storage.tx_list;

%% This section finds in glo_list the devices in new list that you want to 
% use for the positioning afterwards. You can search by coordinates or ID
N      = length(new_list);
em_id  = zeros(N,1);
id_vec = glo_list(:,1); % IDS
for k = 1:N
    xt = glo_list(:,2) == new_list(k,1); % x
    yt = glo_list(:,3) == new_list(k,2); % y
    
    idx = xt & yt & glo_list(:,end) == new_list(k,end);
    if any(idx)
        em_id(k) =   id_vec(idx);
    else
        warning(['couldnt find emitter ',num2str(k),'-th'])
    end
end


%% For other configurations
for run_id   = 1;
    
    %%% Loads desired radio maps
    grid.loadSpectrum(em_id,tx_model,grid.storage);
     
    % configure device to acquire fingerprint
    disp('creating acquisition device...');
    sensitivity  = [ones(1,sum(grid.tx_list(:,end) == 1)).*wifi_sensitivity,ones(1,sum(grid.tx_list(:,end) == 3)).*ble_sensitivity];
    mobile       = Receiver(grid,...
                    sensitivity,...
                    outDir,[outRes,num2str(run_id)],...
                    overwrtRes,... %force Overwrite of fingerprint file
                    nbNeighbours,...
                    shadowingEstimate,...
                    [grid.Mx(:),grid.My(:),grid.Mz(:)]);
   
    

    % Add it to the environment
    disp('running fingerprinting algorithm...');
    mobile.runFingerprinting('All',[]);
    

    % Show environment
    disp('showing it...');
    
    if showRadioMaps
        figure;
        grid.show(gca);
        for k=1:length(grid.tx_list);hold on; contourf(reshape(grid.spectrum.retrieve(k,true),(env_len+1)*1/env_unit,(env_wid+1)*1/env_unit)');hold on;grid.show(gca);colorbar;caxis([-100, 0]);drawnow;pause(0.5);end;
    end
    
    % Walks plot
    if showWalk
        figure;
        grid.show(gca);
        for k=1:nbSteps; hold on;plot(mobile.storage.walk(k,1)+1,mobile.storage.walk(k,2)+1,'-*k');drawnow;end;
    end
    disp('all done...');
    
    % CDF plot
    if showCDF
        figure;
        plot(mobile.cdf_axis,mobile.cdf_val)
        xlim([0 max(round(mobile.cdf_axis+1))]);
        ylim([0 1]);
    end
    
end






