classdef Environment < build.scenario
    %ENVIRONMENT wrapper for SCENARIO object
    
    
    properties
        tx_list
        map
        spectrum
        device
        
        nbSteps
        lenStep
        nbWalks
    end
    
    
    methods
        
        function obj = Environment(len,wid,dep,ndim,unit,dir,matfilename,forceOW)
            %ENVIRONMENT class constructor
            obj = obj@build.scenario(len,wid,dep,ndim,unit,dir,matfilename,forceOW);
        end
        
        function deploy(obj,grid_coord,grid_exclusion_limits,tx_type)
            
            grid_coord=grid_coord.*1/obj.unit;
            
            % Create grid
            tx_coord = obj.create_grid(grid_coord(1),grid_coord(2),grid_coord(3),grid_exclusion_limits);
            
            % append to list coordinates and type
            tx_coord            = [tx_coord, repmat(tx_type,size(tx_coord,1),1)];
            obj.tx_list         = [obj.tx_list;tx_coord];
            
            if tools.isWritable(obj.storage)
                obj.storage.tx_list = obj.tx_list; % CAREFUL
            end
        end
        
        function turnON(obj,hFunc)
            
            % Associate ID and hFunc to TX
            N      = length(obj.tx_list);
            idlist = 1:N;
            
            if length(hFunc) ~= N
                hFunc = repmat({hFunc},N,1);
            end
            
            if tools.isWritable(obj.storage)
                obj.storage.tx_list = [idlist(:),obj.tx_list];
            end
            
            % build radio maps for them
            obj.spectrum = RadioMaps(obj.unit,obj.limits,idlist(:),obj.tx_list,hFunc,true,obj.storage);
            
        end
        
        
        function loadSpectrum(obj,idlist,hFunc,mfile)
            
            % Associate ID and hFunc to TX            
            N = length(idlist);
            if length(hFunc) ~= N
                hFunc = repmat({hFunc},N,1);
            end
            
            % build radio maps for them
            obj.spectrum = RadioMaps(obj.unit,obj.limits,idlist(:),obj.tx_list,hFunc,false,mfile);
            
        end
        
        
        function create2DWalk(obj,nbSteps,lenSteps,nbWalks,mfile)
            %CREATE2DWALK generates N walks of size nbSteps with a step
            %size defined by lenSteps.
            % The walks are stored in a matfile.
            obj.nbSteps = nbSteps;
            obj.lenStep = lenSteps;
            obj.nbWalks = nbWalks;
            
            mfile.nbWalks = nbWalks;
            mfile.nbSteps = nbSteps;
            mfile.walk    = NaN(nbSteps*nbWalks,2);
            head = 1;
            len  = nbSteps;
            for k=1:nbWalks
                mfile.walk(head:head+len-1,:) = build.walk.ramdomWalkAngle(nbSteps,obj.limits);
                head = head+len;
            end
            
            disp('Walks created inside ');
            disp(obj.limits(:,1:2));
        end
        
        
        function grid = create_grid(obj,env_grid_len,env_grid_wid,env_grid_dep,env_exclusion)
            %CREATE_GRID generates a grid for devices to be set on
            Mx   = obj.Mx(1:env_grid_len:end,:);
            My   = obj.My(:,1:env_grid_wid:end);
            Mz   = obj.Mz(1:env_grid_dep:end,:);
            if isempty(Mz)
                Mz = zeros(size(Mx));
            end
            n    = min([numel(Mx) numel(My) numel(Mz)]);
            Mx   = Mx(:);
            My   = My(:);
            Mz   = Mz(:);
            grid = [Mx(1:n),My(1:n),Mz(1:n)];
            grid = unique(grid, 'rows');
            % handle exclusion zone
        end
        
        function tx=getTxObj(obj)
            tx = obj.tx_list;
        end
        
        function show(obj,hOut)
            hold(hOut,'on');
            for k=1:length(obj.tx_list)
                if obj.tx_list(k,end) == 1
                    a = 40;
                else
                    a = 10;
                end
                scatter(hOut,obj.tx_list(k,1)*1/obj.unit+1,obj.tx_list(k,2)*1/obj.unit+1,a,[0 0 0])
            end
            xlim([1 obj.len*1/obj.unit+1])
            ylim([1 obj.wid*1/obj.unit+1])
        end
    end
    
    
    
    
end

