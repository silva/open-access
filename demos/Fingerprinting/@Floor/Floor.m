classdef Floor < build.scenario
    
    properties
        nb_room
        room
        room_pos
    end
    
    properties(Access = private)
        map_ap_to_func
    end
    
    
    methods(Static)
        function grid = createMap(root,tomerge)
            [m,~] = size(root);
            grid = root+tomerge;
            for k=1:m
                f_l_btz = root(k,:) > 1;
                r_l_btz = tomerge(k,:) >1;
                
                hit = f_l_btz & r_l_btz;
                if any(hit)
                    grid(k,hit) = tomerge(k,hit);
                end
            end
            
        end
    end
    
    
    methods(Access=private)
        
        function sr_ap_coord = set_coord_in_small_room(obj,sr_nb_aps,sr_ap_location_type)
            switch sr_nb_aps
                case 1,
                    switch upper(sr_ap_location_type)
                        case 'CORNER'
                            sr_ap_coord = obj.room.limits(1,:);
                        case 'MIDDLE'
                            sr_ap_coord = (obj.room.limits(1,:)+obj.room.limits(2,:))/2;
                        case 'CENTER'
                            sr_ap_coord = (obj.room.limits(1,:)+obj.room.limits(2,:))/2;
                            sr_ap_coord(2) = obj.room.limits(1,2);
                        otherwise
                            error('Floor:deploy','Unknown location for device');
                    end
                    
                case 2,
                    switch upper(sr_ap_location_type)
                        case 'CORNER'
                            sr_ap_coord      = obj.room.limits(1,:);
                            sr_ap_coord(2,:) = obj.room.limits(2,:);
                        case 'MIDDLE'
                            sr_ap_coord = (obj.room.limits(1,:)+obj.room.limits(2,:))/2;
                            sr_ap_coord(2,:) = (obj.room.limits(1,:)+obj.room.limits(2,:))/2;
                            % change orientation up and down
                        case 'CENTER'
                            sr_ap_coord = (obj.room.limits(1,:)+obj.room.limits(2,:))/2;
                            sr_ap_coord(1,2) = obj.room.limits(1,2); % bottom
                            sr_ap_coord(2,:) = sr_ap_coord(1,:);     % top
                            sr_ap_coord(2,2) = obj.room.limits(2,2);
                        otherwise
                            error('Floor:deploy','Unknown location for device');
                    end
                case 4,
                    switch upper(sr_ap_location_type)
                        case 'CORNER'
                            sr_ap_coord      = obj.room.limits(1,:);% bottom left
                            sr_ap_coord(2,:) = obj.room.limits(2,:); % top right
                            sr_ap_coord(3,:) = obj.room.limits(1,:); % top left
                            sr_ap_coord(3,2) = obj.room.limits(2,2);
                            sr_ap_coord(4,:) = obj.room.limits(2,:); % bottom right
                            sr_ap_coord(4,2) = obj.room.limits(1,2);
                            
                        case 'MIDDLE'
                            sr_ap_coord      = (obj.room.limits(1,:)+obj.room.limits(2,:))/2;
                            sr_ap_coord(2,:) = (obj.room.limits(1,:)+obj.room.limits(2,:))/2;
                            sr_ap_coord(3,:) = (obj.room.limits(1,:)+obj.room.limits(2,:))/2;
                            sr_ap_coord(4,:) = (obj.room.limits(1,:)+obj.room.limits(2,:))/2;
                            
                        case 'CENTER'
                            sr_ap_coord      = (obj.room.limits(1,:)+obj.room.limits(2,:))/2; %mid
                            sr_ap_coord(1,2) = obj.room.limits(1,2); % bottom center
                            sr_ap_coord(2,:) = sr_ap_coord(1,:);
                            sr_ap_coord(2,2) = obj.room.limits(2,2); % top center
                            sr_ap_coord(3,:) = (obj.room.limits(1,:)+obj.room.limits(2,:))/2;
                            sr_ap_coord(3,1) = obj.room.limits(1,1); % left center
                            sr_ap_coord(4,:) = (obj.room.limits(1,:)+obj.room.limits(2,:))/2;
                            sr_ap_coord(4,1) = obj.room.limits(2,1); % right center
                            
                        otherwise
                            error('Floor:deploy','Unknown location for device');
                    end
                case 12
                    sr_ap_coord(1,:) = obj.room.limits(1,:);% bottom left
                    sr_ap_coord(2,:) = obj.room.limits(2,:); % top right
                    sr_ap_coord(3,:) = obj.room.limits(1,:); % top left
                    sr_ap_coord(3,2) = obj.room.limits(2,2);
                    sr_ap_coord(4,:) = obj.room.limits(2,:); % bottom right
                    sr_ap_coord(4,2) = obj.room.limits(1,2);
                    
                    sr_ap_coord(5,:) = (obj.room.limits(1,:)+obj.room.limits(2,:))/2;
                    sr_ap_coord(6,:) = (obj.room.limits(1,:)+obj.room.limits(2,:))/2;
                    sr_ap_coord(7,:) = (obj.room.limits(1,:)+obj.room.limits(2,:))/2;
                    sr_ap_coord(8,:) = (obj.room.limits(1,:)+obj.room.limits(2,:))/2;
                    
                    sr_ap_coord(9,:)  = (obj.room.limits(1,:)+obj.room.limits(2,:))/2; %mid
                    sr_ap_coord(9,2)  = obj.room.limits(1,2); % bottom center
                    sr_ap_coord(10,:) = sr_ap_coord(9,:);
                    sr_ap_coord(10,2) = obj.room.limits(2,2); % top center
                    sr_ap_coord(11,:) = (obj.room.limits(1,:)+obj.room.limits(2,:))/2;
                    sr_ap_coord(11,1) = obj.room.limits(1,1); % left center
                    sr_ap_coord(12,:) = (obj.room.limits(1,:)+obj.room.limits(2,:))/2;
                    sr_ap_coord(12,1) = obj.room.limits(2,1); % right center
                    
                otherwise
                    error('Too many devices');
            end
        end
    end
    
    methods
        function obj = Floor(r_len,r_wid,r_dep,ndim,unit,dir,matfilename,forceOW,nb_small_rooms,sr_len,sr_wid,sr_dep,sr_matfilename)
            
            % calls super class
            obj       = obj@build.scenario(r_len,r_wid,r_dep,ndim,unit,dir,matfilename,forceOW);% generate a room
            obj.coord = obj.limits;
            % generate a room (might have different units
            if nargin > 8 && (exist('nb_small_rooms','var') || isempty(nb_small_rooms) || nb_small_rooms ~= 0)
                obj.nb_room = nb_small_rooms;
                if nb_small_rooms > 1, warning('Only one room is possible to define');end;
                
                obj.room        = Floor(sr_len,sr_wid,sr_dep,ndim,unit,dir,['room-',matfilename],forceOW);
                obj.room_pos    = geometry.encloseArea(obj.limits,obj.room.limits);
                obj.room.limits = obj.room_pos;
                obj.room.setReference(obj.limits);

                %deal with other rooms later, if necessary
                
                %mark room in scene
                obj.drawRoom(obj.room_pos);
            end
        end
        
        function drawRoom(obj,limits)
            % move this to inOut
            [wid,len] = build.scenario.computeWidLen(limits);
            limits = limits+1;
            x1 = limits(1,1);
            x2 = limits(2,1);
            y1 = limits(1,2);
            y2 = limits(2,2);
            
            lwid = (x1:x2);
            if isempty(lwid)
                lwid=x2:x1;
            end
            for k = 1:wid+1
                obj.scene(lwid(k),y1) = Inf;
                obj.scene(lwid(k),y2) = Inf;
            end
            
            llen = (y1:y2);
            if isempty(llen)
                llen=y2:y1;
            end
            for k = 1:len+1
                obj.scene(x1,llen(k)) = Inf;
                obj.scene(x2,llen(k)) = Inf;
            end
        end
        
        function setUpStations(obj,nb_APS,interface,exclusion_zone)
            % place stations outside room
            type = RadioSpectrum.Spectrum.getInterfaceId(interface);
            obj.placeObjectOutside(nb_APS,type,exclusion_zone);
        end
        
        
        function [r_nb_aps,sr_nb_aps]=deploy(obj,r_nb_aps,r_ap_type,r_ap_coord,r_spacing,sr_nb_aps,sr_ap_type,sr_ap_coord,sr_spacing)
            
            hFunc = [];
            
            % Floor
            if r_nb_aps > 0
                if size(r_ap_type,1) ~= r_nb_aps
                    r_ap_type = repmat(r_ap_type,r_nb_aps,1);
                end
                [r_type,r_hFunc] = radio.spectrum.getInterfaceIdentity(r_ap_type);
                
                hFunc = [r_hFunc{2}];
                if isempty(r_ap_coord)
                    obj.placeObjects(r_nb_aps,r_type,obj.room.limits);
                else
                    %place it in coords
                    obj.fixObjects(r_type,r_ap_coord)
                end
            elseif ~isempty(r_spacing)
                Mx               = obj.Mx(:);
                My               = obj.My(:);
                
                grid             = [Mx(:),My(:),obj.Mz(:)];
                coord            = grid(1:r_spacing:end,:);
                r_nb_aps         = size(coord,1);
                r_ap_type        = repmat(r_ap_type,r_nb_aps,1);
                [r_type,r_hFunc] = radio.spectrum.getInterfaceIdentity(r_ap_type);
                hFunc            = [r_hFunc{2}];
                
                obj.fixObjects(r_type,coord);
            
            end
            
            
            % Room
            if sr_nb_aps > 0
                if size(sr_ap_type,1) ~= sr_nb_aps
                    sr_ap_type = repmat(sr_ap_type,sr_nb_aps,1);
                end
                
                [sr_type,sr_hFunc] = radio.spectrum.getInterfaceIdentity(sr_ap_type);
                
                hFunc = [hFunc;sr_hFunc{2}];
                
                if isempty(sr_ap_coord)
                    obj.room.placeObjects(sr_nb_aps,sr_type,[]);                    
                else
                    %place it in coords
                    sr_ap_coord = obj.set_coord_in_small_room(sr_nb_aps,sr_ap_coord);
                    obj.room.fixObjects(sr_type,sr_ap_coord)
                end
                
            elseif ~isempty(sr_spacing)
                Mx                 = obj.room.Mx(:)+obj.room_pos(1,1);
                My                 = obj.room.My(:)+obj.room_pos(1,2);
                
                grid               = [Mx(:),My(:),obj.room.Mz(:)];
                coord              = grid(1:sr_spacing:end,:);
                sr_nb_aps          = size(coord,1);
                sr_ap_type         = repmat(sr_ap_type,sr_nb_aps,1);
                [sr_type,sr_hFunc] = radio.spectrum.getInterfaceIdentity(sr_ap_type);
                hFunc              = [hFunc;sr_hFunc{2}];
                
                obj.room.fixObjects(sr_type,coord);
            
            end
            
            
            % the room is located in its coordinates!
            %assing an id to each object and corresponding hFunc
            
            id_list            = 1:(r_nb_aps + sr_nb_aps);
            obj.map_ap_to_func = {id_list', ...            % Access point ID
                [obj.objs;...            % Access point location
                obj.room.objs], ...      % and type
                hFunc'};
        end
        
        
        
        
       
        function cleanScene(obj,type)
            obj.scene(obj.scene==2) = 0;
        end
        
        function update(obj,nb_aps,areaID,ap_type,ap_location)
            
            switch areaID
                case 'floor' %big room
                    
                case 'room' %small room
                    if size(ap_type,1) ~= nb_aps
                        ap_type = repmat(ap_type,nb_aps,1);
                    end
                    
                    [ap_type,sr_hFunc] = radio.spectrum.getInterfaceIdentity(ap_type);
                    
                    % Remove old entries
                    nbVec     = 1:size(obj.map_ap_to_func{2},1);
                    old_entry = obj.map_ap_to_func{2}(:,end) == ap_type(1);
                    obj.map_ap_to_func{1}(old_entry,:) = [];
                    obj.map_ap_to_func{2}(old_entry,:) = [];
                    obj.map_ap_to_func{3}(nbVec(old_entry==1)) = [];
                    
                    last_id  = size(obj.map_ap_to_func{2},1);
                    ap_coord = obj.set_coord_in_small_room(nb_aps,ap_location);
                    
                    hFunc   = [obj.map_ap_to_func{3},sr_hFunc{2}'];
                    
                    new_ids = last_id+1:last_id+nb_aps;
                    id_list = [obj.map_ap_to_func{1};new_ids'];
                    obj.room.cleanScene(ap_type(1));
            end
            
            obj.room.fixObjects(ap_type,ap_coord)
            
            obj.map_ap_to_func{1}  = id_list;
            obj.map_ap_to_func{2} = [obj.objs;...            % Access point location
                obj.room.objs];
            obj.map_ap_to_func{3} = hFunc;
        end
        
        
        % make call from obj to set properties
        function showMap(obj,hOut)
            if ~ishandle(hOut.hAxis)
                hOut.figure;
            end
            
            Em   = obj.getFloorStations();
            grid = Floor.createMap(obj.scene,obj.room.scene);
            
            markers = [{'s'},{'^'},{'^'}];
            color   = [{'b'},{'g'},{'y'}];
            inout.output.showGrid(hOut.hAxis,grid,markers,color,10,obj.unit);
            %             inout.output.writeIn(hOut.hAxis,build.scenario.coordinateMapping(Em{2}(:,1:2),obj.limits),num2str(Em{1}));
            xlim(hOut.hAxis,[obj.limits(1,1)+1 obj.limits(2,2)+1]);
            ylim(hOut.hAxis,[obj.limits(1,1)+1 obj.limits(2,2)+1]);
            disp('Stations locations:')
            disp(fix(obj.objs+1));
            disp(fix(obj.room.objs+1));
        end
        
        %@override
        function create2DWalk(obj,nbSteps,lenSteps,nbWalks)
            %CREATE2DWALK generates N walks of size nbSteps with a step
            %size defined by lenSteps.
            % The walks are stored in a matfile.
            
            obj.storage.number_of_walks = nbWalks;
            obj.storage.number_of_steps_per_walk = nbSteps;
            obj.storage.ermse(nbSteps,nbWalks)=NaN;
            obj.storage.estimates(nbSteps,obj.ndim)=NaN;
            obj.storage.walk = NaN(nbSteps*nbWalks,2);
            head = 1;
            len  = nbSteps;
            for k=1:nbWalks
                wpos = build.scenario.ramdomWalk(nbSteps,[ 0 0; obj.len-1, obj.wid-1],lenSteps);
                obj.storage.walk(head:head+len-1,:) = wpos;
                head = head+len;
            end
            
            disp('Walks created inside ');
            disp(obj.limits(:,1:2));
        end
        
        
        function rt = getFloorStations(obj)
            rt = obj.map_ap_to_func;
        end
        
        function showWalk(obj,hOut,N)
            %SHOWWALK draws the walk on the figure axis provided
            if ~ishandle(hOut.hAxis)
                hOut.figure;
            end
            if ~exist('N','var')
                N = obj.storage.number_of_walks;
            end
            M    = obj.storage.number_of_steps_per_walk;
            head = 1;
            len  = M;
            for k=1:N
                walk = obj.storage.walk(head:head+len-1,1:2)+1;
                inout.output.draw_path(hOut.hAxis(1),walk,[0 0 1],[]);
                head = head+len;
            end
        end
        
        
        
        
        function showAcquisition(obj,hAxis,mobile)
            %SHOWWALK draws the walk on the figure axis provided
            %             if ~ishandle(hAxis)
            % %                 hAxis = [inout.output.createnfig(2,1,1,1); hOut.hAxis];
            %             end
            N = obj.storage.number_of_walks;
            M = obj.storage.number_of_steps_per_walk;
            K = mobile.nbSeen;
            head = 1;
            len  = M;
            
            title(hAxis(2),'RSS observed');
            ylabel(hAxis(2),'RSS dBm');
            xlabel(hAxis(2),'Observation time');
            ylim(hAxis(2),[-100 0]);
            xlim(hAxis(2),[1 10]);
            walk = obj.storage.walk(head:head+len-1,1:2)+1;
            meas = reshape(mobile.fingerprints.meas,K,M,N);
            for s=2:M
                plot(hAxis(1),walk(s-1,1),walk(s-1,2),'wx');
                plot(hAxis(1),walk(1:s,1),walk(1:s,2),'-b');
                plot(hAxis(1),walk(s,1),walk(s,2),'xb');
                plot(hAxis(2),repmat(1:s,K,1)',meas(:,1:s,N)','linewidth',2);
                if s > 10,
                    xlim(hAxis(2),[s-10 s]);
                end
                legend(hAxis(2),cellstr(num2str((1:K)', 'N=%-d')),'Location','NorthEastOutside');
                drawnow
                pause(0.1);
            end
            plot(hAxis(1),walk(s,1),walk(s,2),'wx');
            plot(hAxis(1),walk(s,1),walk(s,2),'bo','MarkerFaceColor','b');
            plot(hAxis(1),walk(1,1),walk(1,2),'bo','MarkerFaceColor','w');
            xlim(hAxis(2),[1 s]);
            
            
        end
        
        
    end
    
end
