
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%   Tampere University of Technology
%
%   FPSIM
%
%   This simulator allows one to set up a room environment with several rf
%   technologies, such as wifi, bluetooth, rfid and so on.
%
%   Developed by,
%
%   Pedro Silva
%   pedro.silvha@tut.fi
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clearvars variables
close all;
rng('shuffle');

dir_storage = './storage/';
mkdir(dir_storage);

%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Scenario Parameters %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%
nbWalks          = 1;
nbSteps          = 100;%room_length*room_width;
room_len         = 25;
room_wid         = 25;
room_dep         = 0;
ndim             = 3;

nRx              = 1; % number of receivers
unit             = 1; % 1 entry == 1 meter
spectrum_db_unit = 1; % fix this (or force db_unit = unit)
lenStep          = 1;
assert(unit >= spectrum_db_unit);


% For Simona's function
use_n_neighbours       = 5;  % (#) number of neihbours for mean calculation
sigma_shadowing_global = 4;  % (#) shawdowing value 
sigma_shadowing_wifi   = 4;  % (#) shawdowing value
sigma_shadowing_rfid   = 2;
wifi_model_table_row   = 4;
nbNeighbours           = 5;
rfid_angle             = 90;



small_room_len     = 8;
small_room_wid     = 8;
small_room_dep     = room_dep;


% Number of APS and types
room_nb_aps        = 1; % 50*50*4 m for 300-400 APs
small_room_nb_aps  = 8;
room_ap_type       = 'wifi';
small_room_ap_type = 'rfid';


%Placement for rfid
rfid_location    = []; % [], corner, middle or center
wifi_sensitivity = -100;%dBm
rfid_sensitivity = -60; %dBm
sensitivity      = [ones(1,room_nb_aps).*wifi_sensitivity,ones(1,small_room_nb_aps).*rfid_sensitivity];

% Output handler
hOut   = inout.output();
hOut.figure(1);

% Create floor and define room limits
floor = Floor(room_len,room_wid,room_dep,ndim,unit,...
    ...
    dir_storage,['ICL_',...
    num2str(nbWalks),'_',num2str(room_len),'x',num2str(room_wid),...
    '_WalksAndMeasurements'],...
    false,...
    ...
    small_room_nb_aps,small_room_len,small_room_wid,small_room_dep);
floor.deploy(room_nb_aps,room_ap_type,[],[],small_room_nb_aps,small_room_ap_type,rfid_location,[]);


% Create walks inside the room if they havent been computed already
if ~floor.room.exists
	floor.room.create2DWalk(nbSteps,lenStep,nbWalks);
	floor.room.storage.walk = bsxfun(@plus,floor.room.storage.walk,floor.room.limits(1,1:2));
end


% Create radio maps
nbPoints = nbSteps*nbWalks; % acquisition points
spectrum = RadioMaps(floor,spectrum_db_unit,wifi_model_table_row,rfid_angle); %should also save to matfile

% % Create sensitivity per device, ok? :)
mobile = Receiver(spectrum,sensitivity,dir_storage,['fingerprints_of_',num2str(nbWalks),'walks_in_',num2str(room_len),'x',num2str(room_wid)],true,nbNeighbours,sigma_shadowing_global,wifi_sensitivity,rfid_sensitivity);
mobile.acquire(floor.room,nbSteps,nbWalks); % Acquire fingerprints for all

outfile = inout.output.createMatfile(dir_storage,'demo',true,true);
mobile.estimate('ALL',outfile); % Obtains the estimated positions

%%
floor.showMap(hOut);
hOut.video(['Two Path Example with ',num2str(room_nb_aps),' (wifi) and ',num2str(small_room_nb_aps),' (rfid)'],5);
inout.output.draw_two_path(hOut.hAxis,floor.room.storage.walk(1:nbSteps,1:2)+1,outfile.pest(:,1:2)+1,[0 0 1],[0 1 0],true,hOut)


%Restores walks to offset for future usage in other room location
floor.room.storage.walk = bsxfun(@minus,floor.room.storage.walk,floor.room.limits(1,1:2));
