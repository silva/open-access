classdef RadioMaps < radio.spectrum
    %UNTITLED2 Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        rfid_ang    % (#)  contains the openning angle used
        wifi_row    % (#)  table row in use (from the model mentioned in radio.pathloss)
        
        wifi_n;     % (#)  wifi propagation coefficient
        wifi_L;     % (dB) wifi pathloss
        wifi_s;     % (dB) wifi shadowing
        
        rfid_n;     % (#)  rfid propagation coefficient
        rfid_L;     % (dB) rfid pathloss
        rfid_s;     % (dB) rfid shadowing
        
    end
    
    methods
        
        function obj = RadioMaps(AreaObj,unit,wifi_model_table_row,rfid_angle)
            
            % Call to constructor class
            obj  = obj@radio.spectrum(AreaObj.limits,unit);
            
            % Construct own details
            obj.setDB(AreaObj.getFloorStations);
            tech          = obj.getDB();
            tech          = tech{2}(:,end); % last col
            arg_list      = zeros(obj.nbEmitters,1);
            for k=1:obj.nbEmitters
                switch upper(radio.spectrum.getInterfaceName(tech(k)))
                    case 'WIFI'
                        arg_list(k) = wifi_model_table_row;
                    case 'RFID'
                        arg_list(k) = rfid_angle;
                end
            end
            obj.setDB([obj.getDB(), arg_list]);
            obj.wifi_row                       = wifi_model_table_row;
            obj.rfid_ang                       = rfid_angle;
            [obj.wifi_n,obj.wifi_L,obj.wifi_s] = radio.pathloss.fetchModelParameters('wifi',wifi_model_table_row);
            [obj.rfid_n,obj.rfid_L,obj.rfid_s] = radio.pathloss.fetchModelParameters('rfid',rfid_angle);
            
            obj.survey();
            
        end
    end
    
    methods
        
      
        function update(obj,AreaObj,wifi_model_table_row,rfid_angle)
            obj.setDB(AreaObj.getFloorStations);
            tech          = obj.getDB();
            tech          = tech{2}(:,end); % last col
            nbSeen        = size(tech,1);
            arg_list      = zeros(nbSeen,1);
            for k=1:nbSeen
                switch upper(radio.spectrum.getInterfaceName(tech(k)))
                    case 'WIFI'
                        arg_list(k) = wifi_model_table_row;
                    case 'RFID'
                        arg_list(k) = rfid_angle;
                end
            end
            obj.setDB([obj.getDB(), arg_list]);
            obj.wifi_row                       = wifi_model_table_row;
            obj.rfid_ang                       = rfid_angle;
            [obj.wifi_n,obj.wifi_L,obj.wifi_s] = radio.pathloss.fetchModelParameters('wifi',wifi_model_table_row);
            [obj.rfid_n,obj.rfid_L,obj.rfid_s] = radio.pathloss.fetchModelParameters('rfid',rfid_angle);
            
            obj.survey(); %provide ids to skip %optional
        end
        
        
        
        
        function [freq,sens,shw] = signalDetails(obj,type)
            
            switch(upper(type))
                
                case 'WIFI'
                    freq = 2.4e9; %Hz
                    sens = obj.sen_wifi;
                    shw  = obj.shw_wifi;
                    
                case 'RFID'
                    freq = 866e6; %Hz
                    sens = obj.sen_rfid;
                    shw  = obj.shw_rfid;
                    
                otherwise
                    error('RadioMaps:signalDetails','Type unknown');
            end
            
        end
        
        function meas = probe(obj,posx,posy,id,shw,sense)
            %PROBE rerieves the value at the given position
            [~,idx] = min(sqrt(sum(bsxfun(@minus,obj.rm_pos(:,1:2),[posx posy]).^2,2)));
            meas = obj.radiomaps(idx,id)+shw;
            if meas < sense
                meas = NaN;
            end
            
        end
        
        
        function survey(obj)
            
            % SURVEY implements the radio map generation
            Em    = obj.getDB();
            N     = obj.nbEmitters;
            Ga    = [obj.x(:),obj.y(:),obj.z(:)];
            MxN   = [size(Ga,1) 1]; % build one shawdowing map for each user + training
            % for each emitter
            for k=1:N
                id   = Em{1}(k);
                pos  = Em{2}(k,1:3);
                hFun = Em{3}{k};
                aFun = Em{end}(k);
                
                % Creates the rss maps
                [n,L,s,f]     = hFun{1}(aFun);
                distMap       = sqrt(sum(bsxfun(@minus,Ga,pos).^2,2));
                radio_map     = arrayfun(hFun{2},distMap,repmat(n,size(Ga,1),1),repmat(L,size(Ga,1),1),repmat(f,size(Ga,1),1));    %
                shadowing_map = hFun{3}(s,MxN);             % calls shadowing function
                
                obj.insert(id,radio_map,shadowing_map,distMap);     % saves to the database
                
                disp(' ');
                disp('Radio map generated...');
                disp(['ID: ',num2str(Em{1}(k))]);
                disp(['Pos: ',num2str(pos)]);
                disp('Function handles')
                disp(hFun);
                disp(['Model input: ',num2str(aFun)]);
            end
            
            obj.rm_pos = Ga;
        end
    end
    
end

