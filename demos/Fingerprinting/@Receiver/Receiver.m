classdef Receiver < build.device
    %UNTITLED6 Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        fingerprints  % mfile with fingerprints
        % shadowing is addded to the ideal database
        % generated according to the number of track points
        storage
        output
        wifi_sensitivity
        rfid_sensitivity
    end
    
    properties(Access=private)
        sigma_shadowing_global
        nbNeighbours
    end
    
    methods
        
        function obj = Receiver(hSpectrum,sensitivity,dir,filename,forceOW,nbNeighbours,sigma_global,wifi_sensitivity,rfid_sensitivity)
            obj = obj@build.device(hSpectrum,sensitivity);
            if exist([dir,filename,'.mat'],'file') && ~forceOW
                obj.fingerprints = inout.output.createMatfile( dir, filename, true, false);
            else
                obj.fingerprints = inout.output.createMatfile( dir, filename, true, true);
            end
            
            obj.nbNeighbours           = nbNeighbours;
            obj.sigma_shadowing_global = sigma_global;
            obj.wifi_sensitivity       = wifi_sensitivity;
            obj.rfid_sensitivity       = rfid_sensitivity;
        end
        
        function acquire(obj,hEnv,nbSteps,nbWalks,hFunc)
            %FOR ALL TRACKS
            
            % baisc step is to go to the database and colect data for each
            % point the user walks by
            
            % do it for all
            nbPoints   = nbSteps*nbWalks;
            Em         = obj.spectrum.getDB();
            N          = obj.spectrum.nbEmitters;
            
            % makes sure there are no old values
            obj.nbSeen                      = N;
            obj.fingerprints.nbSeen         = N;
            obj.fingerprints.shadowing      = [];
            obj.fingerprints.shadowing(N*nbPoints,1)=0;
            obj.fingerprints.nbFingerprints = nbPoints*N;
            
            % stores in each collumn the shadowing values
            s         = zeros(N,1);
            shadowing = zeros([N nbPoints]);
            for k=1:N
                hFun           = Em{3}{k};
                aFun           = Em{4}(k);
                [~,~,s(k),~]   = hFun{1}(aFun);
                shadowing(k,:) = hFun{3}(s(k),[1 nbPoints]);    % calls shadowing function
            end
            obj.fingerprints.shadowing        = shadowing(:);
            obj.fingerprints.shadowing_per_AP = s;
            clear shadowing;
            
            
            %Build fingerprints as
            % ( |<x,y,z>|(1), base_value(1)+shdw(1) | 1 )
            % ( |<x,y,z>|(1), base_value(1)+shdw(2  | 2 )
            % ( |<x,y,z>|(1), base_value(1)+shdw(") | " )
            % ( |<x,y,z>|(1), base_value(1)+shdw(N) | N )
            
            % build N fingerprints ar a time!
            obj.fingerprints.id_list = 1:N;
            obj.fingerprints.id_list = obj.fingerprints.id_list';
            obj.fingerprints.id_list = kron(obj.fingerprints.id_list,ones(1,nbSteps));
            obj.fingerprints.sense   = kron(obj.sensitivity,ones(1,nbSteps));
            obj.fingerprints.track   = round(hEnv.storage.walk*100)/100;
            
            % need to fix this one
            obj.fingerprints.id_list = repmat(reshape(obj.fingerprints.id_list,numel(obj.fingerprints.id_list),1),[nbWalks 1]);
            obj.fingerprints.walks   = kron(obj.fingerprints.track,ones(N,1)); % replicates positions
            obj.fingerprints.meas    = arrayfun(hFunc,...
                obj.fingerprints.walks(:,1),...
                obj.fingerprints.walks(:,2),...
                obj.fingerprints.id_list,...
                obj.fingerprints.shadowing,...
                repmat(reshape(obj.fingerprints.sense,numel(obj.fingerprints.sense),1),[nbWalks 1]));          % computes all the fps
            
        end
        
        
        function estimate(obj,type,hStore)
            
            Em     = obj.spectrum.getDB;
            Em_id  = Em{1};
            Em_dtl = Em{2};
            switch upper(type(1:3))
                case 'ALL'
                    sel_ids = Em_id;
                    sel_sen = obj.sensitivity;
                    
                case 'WIF'
                    sel     = Em_dtl(:,end)==radio.spectrum.inter2id('wifi'); 
                    sel_ids = Em_id(sel);
                    sel_sen = obj.sensitivity(sel);
                    
                case 'RFI'
                    sel     = Em_dtl(:,end)==radio.spectrum.inter2id('rfid');
                    sel_ids = Em_id(sel);
                    sel_sen = obj.sensitivity(sel);
            end
            
            
            nbEm   = obj.spectrum.nbEmitters;
            nbPnt  = obj.fingerprints.nbFingerprints/nbEm; % always true
            db_pos = obj.spectrum.getCoordinates();
            db     = obj.spectrum.dumpSelection(sel_ids,sel_sen); % all the database in a vector with
            
            % I need to send the fingerprints only 
            radio.fingerprint.estimate(hStore,obj.fingerprints,sel_ids,nbPnt,nbEm,db,db_pos,obj.sigma_shadowing_global,obj.nbNeighbours);
            
            % with the track positions estimated, compute the error
            [hStore.rmse,hStore.dist] = math.rmse(obj.fingerprints.track, hStore.pest(:,1:2));
            
            
        end
       
        
        function update(obj,N_new,nbSteps,nbWalks,hFunc,prevSeen)
            Em     = obj.spectrum.getDB;
            obj.sensitivity = zeros(size(Em{1},1),1);
            obj.sensitivity(Em{2}(:,end) == radio.spectrum.getInterfaceIdentity('wifi'))=obj.wifi_sensitivity;
            obj.sensitivity(Em{2}(:,end) == radio.spectrum.getInterfaceIdentity('rfid'))=obj.rfid_sensitivity;
            
            % do new acquisition for the newcomers
            id  = Em{1}(end-(N_new-1):end,:);%retrieve id from EM
            sen = obj.sensitivity(end-(N_new-1):end,:);
            
            
            % for this guy get the new fps
            obj.acquire_for(id,nbSteps,nbWalks,hFunc,sen,prevSeen);
            
       %update the value
%             obj.fingerprints.nbFingerprints = nbPoints*N;
            
        end
        
    end
    
    
    methods(Access=private)
        
        function acquire_for(obj,id,nbSteps,nbWalks,hFunc,sense,db_prev_len)
                   %Build fingerprints as
            % ( |<x,y,z>|(1), base_value(1)+shdw(1) | 1 )
            % ( |<x,y,z>|(1), base_value(1)+shdw(2  | 2 )
            % ( |<x,y,z>|(1), base_value(1)+shdw(") | " )
            % ( |<x,y,z>|(1), base_value(1)+shdw(N) | N )
            nbPoints   = nbSteps*nbWalks;
            Em         = obj.spectrum.getDB();
            N          = length(id); % has to be id_list length!
            s          = zeros(N,1);
            shadowing  = zeros([N nbPoints]);
            if isrow(id)
                aps=id;
            else
                aps=id';
            end
            cnt = 0;
            for k=aps
                cnt= cnt+1;
                hFun           = Em{3}{k};
                aFun           = Em{4}(k);
                [~,~,s(cnt),~]   = hFun{1}(aFun);
                shadowing(cnt,:) = hFun{3}(s(cnt),[1 nbPoints]);    % calls shadowing function
            end
            obj.fingerprints.tmp_shadowing=shadowing(:);
            
            % build N fingerprints ar a time!
            obj.fingerprints.sense       = kron(sense,ones(nbPoints,1));
            obj.fingerprints.id_list     = kron(id,ones(nbPoints,1));
            obj.fingerprints.tmp_id_list = repmat(reshape(obj.fingerprints.id_list,numel(obj.fingerprints.id_list),1),[nbWalks 1]);
            obj.fingerprints.tmp_walks   = kron(obj.fingerprints.track ,ones(N,1)); % replicates positions
            obj.fingerprints.tmp_meas    = arrayfun(hFunc,...
                obj.fingerprints.tmp_walks(:,1),...
                obj.fingerprints.tmp_walks(:,2),...
                obj.fingerprints.id_list,...
                obj.fingerprints.tmp_shadowing,...
                obj.fingerprints.sense);          % computes all the fps 
            
            % replace line entry
            obj.fingerprints.meas       = reshape(obj.fingerprints.meas,db_prev_len,obj.fingerprints.nbFingerprints/db_prev_len);
            obj.fingerprints.meas(id,:) = reshape(obj.fingerprints.tmp_meas,N,nbPoints); 
            obj.fingerprints.meas       = reshape(obj.fingerprints.meas,size(obj.fingerprints.meas,1)*nbPoints,1);
            
            
            obj.fingerprints.shadowing       = reshape(obj.fingerprints.shadowing,db_prev_len,obj.fingerprints.nbFingerprints/db_prev_len);
            obj.fingerprints.shadowing(id,:) = reshape(obj.fingerprints.tmp_shadowing,N,nbPoints); 
            obj.fingerprints.shadowing       = reshape(obj.fingerprints.shadowing,size(obj.fingerprints.shadowing,1)*nbPoints,1);
            obj.fingerprints.id_list         = unique([obj.fingerprints.id_list;obj.fingerprints.id_list]);
            
            % reshape the matrices and add the new shadowing values and fps
            obj.nbSeen                      = obj.spectrum.nbEmitters;
            obj.fingerprints.nbSeen         = obj.spectrum.nbEmitters;
            obj.fingerprints.nbFingerprints = nbPoints*obj.spectrum.nbEmitters;
            
            % cleanup
            obj.fingerprints.tmp_walks      = [];
            obj.fingerprints.tmp_meas       = [];
            obj.fingerprints.tmp_shadowing  = [];
            obj.fingerprints.tmp_id_list    = [];
        end
        
    end
    
    
end

%             % goal is to call function with a reduce footprint
%             obj.fingerprints.track = round(hEnv.storage.walk*100)/100;
%             ntimes   = nbPoints*N;
%             stepSize = N*500;
%             id_list  = 1:N;
%             id_list  = id_list';
%             id_list  = kron(id_list,ones(1,nbSteps));
%             for ii=1:stepSize:ntimes/stepSize
%                 % prepare slices
%                 idx   = ii:ii+stepSize-1;
%                 obj.fingerprints.walks = kron(obj.fingerprints.track(idx,1),ones(N,1));
%                 
%                 obj.fingerprints.meas(idx,1) = arrayfun(hFunc,...
%                 obj.fingerprints.walks(idx,1),...
%                 obj.fingerprints.walks(idx,2),...
%                 obj.fingerprints.id_list,...
%                 obj.fingerprints.shadowing(idx,1),...
%                 repmat(sense(:),[nbWalks 1]));          % computes all the fps
%             end

