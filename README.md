# LICENSE #
This work is provided under [Creative Commons 4.0](http://creativecommons.org/licenses/by/4.0/)

# README #

Welcome to my research open access git repository.

In here you can find code and examples I have used for my publications and work in general. They are free to use, but please make a reference to this page or the paper the code refers to in your publications.

You can find my demos, videos and projects at https://bitbucket.org/silva/open-access/src/36f5def9d57ad3409492b240cfda423739b55da8?at=master

I am using oop code in matlab and for most examples to use, make sure you download all the classes and add them to your matlab path.

### How do I get set up? ###

Set up git in your environment and clone the repository. Alternatively you can download a zip file with all the code.

### Who do I talk to? ###

Please contact me at pedro.silva [at] tut.fi if you have doubts, questions, suggestions or want to contribute.